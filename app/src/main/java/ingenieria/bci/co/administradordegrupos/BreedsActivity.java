package ingenieria.bci.co.administradordegrupos;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Adapters.BreedListAdapter;
import ingenieria.bci.co.administradordegrupos.Adapters.FarmListAdapter;
import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.Breeds;
import ingenieria.bci.co.administradordegrupos.Model.Farms;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;

public class BreedsActivity extends AppCompatActivity {

    Database db;
    ArrayList<Breeds> breedsArray;
    public BreedListAdapter adapter;
    RelativeLayout NoDataPanel;
    FloatingActionButton fabAddBreed;
    ListView breedList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_breeds);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.breedTitle);
        breedList = (ListView) findViewById(R.id.breedlist);
        SetViews();
    }

    protected void onStart() {
        FillList();
        super.onStart();
    }

    private void SetViews() {
        fabAddBreed = (FloatingActionButton) findViewById(R.id.fabAddBreed);
        fabAddBreed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BreedsActivity.this, BreedsAddActivity.class);
                startActivity(intent);
            }
        });
    }

    private void FillList() {
        db = new Database(getBaseContext());
        NoDataPanel = (RelativeLayout) findViewById(R.id.NoDataPanel);
        breedsArray = null;
        breedsArray = db.GetBreeds();
        adapter = new BreedListAdapter(getBaseContext(), breedsArray, BreedsActivity.this);
        breedList.setAdapter(adapter);
        if (breedsArray.size() > 0) {
            NoDataPanel.setVisibility(View.INVISIBLE);

            SetLongPress();
        } else {
            NoDataPanel.setVisibility(View.VISIBLE);
        }
        adapter.notifyDataSetChanged();
    }

    private void SetLongPress() {
        breedList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView parent, View view, final int position, long id) {
                final CharSequence[] items = {getString(R.string.change_status)};
                final AlertDialog.Builder builder = new AlertDialog.Builder(BreedsActivity.this);
                builder.setTitle("BCI");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AlertDialog.Builder builderD = new AlertDialog.Builder(BreedsActivity.this);
                        builderD.setTitle("BCI");
                        builderD.setMessage(R.string.inactivate);
                        builderD.setPositiveButton(R.string.alertyes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Database db = new Database(BreedsActivity.this);
                                db.InactivateBreeds(breedsArray.get(position).getBreedId());
                                dialog.dismiss();
                                Toast.makeText(BreedsActivity.this, R.string.complete, Toast.LENGTH_LONG);
                                try {
                                    FillList();
                                } catch (Exception e) {

                                }
                            }
                        });
                        builderD.setNegativeButton(R.string.alertno, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                        builderD.show();

                    }
                });
                builder.show();
                return false;
            }
        });
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
