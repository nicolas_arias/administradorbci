package ingenieria.bci.co.administradordegrupos.Services;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.Medications;
import ingenieria.bci.co.administradordegrupos.R;

/**
 * Created by nicolas on 7/1/17.
 */

public class DatabaseTools {
    Activity activity;
    Database db;

    public DatabaseTools(Activity _activity) {
        db = new Database(_activity);
        activity = _activity;
    }

    public ArrayList<String> GetArrayObject(String model, String field, String condition, boolean all) {
        ArrayList<String> result = db.getStringArray(model, field, condition);
        if (all)
            result.add(0, activity.getResources().getString(R.string.all));
        return result;
    }

    public void setAdapter(Spinner ddl, String model, String field, String condition) {
        setAdapter(ddl, model, field, condition, true);
    }


    public void setAdapter(Spinner ddl, String model, String field, String condition, boolean all) {
        ArrayAdapter<String> spinnerArray = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, GetArrayObject(model, field, condition, all));
        spinnerArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ddl.setAdapter(spinnerArray);
    }

    public void setAdapterId(Spinner ddl, int id) {
        setAdapterId(ddl, id, true);
    }

    public void setAdapterId(Spinner ddl, int id, boolean all) {
        ArrayList<String> stringArray = new ArrayList<>();
        if (all)
            stringArray.add(activity.getResources().getString(R.string.all));
        for (String str : activity.getResources().getStringArray(id)) {
            stringArray.add(str);
        }
        ArrayAdapter<String> spinnerArray = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, stringArray);
        spinnerArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ddl.setAdapter(spinnerArray);
    }
}
