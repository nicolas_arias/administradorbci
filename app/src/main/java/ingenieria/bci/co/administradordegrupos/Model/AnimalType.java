package ingenieria.bci.co.administradordegrupos.Model;

/**
 * Created by Nicolas on 17/06/2017.
 */

public class AnimalType {
    public String AnimalTypeId;
    public String AnimalTypeName;
    public String AnimalTypeStatus;

    public AnimalType(String animalTypeId, String animalTypeName, String animalTypeStatus) {
        AnimalTypeId = animalTypeId;
        AnimalTypeName = animalTypeName;
        AnimalTypeStatus = animalTypeStatus;
    }

    public String getAnimalTypeId() {
        return AnimalTypeId;
    }

    public void setAnimalTypeId(String animalTypeId) {
        AnimalTypeId = animalTypeId;
    }

    public String getAnimalTypeName() {
        return AnimalTypeName;
    }

    public void setAnimalTypeName(String animalTypeName) {
        AnimalTypeName = animalTypeName;
    }

    public String getAnimalTypeStatus() {
        return AnimalTypeStatus;
    }

    public void setAnimalTypeStatus(String animalTypeStatus) {
        AnimalTypeStatus = animalTypeStatus;
    }
}
