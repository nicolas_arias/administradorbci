package ingenieria.bci.co.administradordegrupos.Model;

/**
 * Created by nicolas on 7/2/17.
 */

public class AnimalSales {
    public int AnimalSaleId;
    public int AnimalSaleSecuence;
    public String AnimalEID;
    public String SaleDate;
    public String PaymentType;
    public String Buyer;
    public String Reference;
    public String Price;


    public AnimalSales() {
    }

    public int getAnimalSaleId() {
        return AnimalSaleId;
    }

    public void setAnimalSaleId(int animalSaleId) {
        AnimalSaleId = animalSaleId;
    }

    public int getAnimalSaleSecuence() {
        return AnimalSaleSecuence;
    }

    public void setAnimalSaleSecuence(int animalSaleSecuence) {
        AnimalSaleSecuence = animalSaleSecuence;
    }

    public String getAnimalEID() {
        return AnimalEID;
    }

    public void setAnimalEID(String animalEID) {
        AnimalEID = animalEID;
    }

    public String getSaleDate() {
        return SaleDate;
    }

    public void setSaleDate(String saleDate) {
        SaleDate = saleDate;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(String paymentType) {
        PaymentType = paymentType;
    }

    public String getBuyer() {
        return Buyer;
    }

    public void setBuyer(String buyer) {
        Buyer = buyer;
    }

    public String getReference() {
        return Reference;
    }

    public void setReference(String reference) {
        Reference = reference;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }
}
