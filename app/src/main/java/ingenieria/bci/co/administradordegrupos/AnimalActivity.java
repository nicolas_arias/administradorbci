package ingenieria.bci.co.administradordegrupos;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import ingenieria.bci.co.administradordegrupos.Adapters.AnimalAdapter;
import ingenieria.bci.co.administradordegrupos.Adapters.HerdListAdapter;
import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.Animal;
import ingenieria.bci.co.administradordegrupos.Model.AnimalCondition;
import ingenieria.bci.co.administradordegrupos.Model.AnimalFlushing;
import ingenieria.bci.co.administradordegrupos.Model.AnimalInsemination;
import ingenieria.bci.co.administradordegrupos.Model.AnimalWeight;
import ingenieria.bci.co.administradordegrupos.Model.Herds;
import ingenieria.bci.co.administradordegrupos.Model.Medications;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
import ingenieria.bci.co.administradordegrupos.Services.DatabaseTools;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

public class AnimalActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    Database db;
    ArrayList<Animal> animalsArray;
    AnimalAdapter adapter;
    RelativeLayout NoDataPanel;
    FloatingActionButton fabAddAnimal;
    ListView animalList;
    RelativeLayout searchPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.animal_list);
        setContentView(R.layout.activity_animal);
        SetViews();
    }

    private void SetViews() {
        fabAddAnimal = (FloatingActionButton) findViewById(R.id.fabAddAnimal);
        fabAddAnimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AnimalActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
        animalList = (ListView) findViewById(R.id.animallist);

    }

    private void FillList() {
        db = new Database(getBaseContext());
        NoDataPanel = (RelativeLayout) findViewById(R.id.NoDataPanel);
        animalsArray = null;
        animalsArray = db.GetAnimals();
        adapter = new AnimalAdapter(getBaseContext(), animalsArray, AnimalActivity.this);
        animalList = (ListView) findViewById(R.id.animallist);
        animalList.setAdapter(adapter);
        getSupportActionBar().setSubtitle("Total : " + animalsArray.size());
        if (animalsArray.size() > 0) {
            NoDataPanel.setVisibility(View.INVISIBLE);



            animalList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView parent, View view, final int position, long id) {
                    final CharSequence[] items = {
                            getString(R.string.details),
                            getString(R.string.edit),
                            getString(R.string.manage_insemination),
                            getString(R.string.Flushing),
                            getString(R.string.animal_condition),
                            getString(R.string.set_weight_details)};
                    final AlertDialog.Builder builder = new AlertDialog.Builder(AnimalActivity.this);
                    builder.setTitle(getString(R.string.title));
                    builder.setItems(items, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            switch (which) {
                                case 0:
                                    SeeDetails(position);
                                    break;
                                case 1:
                                    EditAnimal(position);
                                    break;
                                case 2:
                                    Insemination(position);
                                    break;
                                case 3:
                                    FlushingAnimal(position);
                                    break;
                                case 4:
                                    AnimalCondition(position);
                                    break;
                                case 5:
                                    WeightDetails(position);
                                    break;
                            }
                        }
                    });
                    builder.show();
                    return false;
                }
            });
        } else {
            NoDataPanel.setVisibility(View.VISIBLE);
        }
        adapter.notifyDataSetChanged();
    }

    public void WeightDetails(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AnimalActivity.this);
        LayoutInflater inflater = AnimalActivity.this.getLayoutInflater();
        final View vista = inflater.inflate(R.layout.weight_detail_layout, null);
        TextView txtEID = (TextView) vista.findViewById(R.id.txtEID);
        final EditText txtCurrentWeight = (EditText) vista.findViewById(R.id.txtCurrentWeight);
        final EditText txt12month = (EditText) vista.findViewById(R.id.txt12month);
        final EditText txt18month = (EditText) vista.findViewById(R.id.txt18month);
        final EditText txt24month = (EditText) vista.findViewById(R.id.txt24month);

        txtEID.setText(animalsArray.get(position).getAnimalEID() + "\n" + getString(R.string.values_in) + " " + Tools.GetunitsWeight(this));

        txtCurrentWeight.setText(animalsArray.get(position).getAnimalCurrentWeight());
        txt12month.setText(animalsArray.get(position).getAnimalW12());
        txt18month.setText(animalsArray.get(position).getAnimalW18());
        txt24month.setText(animalsArray.get(position).getAnimalW24());


        final ImageButton btnReadCurrentWeight = (ImageButton) vista.findViewById(R.id.btnReadCurrentWeight);
        btnReadCurrentWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.ReadWeightFromBlueTooth(AnimalActivity.this, txtCurrentWeight, btnReadCurrentWeight);
            }
        });
        builder.setView(vista)
                .setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String current = txtCurrentWeight.getText().toString();
                        String month12 = txt12month.getText().toString();
                        String month18 = txt18month.getText().toString();
                        String month24 = txt24month.getText().toString();

                        String animalEID = animalsArray.get(position).getAnimalEID();
                        if (db.UpdateAnimalWeight(current, month12, month18, month24, animalEID)) {
                            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.complete));
                        } else {
                            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.tryagain));
                        }
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        builder.create().show();
    }

    public void FlushingAnimal(final int position) {
        if (animalsArray.get(position).getAnimalGender().equals("Male") ||
                animalsArray.get(position).getAnimalGender().equals("Masculino")) {
            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.insemination_male));
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(AnimalActivity.this);
        LayoutInflater inflater = AnimalActivity.this.getLayoutInflater();
        final View vista = inflater.inflate(R.layout.flushing_layout, null);

        final EditText txtDateFlushed;
        final EditText txtNoEmbryos;
        final EditText txtComments;
        final Spinner ddlNewHerd;
        TextView txtEIDFlushing = (TextView) vista.findViewById(R.id.txtEIDFlushing);
        txtEIDFlushing.setText(animalsArray.get(position).getAnimalEID());


        txtDateFlushed = (EditText) vista.findViewById(R.id.txtDateFlushed);
        txtNoEmbryos = (EditText) vista.findViewById(R.id.txtNoEmbryos);
        txtComments = (EditText) vista.findViewById(R.id.txtComments);
        txtDateFlushed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.OpenDateDialog(txtDateFlushed, AnimalActivity.this);
            }
        });


        builder.setView(vista)
                .setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String date = txtDateFlushed.getText().toString();
                        String number = txtNoEmbryos.getText().toString();
                        String comments = txtComments.getText().toString();

                        if (date.isEmpty() || number.isEmpty() || comments.isEmpty()) {
                            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.completeFields));
                            return;
                        }


                        AnimalFlushing flushing = new AnimalFlushing();
                        flushing.setAnimalEID(animalsArray.get(position).getAnimalEID());
                        flushing.setAnimalVisualNumber(animalsArray.get(position).getAnimalVisualNumber());
                        flushing.setDateFlushed(date);
                        flushing.setNoEmbryos(number);
                        flushing.setComments(comments);

                        if (db.RegisterFlushing(flushing)) {
                            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.complete));
                        } else {
                            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.tryagain));
                        }
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();

    }

    public void SeeDetails(final int position) {
        Intent intent = new Intent(this, AnimalDetailActivity.class);
        intent.putExtra("EID", animalsArray.get(position).getAnimalEID());
        startActivity(intent);
    }

    public void EditAnimal(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(AnimalActivity.this);
        LayoutInflater inflater = AnimalActivity.this.getLayoutInflater();
        final View vista = inflater.inflate(R.layout.animal_edit_layout, null);
        final Spinner ddlNewStatus;
        final EditText txtChangeDescription;
        final Spinner ddlNewFarm;
        final Spinner ddlNewHerd;
        TextView txtEIDEdit = (TextView) vista.findViewById(R.id.txtEIDEdit);
        txtEIDEdit.setText(animalsArray.get(position).getAnimalEID());
        ddlNewStatus = (Spinner) vista.findViewById(R.id.ddlNewStatus);
        txtChangeDescription = (EditText) vista.findViewById(R.id.txtChangeDescription);
        ddlNewFarm = (Spinner) vista.findViewById(R.id.ddlNewFarm);
        ddlNewHerd = (Spinner) vista.findViewById(R.id.ddlNewHerd);


        builder.setView(vista)
                .setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        try{
                            String newStatus = ddlNewStatus.getSelectedItem().toString();
                            String newDescription = txtChangeDescription.getText().toString();
                            String newFarm = ddlNewFarm.getSelectedItem().toString();
                            String newHerd = ddlNewHerd.getSelectedItem().toString();

                            int animalId = animalsArray.get(position).getAnimalId();
                            if (db.UpdateAnimal(animalId, newStatus, newDescription, newFarm, newHerd)) {
                                Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.complete));
                            } else {
                                Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.tryagain));
                            }
                            FillList();
                            dialog.dismiss();
                        }catch(Exception e){
                            Toast.makeText(AnimalActivity.this,R.string.norecords,Toast.LENGTH_LONG).show();
                        }

                    }
                })
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        final DatabaseTools dbtools = new DatabaseTools(AnimalActivity.this);
        dbtools.setAdapterId(ddlNewStatus, R.array.animal_status, false);
        dbtools.setAdapter(ddlNewFarm, "Farms", "FarmName", "", false);
        dbtools.setAdapter(ddlNewHerd, "Herds", "HerdName", "", false);

        Tools.selectSpinnerItemByValue(ddlNewStatus, animalsArray.get(position).getAnimalStatus());
        Tools.selectSpinnerItemByValue(ddlNewFarm, animalsArray.get(position).getAnimalFarm());
        Tools.selectSpinnerItemByValue(ddlNewHerd, animalsArray.get(position).getAnimalHerd());

        builder.create().show();

    }

    public void Insemination(final int position) {
        if (animalsArray.get(position).getAnimalGender().equals("Male") ||
                animalsArray.get(position).getAnimalGender().equals("Masculino")) {
            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.insemination_male));
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(AnimalActivity.this);
        LayoutInflater inflater = AnimalActivity.this.getLayoutInflater();
        final View vista = inflater.inflate(R.layout.animal_insemination_layout, null);
        final EditText txtLambingDate;
        final EditText txtNoStraws;
        final EditText txtSireId;
        final EditText txtInseminationComments;

        TextView txtEIDInsemination = (TextView) vista.findViewById(R.id.txtEIDInsemination);
        txtEIDInsemination.setText(animalsArray.get(position).getAnimalEID());
        txtLambingDate = (EditText) vista.findViewById(R.id.txtLambingDate);
        txtLambingDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.OpenDateDialog(txtLambingDate, AnimalActivity.this);
            }
        });
        txtNoStraws = (EditText) vista.findViewById(R.id.txtNoStraws);
        txtSireId = (EditText) vista.findViewById(R.id.txtSireId);
        txtInseminationComments = (EditText) vista.findViewById(R.id.txtInseminationComments);


        builder.setView(vista)
                .setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String lambingdate = txtLambingDate.getText().toString();
                        String straws = txtNoStraws.getText().toString();
                        String sireid = txtSireId.getText().toString();
                        String comments = txtInseminationComments.getText().toString();

                        if (lambingdate.isEmpty()) {
                            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.complete_lambingdate));
                            return;
                        }
                        if (straws.isEmpty()) {
                            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.complete_straws));
                            return;
                        }
                        if (comments.isEmpty()) {
                            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.complete_comments));
                            return;
                        }
                        AnimalInsemination process = new AnimalInsemination();
                        process.setAnimalId(animalsArray.get(position).getAnimalId());
                        process.setCalvingDate(lambingdate);
                        process.setSireId(sireid);
                        process.setComments(comments);
                        process.setStatus("ACTIVO");
                        process.setNoStraws(straws);
                        if (db.AddAnimalInsemination(process)) {
                            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.complete));
                        } else {
                            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.tryagain));
                        }
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }

    public void AnimalCondition(final int position) {
        ArrayAdapter<Medications> medicationsAdapter;
        ArrayList<Medications> medicationsArray;

        AlertDialog.Builder builder = new AlertDialog.Builder(AnimalActivity.this);
        LayoutInflater inflater = AnimalActivity.this.getLayoutInflater();
        final View vista = inflater.inflate(R.layout.animal_condition_layout, null);

        final EditText txtLastSeen;
        final EditText txtCondition;
        final EditText txtDiagnosis;
        final Spinner ddlMedicationCondition;
        final EditText txtAvailabe;
        final EditText txtDosage;
        TextView txtEidCondition = (TextView) vista.findViewById(R.id.txtEidCondition);
        txtEidCondition.setText(animalsArray.get(position).getAnimalEID());


        txtLastSeen = (EditText) vista.findViewById(R.id.txtLastSeen);
        txtCondition = (EditText) vista.findViewById(R.id.txtCondition);
        txtDiagnosis = (EditText) vista.findViewById(R.id.txtDiagnosis);
        txtAvailabe = (EditText) vista.findViewById(R.id.txtAvailabe);
        txtDosage = (EditText) vista.findViewById(R.id.txtDosage);


        ddlMedicationCondition = (Spinner) vista.findViewById(R.id.ddlMedicationCondition);
        medicationsArray = db.GetMedications();
        medicationsAdapter = new ArrayAdapter(this, R.layout.spinner, medicationsArray);
        ddlMedicationCondition.setAdapter(medicationsAdapter);
        txtLastSeen.setText(db.GetLastSeenCondition(animalsArray.get(position).getAnimalEID()));
        ddlMedicationCondition.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Medications medication = (Medications) ddlMedicationCondition.getSelectedItem();
                txtAvailabe.setText(String.valueOf(medication.getMedicationQuantity()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        builder.setView(vista)
                .setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String symptoms = "";
                        String condition = txtCondition.getText().toString();
                        String diagnosis = txtDiagnosis.getText().toString();
                        String medicine = ddlMedicationCondition.getSelectedItem().toString();
                        String lastSeen = txtLastSeen.getText().toString();
                        String dosage = txtDosage.getText().toString();
                        String available = txtAvailabe.getText().toString();

                        if (condition.isEmpty() || diagnosis.isEmpty() || medicine.isEmpty()) {
                            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.completeFields));
                            dialog.cancel();
                            return;
                        }

                        if (Double.valueOf(dosage) > Double.valueOf(available)) {
                            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.completeFields));
                            dialog.cancel();
                            return;
                        }

                        AnimalCondition animalCondition = new AnimalCondition();
                        animalCondition.setAnimalEID(animalsArray.get(position).getAnimalEID());
                        animalCondition.setAnimalVisualNumber(animalsArray.get(position).getAnimalVisualNumber());
                        animalCondition.setLastSeen(lastSeen);
                        animalCondition.setSymptoms(symptoms);
                        animalCondition.setCondition(condition);
                        animalCondition.setDiagnosis(diagnosis);
                        animalCondition.setMedication(medicine);
                        animalCondition.setDosage(dosage);

                        if (db.RegisterAnimalCondition(animalCondition)) {
                            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.complete));
                            db.UpdateMedicationDosage(medicine, dosage);

                        } else {
                            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.tryagain));
                        }

                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }

    @Override
    protected void onStart() {
        FillList();
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem item = menu.findItem(R.id.menuSearch);
        final SearchView searchView = (SearchView) item.getActionView();
        searchView.setQueryHint(getString(R.string.search_by_id));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                SearchAnimalFromSearchBar(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //adapter.getFilter().filter(newText);

                return false;
            }
        });
        return true;
    }

    private void SearchAnimalFromSearchBar(String query) {
        if (animalsArray.size() > 0)
            animalsArray.clear();
        animalsArray.addAll(db.GetAnimalsByID(query));
        if (animalsArray.isEmpty()) {
            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.No_data));
        } else {
            Tools.CreateSimpleDialog(AnimalActivity.this, "( " + animalsArray.size() + " ) " + getString(R.string.number_result));
        }
        adapter = new AnimalAdapter(AnimalActivity.this, animalsArray, AnimalActivity.this);
        animalList.setAdapter(adapter);
        getSupportActionBar().setSubtitle("Total : " + animalsArray.size());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        } else if (id == R.id.filterSearch) {
            OpenFilterDialog();
        } else if (id == R.id.homebtn) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.report) {

            if (animalsArray.size() <= 0) {
                Toast.makeText(AnimalActivity.this, R.string.norecords, Toast.LENGTH_LONG).show();
                return false;
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(AnimalActivity.this);
            builder.setTitle(getString(R.string.title));
            builder.setMessage(getString(R.string.send_email_report));
            builder.setPositiveButton(R.string.alertyes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String Data = animalsArray.get(0).title() + "\n";
                    for (Animal item : animalsArray) {
                        Animal exitem = db.GetAnimalByEid(item.getAnimalEID());
                        Data += exitem.toStringAll().replace("\n", "") + "\n";
                    }
                    Tools.SaveReport(AnimalActivity.this, Data, "smooperFileAnimalsStock" + Tools.GetDate(true) + ".txt");

                }
            });
            builder.setNegativeButton(R.string.alertno, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    dialog.dismiss();
                }
            });

            builder.create().show();
        } else if (id == R.id.searcByMachine) {
            Tools.ReadSeachFromBlueTooth(AnimalActivity.this, db, animalList, getSupportActionBar());
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    public void OpenFilterDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AnimalActivity.this);
        LayoutInflater inflater = AnimalActivity.this.getLayoutInflater();
        final View vista = inflater.inflate(R.layout.filter_layout, null);
        final Spinner ddlFilterType;
        final Spinner ddlFilterBreed;
        final Spinner ddlFilterFarm;
        final Spinner ddlFilterHerd;

        ddlFilterType = (Spinner) vista.findViewById(R.id.ddlFilterType);
        ddlFilterBreed = (Spinner) vista.findViewById(R.id.ddlFilterBreed);
        ddlFilterFarm = (Spinner) vista.findViewById(R.id.ddlFilterFarm);
        ddlFilterHerd = (Spinner) vista.findViewById(R.id.ddlFilterHerd);

        builder.setView(vista)
                .setPositiveButton(R.string.btn_filter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String type = ddlFilterType.getSelectedItem().toString();
                        String breed = ddlFilterBreed.getSelectedItem().toString();
                        String farm = ddlFilterFarm.getSelectedItem().toString();
                        String herd = ddlFilterHerd.getSelectedItem().toString();
                        if (animalsArray.size() > 0)
                            animalsArray.clear();
                        animalsArray.addAll(db.GetAnimalsFilter(type, breed, farm, herd));
                        if (animalsArray.isEmpty()) {
                            Tools.CreateSimpleDialog(AnimalActivity.this, getString(R.string.No_data));
                            return;
                        } else {
                            Tools.CreateSimpleDialog(AnimalActivity.this, "( " + animalsArray.size() + " ) " + getString(R.string.number_result));
                        }
                        getSupportActionBar().setSubtitle("Total : " + animalsArray.size());
                        adapter = new AnimalAdapter(AnimalActivity.this, animalsArray, AnimalActivity.this);
                        animalList.setAdapter(adapter);
                    }
                })
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        final DatabaseTools dbtools = new DatabaseTools(AnimalActivity.this);
        dbtools.setAdapterId(ddlFilterType, R.array.cattle_type);
        dbtools.setAdapter(ddlFilterFarm, "Farms", "FarmName", "");
        dbtools.setAdapter(ddlFilterHerd, "Herds", "HerdName", "");
        ddlFilterType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String ddlSelected = ddlFilterType.getSelectedItem().toString();
                String condition = "";
                if (!ddlSelected.equals(getString(R.string.all))) {
                    condition = "BreedType = '" + ddlSelected + "'";
                }
                dbtools.setAdapter(ddlFilterBreed, "Breeds", "BreedName", condition);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        builder.create().show();
    }

}
