package ingenieria.bci.co.administradordegrupos.View.Register;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.Animal;
import ingenieria.bci.co.administradordegrupos.Model.Farms;
import ingenieria.bci.co.administradordegrupos.Model.Herds;
import ingenieria.bci.co.administradordegrupos.R;
import ingenieria.bci.co.administradordegrupos.RegisterActivity;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

/**
 * Created by nicolas on 6/27/17.
 */

public class register_step3 extends Fragment {
    View vista;
    Database db;
    Spinner ddlHerd;
    Spinner ddlFarm;
    ImageView BtnPhoto;
    Button btnSaveAnimalFinal;
    Uri imageUri;
    Bitmap bitmap;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.register_step3, container, false);
        db = new Database(getActivity());
        setControls();
        return vista;
    }

    public void setControls() {
        ddlHerd = (Spinner) vista.findViewById(R.id.ddlHerd);
        ddlFarm = (Spinner) vista.findViewById(R.id.ddlFarm);
        BtnPhoto = (ImageView) vista.findViewById(R.id.BtnPhoto);
        btnSaveAnimalFinal = (Button) vista.findViewById(R.id.btnSaveAnimalFinal);

        ArrayList<Herds> herdList = db.GetHerds();
        ArrayList<String> herdListString = new ArrayList<>();
        for (Herds herd : herdList) {
            herdListString.add(herd.getHerdName());
        }
        herdListString.add(0, getString(R.string.choose_spinner));
        ArrayAdapter<String> spinnerArray = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, herdListString);
        spinnerArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ddlHerd.setAdapter(spinnerArray);

        ArrayList<Farms> farmList = db.GetFarms();
        ArrayList<String> farmListString = new ArrayList<>();
        for (Farms farm : farmList) {
            farmListString.add(farm.getFarmName());
        }
        farmListString.add(0, getString(R.string.choose_spinner));
        ArrayAdapter<String> farmAdapterArray = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, farmListString);
        farmAdapterArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ddlFarm.setAdapter(farmAdapterArray);


        BtnPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TakePicture();
            }
        });
        btnSaveAnimalFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveAnimal();
            }
        });
    }

    private void SaveAnimal() {
        String Herd = ddlHerd.getSelectedItem().toString();
        String Farm = ddlFarm.getSelectedItem().toString();
        if (ddlHerd.equals(getString(R.string.choose_spinner))) {
            Tools.CreateSimpleDialog(getActivity(), getString(R.string.setHerd));
            return;
        } else if (ddlHerd.equals(getString(R.string.choose_spinner))) {
            Tools.CreateSimpleDialog(getActivity(), getString(R.string.setFarm));
            return;
        }

        Animal animal = new Animal();
        animal.setAnimalEID(RegisterActivity.EId);
        animal.setAnimalVisualNumber(RegisterActivity.Vnumber);
        animal.setAnimalDateOfBirth(RegisterActivity.DateOfBirth);
        animal.setAnimalType(RegisterActivity.Type);
        animal.setAnimalGroup(RegisterActivity.Group);
        animal.setAnimalDestination(RegisterActivity.Destination);
        animal.setAnimalBreed(RegisterActivity.Breed);
        animal.setAnimalGender(RegisterActivity.Gender);
        animal.setAnimalBirthWeight(RegisterActivity.BirthWeight);
        animal.setAnimalCurrentWeight(RegisterActivity.CurrentWeight);
        animal.setAnimalDamId(RegisterActivity.DamId);
        animal.setAnimalDamGrandDamId(RegisterActivity.DamGrandDamId);
        animal.setAnimalDamGrandSireId(RegisterActivity.DamGrandSireId);
        animal.setAnimalSireId(RegisterActivity.SireId);
        animal.setAnimalSireGrandDamId(RegisterActivity.SireGrandDamId);
        animal.setAnimalSireGrandSireId(RegisterActivity.SireGrandSireId);
        animal.setAnimalEmbryo(RegisterActivity.IsEmbryo ? "1" : "0");
        animal.setAnimalSurrogateDamId(RegisterActivity.SurrogateDamId);
        animal.setAnimalHerd(Herd);
        animal.setAnimalFarm(Farm);
        animal.setAnimalPhoto(RegisterActivity.Photo);

        if (db.AddAnimal(animal)) {
            Tools.CreateContinueDialog(getActivity(), getString(R.string.AnimalSaved));
        } else {
            Tools.CreateSimpleDialog(getActivity(), getString(R.string.tryagain));
        }
    }

    private void TakePicture() {
        try {
            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA}, 0);
            }
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            Date cDate = new Date();
            String fDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cDate);
            String url = "Pic" + fDate + ".jpg";
            File photo = new File(getActivity().getObbDir(), url);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            if (android.os.Build.VERSION.SDK_INT >= 24){
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".ingenieria.bci.co.administradordegrupos",photo));
                imageUri =  FileProvider.getUriForFile(getActivity(), getActivity().getApplicationContext().getPackageName() + ".ingenieria.bci.co.administradordegrupos",photo);//Uri.fromFile(photo);

            } else{
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photo));
                imageUri = Uri.fromFile(photo);
            }
            startActivityForResult(intent, 100);
        } catch (Exception ex) {
            Tools.CreateSimpleDialog(getActivity(), ex.getMessage());
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 100:
                if (resultCode == Activity.RESULT_OK) {

                    try {
                        Uri selectedImage = imageUri;
                        getActivity().getContentResolver().notifyChange(selectedImage, null);
                        ContentResolver cr = getActivity().getContentResolver();
                        bitmap = android.provider.MediaStore.Images.Media
                                .getBitmap(cr, selectedImage);
                        RegisterActivity.Photo = imageUri.toString();
                        BtnPhoto.setImageBitmap(bitmap);
                    } catch (Exception e) {
                        Tools.CreateSimpleDialog(getActivity(), e.getMessage());
                    }
                }
        }
    }
}
