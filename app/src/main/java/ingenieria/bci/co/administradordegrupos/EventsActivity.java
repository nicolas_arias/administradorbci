package ingenieria.bci.co.administradordegrupos;

import android.app.AlertDialog;
import android.app.usage.UsageEvents;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Adapters.FarmListAdapter;
import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.Farms;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

public class EventsActivity extends AppCompatActivity {
    Database db;
    ArrayList<String> eventsArray;
    public ArrayAdapter<String> adapter;
    RelativeLayout NoDataPanel;
    FloatingActionButton fabAddEvent;
    ListView eventList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_events);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.Events));
        SetViews();
    }

    private void SetViews() {
        eventList = (ListView) findViewById(R.id.eventlist);
        fabAddEvent = (FloatingActionButton) findViewById(R.id.fabAddEvent);
        fabAddEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EventsActivity.this);
                LayoutInflater inflater = EventsActivity.this.getLayoutInflater();
                final View vista = inflater.inflate(R.layout.add_event, null);
                final EditText txtEventName = (EditText) vista.findViewById(R.id.txtEventName);
                builder.setView(vista)
                        .setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                String EventName = txtEventName.getText().toString();
                                if (EventName.isEmpty()) {
                                    Tools.CreateSimpleDialog(EventsActivity.this, getString(R.string.completeFields));
                                    return;
                                }
                                if (db.AddEvent(EventName)) {
                                    Tools.CreateSimpleDialog(EventsActivity.this, getString(R.string.complete));
                                    FillList();
                                } else {
                                    Tools.CreateSimpleDialog(EventsActivity.this, getString(R.string.tryagain));
                                }
                            }
                        }).setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
                builder.create().show();
            }
        });
    }

    private void FillList() {
        db = new Database(getBaseContext());
        NoDataPanel = (RelativeLayout) findViewById(R.id.NoDataPanel);
        eventsArray = null;
        eventsArray = db.GetEvents();
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, eventsArray);
        eventList.setAdapter(adapter);
        if (eventsArray.size() > 0) {
            NoDataPanel.setVisibility(View.INVISIBLE);

            SetLongPress();

        } else {
            NoDataPanel.setVisibility(View.VISIBLE);
        }
        adapter.notifyDataSetChanged();
    }

    private void SetLongPress() {
        eventList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView parent, View view, final int position, long id) {
                final CharSequence[] items = { getString(R.string.change_status)};
                final AlertDialog.Builder builder = new AlertDialog.Builder(EventsActivity.this);
                builder.setTitle(getString(R.string.title));
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            AlertDialog.Builder builderD = new AlertDialog.Builder(EventsActivity.this);
                            builderD.setTitle(getString(R.string.title));
                            builderD.setMessage(R.string.inactivate);
                            builderD.setPositiveButton(R.string.alertyes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    db.InactivateEvent(eventsArray.get(position).toString());
                                    dialog.dismiss();
                                    Toast.makeText(EventsActivity.this, R.string.complete, Toast.LENGTH_LONG);
                                    try {
                                        FillList();
                                    } catch (Exception e) {

                                    }
                                }
                            });
                            builderD.setNegativeButton(R.string.alertno, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builderD.show();
                        }
                    }
                });
                builder.show();
                return false;
            }
        });
    }

    @Override
    protected void onStart() {
        FillList();
        super.onStart();
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
