package ingenieria.bci.co.administradordegrupos.Data;

/**
 * Created by Nicolas on 17/06/2017.
 */

public class DatabaseTables {
    public static final String AnimalTable = "create table Animal "
            + "(AnimalId INTEGER PRIMARY KEY AUTOINCREMENT,"
            + " AnimalEID TEXT,"
            + " AnimalVisualNumber TEXT,"
            + " AnimalDateOfBirth TEXT,"
            + " AnimalType TEXT,"
            + " AnimalGroup TEXT,"
            + " AnimalDestination TEXT,"
            + " AnimalBreed TEXT,"
            + " AnimalGender TEXT,"
            + " AnimalBirthWeight TEXT,"
            + " AnimalCurrentWeight TEXT,"
            + " AnimalDamId TEXT,"
            + " AnimalDamGrandDamId TEXT,"
            + " AnimalDamGrandSireId TEXT,"
            + " AnimalSireId TEXT,"
            + " AnimalSireGrandDamId TEXT,"
            + " AnimalSireGrandSireId TEXT,"
            + " AnimalEmbryo TEXT,"
            + " AnimalSurrogateDamId TEXT, "
            + " AnimalHerd TEXT,"
            + " AnimalFarm TEXT,"
            + " AnimalPhoto TEXT,"
            + " AnimalStatus TEXT,"
            + " Status TEXT,"
            + " AnimalW12 TEXT,"
            + " AnimalW18 TEXT,"
            + " AnimalW24 TEXT)";
    public static final String MedicationsTable = " create table Medications " +
            "(MedicationId INTEGER PRIMARY KEY AUTOINCREMENT," +
            "MedicationName TEXT," +
            "MedicationPrice NUMERIC," +
            "MedicationQuantity NUMERIC," +
            "MedicationExpiryDate TEXT," +
            "MedicationSupplierName TEXT," +
            "MedicationSupplierTelephone TEXT," +
            "MedicationStatus TEXT) ";
    public static final String UsersTable = "create table Users " +
            "(UserId INTEGER PRIMARY KEY AUTOINCREMENT," +
            "UserName TEXT," +
            "UserPassword TEXT," +
            "UserEmail TEXT," +
            "UserLicence TEXT," +
            "UserStatus TEXT," +
            "UserExpireDate) ";
    public static final String HerdsTable = " create table Herds " +
            "(HerdId INTEGER PRIMARY KEY AUTOINCREMENT," +
            "HerdName TEXT," +
            "HerdStatus TEXT) ";
    public static final String FarmsTable = " create table Farms " +
            "(FarmId INTEGER PRIMARY KEY AUTOINCREMENT," +
            "FarmName TEXT," +
            "FarmStatus TEXT," +
            "FarmSize NUMERIC," +
            "FarmLng TEXT," +
            "FarmLat TEXT) ";
    public static final String BreedsTable = " create table Breeds " +
            "(BreedId INTEGER PRIMARY KEY AUTOINCREMENT," +
            "BreedName TEXT," +
            "BreedType TEXT," +
            "BreedDestination TEXT," +
            "BreedStatus TEXT) ";
    public static final String AnimalTypeTable = " create table AnimalType " +
            "(AnimalTypeId  INTEGER PRIMARY KEY AUTOINCREMENT," +
            "AnimalTypeName TEXT," +
            "AnimalTypeStatus TEXT) ";
    public static final String AnimalChageStatusTable = "create table ChangeStatus (" +
            "ChangeId INTEGER PRIMARY KEY AUTOINCREMENT," +
            "ChangeDescription TEXT," +
            "ChangeDate TEXT," +
            "AnimalId INTEGER)";
    public static final String AnimalInsemination = " create table AnimalInsemination (" +
            "InseminationId INTEGER PRIMARY KEY AUTOINCREMENT," +
            "AnimalId INTEGER," +
            "CalvingDate TEXT," +
            "NoStraws INTEGER," +
            "SireId TEXT," +
            "Comments TEXT," +
            "Status TEXT)";
    public static final String AnimalSales = "create table AnimalSales (" +
            " AnimalSaleId INTEGER PRIMARY KEY AUTOINCREMENT," +
            " AnimalSaleSecuence INTEGER," +
            " AnimalEID TEXT," +
            " SaleDate TEXT," +
            " PaymentType TEXT," +
            " Buyer TEXT," +
            " Reference TEXT," +
            " Price TEXT)";
    public static final String AnimalFlushing = "create table AnimalFlushing (" +
            "FlushingId  INTEGER PRIMARY KEY AUTOINCREMENT," +
            "AnimalEID TEXT," +
            "AnimalVisualNumber TEXT," +
            "DateFlushed TEXT," +
            "NoEmbryos TEXT," +
            "Comments TEXT)";
    public static final String AnimalDipping = "create table AnimalDipping (" +
            "DippingId INTEGER PRIMARY KEY AUTOINCREMENT," +
            "AnimalEID TEXT," +
            "Date TEXT," +
            "Method TEXT," +
            "Comments TEXT," +
            "Secuence INTEGER)";
    public static final String AnimalDisease = "create table AnimalDisease (" +
            "RegisterId INTEGER PRIMARY KEY AUTOINCREMENT," +
            "AnimalEID TEXT," +
            "Date TEXT," +
            "Comments TEXT," +
            "DiseaseType TEXT," +
            "Secuence INTEGER)";
    public static final String AnimalVaccine = "create table AnimalVaccine (" +
            "RegisterId INTEGER PRIMARY KEY AUTOINCREMENT," +
            "DosageVolume TEXT," +
            "Date TEXT," +
            "Comments TEXT," +
            "Secuence INTEGER," +
            "Vaccine TEXT," +
            "AnimalEID TEXT)";
    public static final String AnimalCondition = "create table AnimalCondition (" +
            "RegisterId INTEGER PRIMARY KEY AUTOINCREMENT," +
            "AnimalEID TEXT," +
            "AnimalVisualNumber TEXT," +
            "LastSeen TEXT," +
            "Symptoms TEXT," +
            "Condition TEXT," +
            "Diagnosis TEXT," +
            "Medication TEXT," +
            "Dosage TEXT," +
            "Date TEXT)";
    public static final String AnimalWeighHistory = "create table AnimalWeigh (" +
            "RegisterId INTEGER PRIMARY KEY AUTOINCREMENT," +
            "AnimalEID TEXT," +
            "Weight TEXT," +
            "OldWeight TEXT," +
            "Date TEXT," +
            "Event TEXT)";
    public static final String AnimalCountHistory = "create table AnimalCount (" +
            "CountId INTEGER PRIMARY KEY AUTOINCREMENT," +
            "AnimalEID TEXT," +
            "Date TEXT," +
            "Event TEXT)";
    public static final String Events = "create table Events (" +
            "EventName TEXT PRIMARY KEY," +
            "EventState TEXT)";

}