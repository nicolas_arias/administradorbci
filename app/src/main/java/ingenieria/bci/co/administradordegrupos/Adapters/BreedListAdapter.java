package ingenieria.bci.co.administradordegrupos.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Model.Breeds;
import ingenieria.bci.co.administradordegrupos.Model.Farms;
import ingenieria.bci.co.administradordegrupos.R;

/**
 * Created by nicolas on 6/19/17.
 */

public class BreedListAdapter extends ArrayAdapter<Breeds> {
    final Context mainContext;
    final Activity activity;

    public BreedListAdapter(Context context, ArrayList<Breeds> breeds, Activity activity) {
        super(context, 0, breeds);
        this.mainContext = context;
        this.activity = activity;
    }

    private static class ViewHolder {
        TextView lblBreedName;
        TextView lblBreedType;
        TextView lblBreedDestination;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Breeds breed = getItem(position);

        BreedListAdapter.ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new BreedListAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.breed_list, parent, false);
            viewHolder.lblBreedName = (TextView) convertView.findViewById(R.id.lblBreedName);
            viewHolder.lblBreedType = (TextView) convertView.findViewById(R.id.lblBreedType);
            viewHolder.lblBreedDestination = (TextView) convertView.findViewById(R.id.lblBreedDestination);
            viewHolder.lblBreedName.setText(breed.getBreedName());
            viewHolder.lblBreedType.setText(breed.getBreedType());
            viewHolder.lblBreedDestination.setText(breed.getBreedDestination());

        } else {
            viewHolder = (BreedListAdapter.ViewHolder) convertView.getTag();
        }
        return convertView;
    }
}
