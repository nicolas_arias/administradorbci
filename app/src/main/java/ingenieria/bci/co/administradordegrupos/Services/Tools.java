package ingenieria.bci.co.administradordegrupos.Services;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.channels.FileChannel;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import ingenieria.bci.co.administradordegrupos.Adapters.AnimalAdapter;
import ingenieria.bci.co.administradordegrupos.AnimalActivity;
import ingenieria.bci.co.administradordegrupos.ConfigActivity;
import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.MainActivity;
import ingenieria.bci.co.administradordegrupos.MedicationActivity;
import ingenieria.bci.co.administradordegrupos.Model.Animal;
import ingenieria.bci.co.administradordegrupos.R;
import rid.ptdevice.BTManager;
import rid.ptdevice.FDXB;
import rid.ptdevice.ISO11784;
import rid.ptdevice.TTFParser;
import rid.ptdevice.Tag;


/**
 * Created by nicolas on 6/16/17.
 */

public class Tools {

    public static void CreateSimpleDialog(Context context, String Message) {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.title));
        builder.setMessage(Message);
        builder.setPositiveButton(R.string.btn_close, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public static void CreateToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void CreateContinueDialog(final Context context, String Message) {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.title));
        builder.setMessage(Message);
        builder.setPositiveButton(R.string.btn_continue, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(context, MainActivity.class);
                context.startActivity(intent);
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    public static ArrayList<String> arrayToList(String[] array) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            arrayList.add(array[i]);
        }
        return arrayList;
    }

    public static void SetAdapters(Context context, ArrayList<String> array, Spinner spinner) {
        ArrayAdapter<String> spinnerArray = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, array);
        spinnerArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArray);
    }

    public static String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return encoded;
    }

    public static void selectSpinnerItemByValue(Spinner spnr, String value) {
        ArrayAdapter<String> adapter = (ArrayAdapter<String>) spnr.getAdapter();
        for (int position = 0; position < adapter.getCount(); position++) {
            if (adapter.getItem(position).toString().equals(value)) {
                spnr.setSelection(position);
                return;
            }
        }
    }

    public static void OpenDateDialog(final EditText txtDob, Context context) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        ;
        DatePickerDialog mdiDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                int month = monthOfYear + 1;
                int day = dayOfMonth;
                txtDob.setText(year + "-" + (month < 10 ? "0" + month : String.valueOf(month)) + "-" + (day < 10 ? "0" + day : String.valueOf(day)));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        mdiDialog.show();
    }

    public static void ReadWeightFromBlueTooth(final Activity context, final EditText txtWeight, final ImageButton trigger) {
        final ProgressDialog dialog = new ProgressDialog(context);
        dialog.setTitle(context.getResources().getString(R.string.bci));
        dialog.setMessage(context.getResources().getString(R.string.loadingTitle));
        txtWeight.setOnClickListener(null);
        final Bluetooth bluetooth = new Bluetooth(context);
        Bluetooth.isWeight = true;
        bluetooth.enableBluetooth();
        txtWeight.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Bluetooth.isWeight = true;
                if (!hasFocus) {
                    if (bluetooth.isConnected())
                        bluetooth.disconnect();
                }
            }
        });
        txtWeight.requestFocus();
        bluetooth.setCommunicationCallback(new Bluetooth.CommunicationCallback() {
            @Override
            public void onConnect(BluetoothDevice device) {


            }

            @Override
            public void onDisconnect(BluetoothDevice device, String message) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                });
            }

            @Override
            public void onMessage(final String message) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            txtWeight.setText(message);
                            trigger.setEnabled(true);
                            dialog.dismiss();
                        } catch (Exception e) {

                        }
                    }
                });
            }

            @Override
            public void onError(final String message) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                        trigger.setEnabled(true);
                    }
                });
            }

            @Override
            public void onConnectError(BluetoothDevice device, final String message) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        trigger.setEnabled(true);
                    }
                });
            }
        });
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.show();
                bluetooth.connectToName(GetDefaultSetting(context, "btweight"));
            }
        });
    }

    public static Bluetooth ReadIdentificationFromBlueTooth(final Activity context, final EditText txtEID, final ImageButton btn) {
        final ProgressDialog dialog = new ProgressDialog(context);
        dialog.setTitle(context.getResources().getString(R.string.bci));
        dialog.setMessage(context.getResources().getString(R.string.loadingTitle));
        txtEID.setOnClickListener(null);
        final Bluetooth bluetooth = new Bluetooth(context);
        Bluetooth.isWeight = false;
        bluetooth.enableBluetooth();
        bluetooth.removeCommunicationCallback();
        txtEID.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Bluetooth.isWeight = false;
                if (!hasFocus) {
                    if (bluetooth.isConnected())
                        bluetooth.disconnect();
                } else {
                    if (!bluetooth.isConnected())
                        context.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (!bluetooth.isConnected()) {
                                    bluetooth.connectToName(GetDefaultSetting(context, "btid"));
                                }
                            }
                        });
                }
            }

        });
        bluetooth.setCommunicationCallback(new Bluetooth.CommunicationCallback() {
            @Override
            public void onConnect(BluetoothDevice device) {


            }

            @Override
            public void onDisconnect(BluetoothDevice device, String message) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                });
            }

            @Override
            public void onMessage(final String message) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String eid = BTManager.TagMonitor.parseRecord(message).getId();
                            Object sr = TTFParser.parseID("FDXB", eid);
                            txtEID.setText(sr.toString());
                            if (btn != null) {
                                btn.callOnClick();
                            }
                            dialog.dismiss();
                        } catch (Exception e) {

                        }
                    }
                });
            }

            @Override
            public void onError(final String message) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
            }

            @Override
            public void onConnectError(BluetoothDevice device, final String message) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                    }
                });

            }
        });
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                txtEID.requestFocus();
            }
        });
        return bluetooth;
    }

    public static void ReadSeachFromBlueTooth(final Activity context, final Database db, final ListView animalList, final ActionBar actionBar) {
        final ProgressDialog dialog = new ProgressDialog(context);
        dialog.setTitle(context.getResources().getString(R.string.bci));
        dialog.setMessage(context.getResources().getString(R.string.loadingTitle));

        final Bluetooth bluetooth = new Bluetooth(context);
        Bluetooth.isWeight = false;
        bluetooth.enableBluetooth();
        bluetooth.setCommunicationCallback(new Bluetooth.CommunicationCallback() {
            @Override
            public void onConnect(BluetoothDevice device) {


            }

            @Override
            public void onDisconnect(BluetoothDevice device, String message) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                });
            }

            @Override
            public void onMessage(final String message) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AnimalAdapter adapter;
                        String eid = BTManager.TagMonitor.parseRecord(message).getId();
                        Object sr = TTFParser.parseID("FDXB", eid);
                        bluetooth.disconnect();

                        ArrayList<Animal> animalsArray = new ArrayList<Animal>();
                        animalsArray.addAll(db.GetAnimalsByID(sr.toString()));
                        if (animalsArray.isEmpty()) {
                            Tools.CreateSimpleDialog(context, context.getString(R.string.No_data));
                        } else {
                            Tools.CreateSimpleDialog(context, "( " + animalsArray.size() + " ) " + context.getString(R.string.number_result));
                        }
                        adapter = new AnimalAdapter(context, animalsArray, context);
                        animalList.setAdapter(adapter);
                        actionBar.setSubtitle("Total : " + animalsArray.size());
                        dialog.dismiss();
                    }
                });
            }

            @Override
            public void onError(final String message) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
            }

            @Override
            public void onConnectError(BluetoothDevice device, final String message) {
                context.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
            }
        });
        context.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialog.show();
                bluetooth.connectToName(GetDefaultSetting(context, "btid"));

            }
        });
    }

    public static String GetDate(boolean hour) {
        Date cDate = new Date();
        if (hour)
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(cDate);
        else
            return new SimpleDateFormat("yyyy-MM-dd").format(cDate);
    }

    public static String GetDefaultSetting(Context context, String key) {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
        return SP.getString(key, "");
    }

    public static void SetDefaultSettings(Context context, String key, String value) {
        SharedPreferences SP = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = SP.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public static String GetCurrency(Context context) {
        String val = GetDefaultSetting(context, "currency");
        if (val.equals("1"))
            return "COP";
        else
            return "USD";
    }

    public static String GetunitsWeight(Context context) {
        String val = GetDefaultSetting(context, "units");
        if (val.equals("1"))
            return "Kg";
        else
            return "Lb";
    }

    public static String GetunitsL(Context context) {
        String val = GetDefaultSetting(context, "units");
        if (val.equals("1"))
            return "ml";
        else
            return "ml";
    }

    public static String GetunitsM(Context context) {
        String val = GetDefaultSetting(context, "units");
        if (val.equals("1"))
            return "Mt";
        else
            return "Mi";
    }

    public static String GetLanguage(Context context) {
        String val = GetDefaultSetting(context, "language");
        if (val.equals("1"))
            return "ES";
        else
            return "EN";
    }

    public static String Number(String val) {
        NumberFormat nf = NumberFormat.getInstance(Locale.US);
        try {
            return nf.parse(val).toString();
        } catch (ParseException e) {
            return "0";
        }
    }

    public static String NumberW(String val, Context context) {
        NumberFormat nf = NumberFormat.getInstance(Locale.US);
        try {
            return nf.parse(val).toString() + " " + GetunitsWeight(context);
        } catch (ParseException e) {
            return "0";
        }
    }

    public static void ExportByEmail(Context context, String fileName) {

        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("text/html");
        //emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"me@gmail.com"});
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, context.getResources().getString(R.string.summary));
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Smooper Ganadero");
        Uri file = Uri.parse("file://" + fileName);
        emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        emailIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        emailIntent.putExtra(Intent.EXTRA_STREAM, file);
        context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
    }

    public static void SaveReport(Context context, String table, String fileName) {
        try {
            fileName = fileName.replace(":", "");
            //File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS);
            File path = context.getObbDir();//Environment.getDataDirectory();
            File file = new File(path, fileName);
            FileOutputStream stream = new FileOutputStream(file);
            try {
                stream.write(table.getBytes());
            } finally {
                stream.close();
            }
            ExportByEmail(context, file.getAbsolutePath());
        } catch (FileNotFoundException e) {
            CreateSimpleDialog(context, context.getResources().getString(R.string.fileNotFound));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String BackUpDataBase(Context context) {
        try {
            final String inFileName = context.getDatabasePath("administradorbci").getPath();
            File dbFile = new File(inFileName);
            FileInputStream fis = new FileInputStream(dbFile);

            String outFileName = Environment.getExternalStorageDirectory() + "/administradorbci.db";
            OutputStream output = new FileOutputStream(outFileName);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer)) > 0) {
                output.write(buffer, 0, length);
            }

            // Close the streams
            output.flush();
            output.close();
            fis.close();
            return inFileName;
        } catch (Exception e) {
            return "";
        }

    }

    public static boolean BackUpRestore(Activity context) {

        try {
            final String inFileName = Environment.getExternalStorageDirectory() + "/administradorbci.db";
            File dbFile = new File(inFileName);
            FileInputStream fis = new FileInputStream(dbFile);

            String outFileName = context.getDatabasePath("administradorbci").getParent();
            outFileName += "/administradorbci";
            File dbFileOld = new File(outFileName);

            if (dbFileOld.delete()) {
                OutputStream output = new FileOutputStream(outFileName);

                byte[] buffer = new byte[1024];
                int length;
                while ((length = fis.read(buffer)) > 0) {
                    output.write(buffer, 0, length);
                }

                // Close the streams
                output.flush();
                output.close();
                fis.close();

                Intent i = context.getPackageManager()
                        .getLaunchIntentForPackage(context.getPackageName());

                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.finish();
                context.startActivity(i);
                System.exit(0);
            }


            return true;
        } catch (Exception e) {
            return false;
        }
    }

}

