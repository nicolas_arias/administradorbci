package ingenieria.bci.co.administradordegrupos;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import ingenieria.bci.co.administradordegrupos.Model.ReportBreed;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;

public class ManageActivity extends AppCompatActivity {

    ImageView btnAnimalList;
    ImageView BtnSale;
    ImageView btnDeworming;
    ImageView btnMedicate;
    ImageView btnReport;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.manage);
        setContentView(R.layout.activity_manage);
        setcontrols();
    }

    private void setcontrols() {
        btnAnimalList = (ImageView) findViewById(R.id.btnAnimalList);
        btnDeworming = (ImageView) findViewById(R.id.btnDeworming);
        btnMedicate = (ImageView) findViewById(R.id.btnMedicate);
        BtnSale = (ImageView) findViewById(R.id.BtnSale);
        btnReport = (ImageView) findViewById(R.id.btnReport);
        BtnSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManageActivity.this, MoveActivity.class);
                startActivity(intent);
            }
        });
        btnAnimalList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManageActivity.this, AnimalActivity.class);
                startActivity(intent);
            }
        });
        btnDeworming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManageActivity.this, DippingActivity.class);
                startActivity(intent);
            }
        });
        btnMedicate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = {
                        getString(R.string.report_disease),
                        getString(R.string.apply_vaccinate)};
                final AlertDialog.Builder builder = new AlertDialog.Builder(ManageActivity.this);
                builder.setTitle(getString(R.string.title));
                builder.setItems(items, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which) {
                            case 0:
                                Intent intent = new Intent(ManageActivity.this, DiseaseActivity.class);
                                startActivity(intent);
                                break;
                            case 1:
                                Intent intent2 = new Intent(ManageActivity.this, VaccunateActivity.class);
                                startActivity(intent2);
                                break;
                        }
                    }
                });
                builder.show();
            }
        });
        btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] items = {
                        getString(R.string.report_farms),
                        getString(R.string.report_herds),
                        getString(R.string.report_count),
                        getString(R.string.report_weight)};

                final AlertDialog.Builder builder = new AlertDialog.Builder(ManageActivity.this);
                builder.setTitle(getString(R.string.title));
                builder.setItems(items, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        switch (which) {
                            case 0:
                                Intent intent = new Intent(ManageActivity.this, ReportActivity.class);
                                intent.putExtra("tipo", "farm");
                                startActivity(intent);
                                break;
                            case 1:
                                Intent intent2 = new Intent(ManageActivity.this, ReportActivity.class);
                                intent2.putExtra("tipo", "herd");
                                startActivity(intent2);
                                break;
                            case 2:
                                Intent intent3 = new Intent(ManageActivity.this, CountWeighListActivity.class);
                                intent3.putExtra("tipo", "count");
                                startActivity(intent3);
                                break;
                            case 3:
                                Intent intent4 = new Intent(ManageActivity.this, CountWeighListActivity.class);
                                intent4.putExtra("tipo", "weight");
                                startActivity(intent4);
                                break;
                        }
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
