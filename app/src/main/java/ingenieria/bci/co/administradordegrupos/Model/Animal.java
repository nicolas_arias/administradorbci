package ingenieria.bci.co.administradordegrupos.Model;

/**
 * Created by nicolas on 6/16/17.
 */

public class Animal {
    public int AnimalId;
    public String AnimalEID;
    public String AnimalVisualNumber;
    public String AnimalDateOfBirth;
    public String AnimalType;
    public String AnimalGroup;
    public String AnimalDestination;
    public String AnimalBreed;
    public String AnimalGender;
    public String AnimalBirthWeight;
    public String AnimalCurrentWeight;
    public String AnimalDamId;
    public String AnimalDamGrandDamId;
    public String AnimalDamGrandSireId;
    public String AnimalSireId;
    public String AnimalSireGrandDamId;
    public String AnimalSireGrandSireId;
    public String AnimalEmbryo;
    public String AnimalSurrogateDamId;
    public String AnimalHerd;
    public String AnimalFarm;
    public String AnimalStatus;
    public String AnimalPhoto;

    public String AnimalW12;
    public String AnimalW18;
    public String AnimalW24;

    public String title() {
        return "ANIMALID;" +
                "VISUALNUMBER;" +
                "BIRTHDATE;" +
                "TYPE;" +
                "GROUP;" +
                "DESTINATION;" +
                "BREED;" +
                "GENDER;" +
                "BIRTHWEIGHT;" +
                "CURRENTWEIGHT;" +
                "DAMID;" +
                "SIREID;" +
                "EMBRYO;" +
                "SURROGATE;" +
                "HERD;" +
                "FARM;" +
                "STATUS;" +
                "W12;" +
                "W18;" +
                "W24";
    }

    public String toStringAll() {
        return getAnimalEID() + ";" +
                getAnimalVisualNumber() + ";" +
                getAnimalDateOfBirth() + ";" +
                getAnimalType() + ";" +
                getAnimalGroup() + ";" +
                getAnimalDestination() + ";" +
                getAnimalBreed() + ";" +
                getAnimalGender() + ";" +
                getAnimalBirthWeight().replace("\n", "") + ";" +
                getAnimalCurrentWeight() + ";" +
                getAnimalDamId() + ";" +
                getAnimalSireId() + ";" +
                getAnimalEmbryo() + ";" +
                getAnimalSurrogateDamId() + ";" +
                getAnimalHerd() + ";" +
                getAnimalFarm() + ";" +
                getAnimalStatus() + ";" +
                getAnimalW12() + ";" +
                getAnimalW18() + ";" +
                getAnimalW24();
    }


    public String getAnimalW12() {
        return AnimalW12;
    }

    public void setAnimalW12(String animalW12) {
        AnimalW12 = animalW12;
    }

    public String getAnimalW18() {
        return AnimalW18;
    }

    public void setAnimalW18(String animalW18) {
        AnimalW18 = animalW18;
    }

    public String getAnimalW24() {
        return AnimalW24;
    }

    public void setAnimalW24(String animalW24) {
        AnimalW24 = animalW24;
    }

    public String toString() {
        return getAnimalEID();
    }

    public String getAnimalPhoto() {
        return AnimalPhoto;
    }

    public void setAnimalPhoto(String animalPhoto) {
        AnimalPhoto = animalPhoto;
    }

    public int getAnimalId() {
        return AnimalId;
    }

    public void setAnimalId(int animalId) {
        AnimalId = animalId;
    }

    public String getAnimalEID() {
        return AnimalEID;
    }

    public void setAnimalEID(String animalEID) {
        AnimalEID = animalEID;
    }

    public String getAnimalVisualNumber() {
        return AnimalVisualNumber;
    }

    public void setAnimalVisualNumber(String animalVisualNumber) {
        AnimalVisualNumber = animalVisualNumber;
    }

    public String getAnimalDateOfBirth() {
        return AnimalDateOfBirth;
    }

    public void setAnimalDateOfBirth(String animalDateOfBirth) {
        AnimalDateOfBirth = animalDateOfBirth;
    }

    public String getAnimalType() {
        return AnimalType;
    }

    public void setAnimalType(String animalType) {
        AnimalType = animalType;
    }

    public String getAnimalGroup() {
        return AnimalGroup;
    }

    public void setAnimalGroup(String animalGroup) {
        AnimalGroup = animalGroup;
    }

    public String getAnimalDestination() {
        return AnimalDestination;
    }

    public void setAnimalDestination(String animalDestination) {
        AnimalDestination = animalDestination;
    }

    public String getAnimalBreed() {
        return AnimalBreed;
    }

    public void setAnimalBreed(String animalBreed) {
        AnimalBreed = animalBreed;
    }

    public String getAnimalGender() {
        return AnimalGender;
    }

    public void setAnimalGender(String animalGender) {
        AnimalGender = animalGender;
    }

    public String getAnimalBirthWeight() {
        return AnimalBirthWeight;
    }

    public void setAnimalBirthWeight(String animalBirthWeight) {
        AnimalBirthWeight = animalBirthWeight;
    }

    public String getAnimalCurrentWeight() {
        return AnimalCurrentWeight;
    }

    public void setAnimalCurrentWeight(String animalCurrentWeight) {
        AnimalCurrentWeight = animalCurrentWeight;
    }

    public String getAnimalDamId() {
        return AnimalDamId;
    }

    public void setAnimalDamId(String animalDamId) {
        AnimalDamId = animalDamId;
    }

    public String getAnimalDamGrandDamId() {
        return AnimalDamGrandDamId;
    }

    public void setAnimalDamGrandDamId(String animalDamGrandDamId) {
        AnimalDamGrandDamId = animalDamGrandDamId;
    }

    public String getAnimalDamGrandSireId() {
        return AnimalDamGrandSireId;
    }

    public void setAnimalDamGrandSireId(String animalDamGrandSireId) {
        AnimalDamGrandSireId = animalDamGrandSireId;
    }

    public String getAnimalSireId() {
        return AnimalSireId;
    }

    public void setAnimalSireId(String animalSireId) {
        AnimalSireId = animalSireId;
    }

    public String getAnimalSireGrandDamId() {
        return AnimalSireGrandDamId;
    }

    public void setAnimalSireGrandDamId(String animalSireGrandDamId) {
        AnimalSireGrandDamId = animalSireGrandDamId;
    }

    public String getAnimalSireGrandSireId() {
        return AnimalSireGrandSireId;
    }

    public void setAnimalSireGrandSireId(String animalSireGrandSireId) {
        AnimalSireGrandSireId = animalSireGrandSireId;
    }

    public String getAnimalEmbryo() {
        return AnimalEmbryo;
    }

    public void setAnimalEmbryo(String animalEmbryo) {
        AnimalEmbryo = animalEmbryo;
    }

    public String getAnimalSurrogateDamId() {
        return AnimalSurrogateDamId;
    }

    public void setAnimalSurrogateDamId(String animalSurrogateDamId) {
        AnimalSurrogateDamId = animalSurrogateDamId;
    }

    public String getAnimalHerd() {
        return AnimalHerd;
    }

    public void setAnimalHerd(String animalHerd) {
        AnimalHerd = animalHerd;
    }

    public String getAnimalFarm() {
        return AnimalFarm;
    }

    public void setAnimalFarm(String animalFarm) {
        AnimalFarm = animalFarm;
    }

    public String getAnimalStatus() {
        return AnimalStatus;
    }

    public void setAnimalStatus(String animalStatus) {
        AnimalStatus = animalStatus;
    }


}
