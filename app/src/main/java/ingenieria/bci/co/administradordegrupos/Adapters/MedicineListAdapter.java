package ingenieria.bci.co.administradordegrupos.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Model.Herds;
import ingenieria.bci.co.administradordegrupos.Model.Medications;
import ingenieria.bci.co.administradordegrupos.R;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

/**
 * Created by nicolas on 6/24/17.
 */

public class MedicineListAdapter extends ArrayAdapter<Medications> {
    final Context mainContext;
    final Activity activity;

    public MedicineListAdapter(Context context, ArrayList<Medications> medications, Activity activity) {
        super(context, R.layout.medicine_list, medications);
        this.mainContext = context;
        this.activity = activity;
    }

    private static class ViewHolder {
        TextView lblMedicineName;
        TextView lblMedicineQty;
        TextView lblMedicineBn;
        TextView lblMedicinePr;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Medications medications = getItem(position);

        MedicineListAdapter.ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new MedicineListAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.medicine_list, parent, false);

            viewHolder.lblMedicineName = (TextView) convertView.findViewById(R.id.lblMedicineName);
            viewHolder.lblMedicineQty = (TextView) convertView.findViewById(R.id.lblMedicineQty);
            viewHolder.lblMedicineBn = (TextView) convertView.findViewById(R.id.lblMedicineBn);
            viewHolder.lblMedicinePr = (TextView) convertView.findViewById(R.id.lblMedicinePr);

            viewHolder.lblMedicineName.setText(medications.getMedicationName());
            viewHolder.lblMedicineQty.setText(String.valueOf(activity.getResources().getString(R.string.wquantity) + ":\t" + Tools.Number(String.valueOf(medications.getMedicationQuantity()))));
            viewHolder.lblMedicinePr.setText(String.valueOf(activity.getResources().getString(R.string.wprice) + ":\t" + Tools.GetCurrency(activity) + " " + Tools.Number(String.valueOf(medications.getMedicationPrice()))));
            viewHolder.lblMedicineBn.setText(String.valueOf("ID:\t" + medications.getMedicationId()));

        } else {
            viewHolder = (MedicineListAdapter.ViewHolder) convertView.getTag();
        }
        return convertView;
    }
}
