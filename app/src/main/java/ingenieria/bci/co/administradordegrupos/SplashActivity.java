package ingenieria.bci.co.administradordegrupos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");

        } catch (Exception e) {
        }
        setContentView(R.layout.activity_splash);
        if (Tools.GetDefaultSetting(SplashActivity.this, "user").isEmpty()) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
        finish();
    }
}
