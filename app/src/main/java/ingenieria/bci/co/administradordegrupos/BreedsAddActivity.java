package ingenieria.bci.co.administradordegrupos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.Breeds;
import ingenieria.bci.co.administradordegrupos.Model.Farms;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;

public class BreedsAddActivity extends AppCompatActivity {

    EditText txtBreedName;
    Spinner ddlBreedDestination;
    Spinner ddlAnimalType;
    Button btnSaveBreed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_breeds_add);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.breedsAddTitle);
        txtBreedName = (EditText) findViewById(R.id.txtBreedName);
        ddlBreedDestination = (Spinner) findViewById(R.id.ddlBreedDestination);
        ddlAnimalType = (Spinner) findViewById(R.id.ddlAnimalType);
        btnSaveBreed = (Button) findViewById(R.id.btnSaveBreed);


        ArrayAdapter<String> spinnerType = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.cattle_type));
        spinnerType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // ------------ //
        ddlAnimalType.setAdapter(spinnerType);
        ddlAnimalType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    SetDestinationddl(R.array.cattle_destination);
                } else if (position == 1) {
                    SetDestinationddl(R.array.goat_destination);
                } else if (position == 2) {
                    SetDestinationddl(R.array.sheep_destination);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        setListener();
    }


    public void SetDestinationddl(int Id) {
        ArrayAdapter<String> spinnerDestination = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(Id));
        spinnerDestination.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ddlBreedDestination.setAdapter(spinnerDestination);
    }

    private void setListener() {
        btnSaveBreed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Name = txtBreedName.getText().toString();
                String Destination = ddlBreedDestination.getSelectedItem().toString();
                String Type = ddlAnimalType.getSelectedItem().toString();

                if (Name.isEmpty() || Destination.isEmpty() || Type.isEmpty()) {
                    Toast.makeText(BreedsAddActivity.this, R.string.completeFields, Toast.LENGTH_LONG).show();
                    return;
                } else {
                    Database db = new Database(BreedsAddActivity.this);
                    Breeds breeds = new Breeds();
                    breeds.setBreedName(Name);
                    breeds.setBreedDestination(Destination);
                    breeds.setBreedType(Type);
                    if (db.AddBreed(breeds)) {
                        finishActivity();
                    }
                }
            }
        });
    }

    public void finishActivity() {
        this.finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

}
