package ingenieria.bci.co.administradordegrupos;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Adapters.CountAdapter;
import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.Animal;
import ingenieria.bci.co.administradordegrupos.Model.AnimalWeight;
import ingenieria.bci.co.administradordegrupos.Services.Bluetooth;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

public class WeighActivity extends AppCompatActivity {
    EditText txtEID;
    EditText txtVN;
    EditText txtCW;
    TextView process_name;
    ImageButton btnadd;
    ListView listView;
    Database db;
    ArrayAdapter<AnimalWeight> adapter;
    ArrayList<String> animalstring;
    ArrayList<String> EventList;
    ArrayList<Animal> animalmodel;
    ArrayList<AnimalWeight> animalWeight;
    ImageButton btnRead;
    String event;
    Bluetooth bth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_weigh);
        setTitle(getString(R.string.weigh_livestock));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        db = new Database(WeighActivity.this);
        txtEID = (EditText) findViewById(R.id.txtEID);
        txtEID.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return false;
            }
        });
        txtVN = (EditText) findViewById(R.id.txtVN);
        txtCW = (EditText) findViewById(R.id.txtCW);
        process_name = (TextView) findViewById(R.id.process_name);
        btnRead = (ImageButton) findViewById(R.id.btnRead);
        EventList = new ArrayList<>();
        btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.ReadWeightFromBlueTooth(WeighActivity.this, txtCW, btnRead);
            }
        });
        btnadd = (ImageButton) findViewById(R.id.btnadd);
        listView = (ListView) findViewById(R.id.listView);
        animalstring = new ArrayList<>();
        animalmodel = new ArrayList<>();
        animalWeight = new ArrayList<>();
        adapter = new CountAdapter(WeighActivity.this, animalWeight, WeighActivity.this, true);
        listView.setAdapter(adapter);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(WeighActivity.this);
                builder.setTitle(getString(R.string.title));
                builder.setMessage(getString(R.string.deleteitem));
                builder.setCancelable(false);
                builder.setPositiveButton(R.string.alertyes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        animalWeight.remove(position);
                        animalmodel.remove(position);
                        getSupportActionBar().setSubtitle("Total : " + animalWeight.size());
                        adapter.notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton(R.string.alertno, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.show();
                return false;
            }
        });
        btnadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String eid = txtEID.getText().toString();
                String vn = txtVN.getText().toString();
                String we = txtCW.getText().toString();
                if (!eid.isEmpty() && !we.isEmpty()) {
                    Animal animal = db.GetAnimalByEid(eid);
                    AddItem(animal, we);
                } else if (!vn.isEmpty() && !we.isEmpty()) {
                    Animal animal = db.GetAnimalByEidVn(eid);
                    AddItem(animal, we);
                } else {
                    Tools.CreateSimpleDialog(WeighActivity.this, getString(R.string.completeFields));
                }
            }
        });
        bth = Tools.ReadIdentificationFromBlueTooth(WeighActivity.this, txtEID, null);
        OpenSelectEventModal();
    }

    private void OpenSelectEventModal() {

        AlertDialog.Builder builder = new AlertDialog.Builder(WeighActivity.this);
        LayoutInflater inflater = WeighActivity.this.getLayoutInflater();
        final View vista = inflater.inflate(R.layout.select_event, null);
        final Spinner ddlEventName = (Spinner) vista.findViewById(R.id.ddEventName);
        ArrayList<String> eventArray = db.GetEvents();
        eventArray.add(0, getString(R.string.choose_spinner));
        ArrayAdapter<String> ddladapter = new ArrayAdapter<String>(WeighActivity.this, R.layout.spinner, eventArray);
        ddlEventName.setAdapter(ddladapter);
        builder.setView(vista)
                .setCancelable(false)
                .setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        String EventName = ddlEventName.getSelectedItem().toString();
                        if (EventName.equals(getString(R.string.choose_spinner))) {
                            Tools.CreateSimpleDialog(WeighActivity.this, getString(R.string.completeFields));
                            Back();
                            return;
                        }
                        process_name.setText(EventName);
                        event = EventName;

                    }
                });
        builder.create().show();
    }

    private void Back() {
        if (bth.isConnected())
            bth.disconnect();
        this.finish();
        super.onBackPressed();
    }

    private void AddItem(Animal animal, String weight) {
        if (animal != null) {
            for (AnimalWeight an : animalWeight) {
                if (animal.getAnimalEID().equals(an.getAnimalEID())) {
                    Tools.CreateSimpleDialog(WeighActivity.this, getString(R.string.this_animal_on_list));
                    return;
                }
            }
            //animal.setAnimalCurrentWeight(weight);
            db.UpdateAnimalCurrentWeight(weight, animal.getAnimalEID());

            animalmodel.add(animal);
            animalWeight.add(new AnimalWeight(animal.getAnimalEID(), weight, Tools.GetDate(false), event, animal.getAnimalCurrentWeight()));
            adapter.notifyDataSetChanged();
            txtEID.setText("");
            txtVN.setText("");
            txtCW.setText("");
            getSupportActionBar().setSubtitle("Total : " + animalmodel.size());
        } else {
            Tools.CreateSimpleDialog(WeighActivity.this, getString(R.string.No_data));
        }
    }

    private void SaveProcess() {
        if (animalWeight.size() <= 0) {
            Toast.makeText(WeighActivity.this, R.string.norecords, Toast.LENGTH_LONG).show();
            return;
        }
        String SaveDate = Tools.GetDate(true);
        for (AnimalWeight animal : animalWeight) {
            animal.setEvent(animal.getEvent() + "-" + SaveDate);
            db.RegisterAnimalEvent(animal, true);
        }
        Tools.CreateSimpleDialog(WeighActivity.this, getString(R.string.complete));
        OpenMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_export, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        } else if (id == R.id.save) {
            SaveProcess();
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(WeighActivity.this);
        builder.setTitle(getString(R.string.title));
        builder.setMessage(getString(R.string.areyoushure));
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.alertyes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Back();
            }
        });
        builder.setNegativeButton(R.string.alertno, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void OpenMenu() {

        if (animalWeight.size() <= 0) {
            Toast.makeText(WeighActivity.this, R.string.norecords, Toast.LENGTH_LONG).show();
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(WeighActivity.this);
        builder.setTitle(getString(R.string.title));
        builder.setMessage(getString(R.string.send_email_report));
        builder.setPositiveButton(R.string.alertyes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String Data = animalWeight.get(0).title();
                for (AnimalWeight item : animalWeight) {
                    Data += item.toString();
                }
                Tools.SaveReport(WeighActivity.this, Data, "smooperFile" + Tools.GetDate(true) + ".txt");
                Back();
            }
        });
        builder.setNegativeButton(R.string.alertno, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                Back();
                dialog.dismiss();
            }
        });

        builder.create().show();
    }

}
