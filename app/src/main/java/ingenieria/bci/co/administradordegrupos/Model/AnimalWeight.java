package ingenieria.bci.co.administradordegrupos.Model;

/**
 * Created by nicolas on 7/9/17.
 */

public class AnimalWeight {
    public int RegisterId;
    public String AnimalEID;
    public String Weight;
    public String Date;
    public String Event;
    public String OldWeight;

    public AnimalWeight() {

    }

    public AnimalWeight(String animalEID, String weight, String date, String event) {
        AnimalEID = animalEID;
        Weight = weight;
        Date = date;
        Event = event;
    }
    public AnimalWeight(String animalEID, String weight, String date, String event,String Old) {
        AnimalEID = animalEID;
        Weight = weight;
        Date = date;
        Event = event;
        OldWeight = Old;
    }
    public String title() {
        return "ID;ANIMALEID;WEIGHT;OLDWEIGHT;DATE;EVENT\n";
    }

    public String toString() {
        return getRegisterId() + ";" +
                getAnimalEID() + ";" +
                getWeight() + ";" +
                getOldWeight() + ";" +
                getDate() + ";" +
                getEvent() + "\n";
    }

    public String getOldWeight() {
        return OldWeight;
    }

    public void setOldWeight(String oldWeight) {
        OldWeight = oldWeight;
    }

    public int getRegisterId() {
        return RegisterId;
    }

    public void setRegisterId(int registerId) {
        RegisterId = registerId;
    }

    public String getAnimalEID() {
        return AnimalEID;
    }

    public void setAnimalEID(String animalEID) {
        AnimalEID = animalEID;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getEvent() {
        return Event;
    }

    public void setEvent(String event) {
        Event = event;
    }
}
