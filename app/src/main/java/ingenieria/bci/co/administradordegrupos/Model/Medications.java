package ingenieria.bci.co.administradordegrupos.Model;

/**
 * Created by nicolas on 6/13/17.
 */

public class Medications {
    public int MedicationId;
    public String MedicationName;
    public double MedicationPrice;
    public double MedicationQuantity;
    public String MedicationExpiryDate;
    public String MedicationSupplierName;
    public String MedicationSupplierTelephone;
    public String MedicationStatus;

    public String toString() {
        return getMedicationName();
    }

    public Medications() {
    }

    public int getMedicationId() {
        return MedicationId;
    }

    public void setMedicationId(int medicationId) {
        MedicationId = medicationId;
    }

    public String getMedicationName() {
        return MedicationName;
    }

    public void setMedicationName(String medicationName) {
        MedicationName = medicationName;
    }

    public double getMedicationPrice() {
        return MedicationPrice;
    }

    public void setMedicationPrice(double medicationPrice) {
        MedicationPrice = medicationPrice;
    }

    public double getMedicationQuantity() {
        return MedicationQuantity;
    }

    public void setMedicationQuantity(double medicationQuantity) {
        MedicationQuantity = medicationQuantity;
    }

    public String getMedicationExpiryDate() {
        return MedicationExpiryDate;
    }

    public void setMedicationExpiryDate(String medicationExpiryDate) {
        MedicationExpiryDate = medicationExpiryDate;
    }

    public String getMedicationSupplierName() {
        return MedicationSupplierName;
    }

    public void setMedicationSupplierName(String medicationSupplierName) {
        MedicationSupplierName = medicationSupplierName;
    }

    public String getMedicationSupplierTelephone() {
        return MedicationSupplierTelephone;
    }

    public void setMedicationSupplierTelephone(String medicationSupplierTelephone) {
        MedicationSupplierTelephone = medicationSupplierTelephone;
    }

    public String getMedicationStatus() {
        return MedicationStatus;
    }

    public void setMedicationStatus(String medicationStatus) {
        MedicationStatus = medicationStatus;
    }
}
