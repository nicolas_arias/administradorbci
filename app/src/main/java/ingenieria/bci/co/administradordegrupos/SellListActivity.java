package ingenieria.bci.co.administradordegrupos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Adapters.DynamicAdapter;
import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.Animal;
import ingenieria.bci.co.administradordegrupos.Model.DynamicList;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;

public class SellListActivity extends AppCompatActivity {

    ListView sellList;
    DynamicAdapter adapter;
    ArrayList<DynamicList> arrayList;
    String searchType;
    String searchValue;
    Database db;
    CheckBox allck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_sell_list);
        setTitle(getString(R.string.Choose_animals));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        db = new Database(SellListActivity.this);
        searchType = intent.getStringExtra("type");
        searchValue = intent.getStringExtra("value");
        setList();
    }

    private void setList() {
        allck = (CheckBox) findViewById(R.id.allck);
        allck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (DynamicList item : arrayList) {
                    if (allck.isChecked())
                        item.setSelected(true);
                    else
                        item.setSelected(false);
                }
                adapter.notifyDataSetChanged();
            }
        });
        sellList = (ListView) findViewById(R.id.sellList);
        SetDataList();
        adapter = new DynamicAdapter(SellListActivity.this, arrayList, SellListActivity.this);
        sellList.setAdapter(adapter);

    }

    private void SetDataList() {
        if (searchValue.equals(getString(R.string.all)))
            searchValue = "%";
        if (searchType.equals(getString(R.string.Herd)))
            arrayList = db.GetAnimalsSell("select AnimalId,AnimalVisualNumber,AnimalEID from Animal where AnimalStatus = 'ACTIVO' and AnimalHerd like '" + searchValue + "'");
        else
            arrayList = db.GetAnimalsSell("select AnimalId,AnimalVisualNumber,AnimalEID from Animal where AnimalStatus = 'ACTIVO' and AnimalFarm like '" + searchValue + "'");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        ArrayList<String> selectedItems = new ArrayList<>();
        for (DynamicList item : arrayList) {
            if (item.getSelected())
                selectedItems.add(String.valueOf(item.getValue()));
        }
        MoveActivity.IdData = selectedItems;
        DippingActivity.IdData = selectedItems;
        DiseaseActivity.IdData = selectedItems;
        VaccunateActivity.IdData = selectedItems;
        this.finish();
        super.onBackPressed();
    }
}
