package ingenieria.bci.co.administradordegrupos.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Model.AnimalWeight;
import ingenieria.bci.co.administradordegrupos.Model.Breeds;
import ingenieria.bci.co.administradordegrupos.R;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

/**
 * Created by nicolas on 7/9/17.
 */

public class CountAdapter extends ArrayAdapter<AnimalWeight> {
    final Context mainContext;
    final Activity activity;
    final boolean isweight;

    public CountAdapter(Context context, ArrayList<AnimalWeight> animal, Activity activity, boolean isWeight) {
        super(context, 0, animal);
        this.mainContext = context;
        this.activity = activity;
        this.isweight = isWeight;
    }

    private static class ViewHolder {
        TextView lblEID;
        TextView lblDate;
        TextView lblWeight;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final AnimalWeight animalWeight = getItem(position);

        CountAdapter.ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new CountAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.count_list, parent, false);
            viewHolder.lblEID = (TextView) convertView.findViewById(R.id.lblEID);
            viewHolder.lblDate = (TextView) convertView.findViewById(R.id.lblDate);
            viewHolder.lblWeight = (TextView) convertView.findViewById(R.id.lblWeight);

            viewHolder.lblEID.setText(animalWeight.getAnimalEID());
            viewHolder.lblDate.setText(animalWeight.getDate());
            if (isweight)
                viewHolder.lblWeight.setText(animalWeight.getWeight() + " " + Tools.GetunitsWeight(activity));
            else {
                viewHolder.lblWeight.setVisibility(View.INVISIBLE);
                viewHolder.lblWeight.setHeight(0);
            }

        } else {
            viewHolder = (CountAdapter.ViewHolder) convertView.getTag();
        }
        return convertView;
    }
}
