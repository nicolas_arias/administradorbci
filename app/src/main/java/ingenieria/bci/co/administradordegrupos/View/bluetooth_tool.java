//package ingenieria.bci.co.administradordegrupos.View;
//
//import android.app.Activity;
//import android.app.DialogFragment;
//import android.app.Fragment;
//import android.bluetooth.BluetoothDevice;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.ListView;
//import android.widget.Spinner;
//import android.widget.Toast;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import ingenieria.bci.co.administradordegrupos.Data.Database;
//import ingenieria.bci.co.administradordegrupos.R;
//import ingenieria.bci.co.administradordegrupos.RegisterActivity;
//import ingenieria.bci.co.administradordegrupos.Services.Bluetooth;
//
///**
// * Created by nicolas on 6/28/17.
// */
//
//public class bluetooth_tool extends DialogFragment {
//    View vista;
//    Activity activity;
//    ListView spinner;
//    Button button;
//    Button off;
//    ArrayList<String> deviceNames;
//    ArrayAdapter<String> adapter;
//    List<BluetoothDevice> devices;
//    Bluetooth bluetooth;
//    EditText txtValor;
//
//    public static bluetooth_tool newInstance(int num) {
//        bluetooth_tool f = new bluetooth_tool();
//
//        // Supply num input as an argument.
//        Bundle args = new Bundle();
//        args.putInt("num", num);
//        f.setArguments(args);
//
//        return f;
//    }
//
//
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
//        vista = inflater.inflate(R.layout.bluetooth_devices, container, false);
//        activity = getActivity();
//        devices = new ArrayList<>();
//        spinner = (ListView) vista.findViewById(R.id.ddlDevices);
//        setData();
//        adapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, new ArrayList<String>());
//        spinner.setAdapter(adapter);
//        txtValor = (EditText) vista.findViewById(R.id.txtValor);
//        spinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//
//                bluetooth.connectToDevice(devices.get(position));
//            }
//        });
//        return vista;
//    }
//
//    public void setData() {
//        String btName = getString(R.string.btName);
//        bluetooth = new Bluetooth(activity);
//        bluetooth.enableBluetooth();
//        bluetooth.setDiscoveryCallback(new Bluetooth.DiscoveryCallback() {
//            @Override
//            public void onFinish() {
//                Toast.makeText(activity, "Scann Finished!!!", Toast.LENGTH_LONG).show();
//            }
//
//            @Override
//            public void onDevice(BluetoothDevice device) {
//                // device found
//                final BluetoothDevice tmp = device;
//                devices.add(device);
//
//                activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        adapter.add(tmp.getAddress() + " - " + tmp.getName());
//                    }
//                });
//            }
//
//            @Override
//            public void onPair(BluetoothDevice device) {
//                Toast.makeText(activity, "Conectado!!!", Toast.LENGTH_LONG).show();
//            }
//
//            @Override
//            public void onUnpair(BluetoothDevice device) {
//                Toast.makeText(activity, "Desconectado", Toast.LENGTH_LONG).show();
//            }
//
//            @Override
//            public void onError(final String message) {
//                activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
//                    }
//                });
//
//            }
//        });
//        bluetooth.setCommunicationCallback(new Bluetooth.CommunicationCallback() {
//            @Override
//            public void onConnect(BluetoothDevice device) {
//                activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(activity, "Conectado!!!", Toast.LENGTH_LONG).show();
//                    }
//                });
//
//            }
//
//            @Override
//            public void onDisconnect(BluetoothDevice device, String message) {
//                activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(activity, "Desconectado!!!", Toast.LENGTH_LONG).show();
//                    }
//                });
//
//            }
//
//            @Override
//            public void onMessage(final String message) {
//                activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        // Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
//                        txtValor.setText(message);
//                    }
//                });
//
//            }
//
//            @Override
//            public void onError(final String message) {
//                activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
//                    }
//                });
//                // error occurred
//            }
//
//            @Override
//            public void onConnectError(BluetoothDevice device, String message) {
//                Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
//            }
//        });
//        button = (Button) vista.findViewById(R.id.btnScan);
//        off = (Button) vista.findViewById(R.id.btnOff);
//        button.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        bluetooth.connectToName(getString(R.string.btName));
//                    }
//                });
//
//                //bluetooth.scanDevices();
//            }
//        });
//        off.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(activity, bluetooth.getDevice().getName(), Toast.LENGTH_LONG).show();
//                bluetooth.disconnect();
//                //bluetooth.unpair(bluetooth.getDevice());
//            }
//        });
//    }
//}
