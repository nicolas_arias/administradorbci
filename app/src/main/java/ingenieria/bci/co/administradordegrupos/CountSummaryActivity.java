package ingenieria.bci.co.administradordegrupos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Adapters.CountAdapter;
import ingenieria.bci.co.administradordegrupos.Adapters.HerdSummaryAdapter;
import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.AnimalWeight;
import ingenieria.bci.co.administradordegrupos.Model.ReportBreed;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

public class CountSummaryActivity extends AppCompatActivity {

    ListView summarylist;
    Database db;
    ArrayList<AnimalWeight> array;
    ArrayAdapter<AnimalWeight> adapter;
    String Event;
    TextView lblTotal;
    TextView lblEvent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_count_summary);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        summarylist = (ListView) findViewById(R.id.summarylist);
        array = new ArrayList<>();
        db = new Database(this);

        lblTotal = (TextView) findViewById(R.id.lblTotal);
        lblEvent = (TextView) findViewById(R.id.lblEvent);

        Intent intent = getIntent();
        String tipo = intent.getStringExtra("tipo");
        Event = intent.getStringExtra("event");
        if (tipo.equals("weight")) {
            setTitle(getString(R.string.weight_summary));
            Weigh();
        } else {
            setTitle(getString(R.string.count_summary));
            Count();
        }
    }

    private void Weigh() {
        if (!array.isEmpty())
            array.clear();
        array = db.GetWeightEventsByEvent(Event);
        adapter = new CountAdapter(this, array, this, true);
        summarylist.setAdapter(adapter);
        lblEvent.setText(array.get(0).getEvent());
        lblTotal.setText("Total : " + String.valueOf(array.size()));
    }

    private void Count() {
        if (!array.isEmpty())
            array.clear();
        array = db.GetCountEventsByEvent(Event);
        adapter = new CountAdapter(this, array, this, false);
        summarylist.setAdapter(adapter);
        lblEvent.setText(array.get(0).getEvent());
        lblTotal.setText(String.valueOf(array.size()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_export_summary, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        } else if (id == R.id.export) {
            String Data = array.get(0).title();
            for (AnimalWeight record : array) {
                Data += record.toString();
            }
            Tools.SaveReport(CountSummaryActivity.this, Data, "smooperFile" + Tools.GetDate(true) + ".txt");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
