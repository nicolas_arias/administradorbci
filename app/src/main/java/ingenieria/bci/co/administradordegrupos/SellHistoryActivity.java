package ingenieria.bci.co.administradordegrupos;

import android.content.Intent;
import android.support.annotation.StringDef;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.AnimalSales;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;

public class SellHistoryActivity extends AppCompatActivity {

    ListView listView;
    String text = "";
    String value = "";
    ArrayAdapter<String> adapter;
    ArrayList<String> arrayList;
    Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_sell_history);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.sales_details));
        db = new Database(SellHistoryActivity.this);
        listView = (ListView) findViewById(R.id.listView);
        arrayList = db.GetSalesHistoryString();
        adapter = new ArrayAdapter<String>(this, R.layout.spinner, arrayList);
        listView.setAdapter(adapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
