package ingenieria.bci.co.administradordegrupos.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.Image;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.HerdsActivity;
import ingenieria.bci.co.administradordegrupos.Model.Herds;
import ingenieria.bci.co.administradordegrupos.R;

/**
 * Created by Nicolas on 17/06/2017.
 */

public class HerdListAdapter extends ArrayAdapter<Herds> {
    final Context mainContext;
    final Activity activity;

    public HerdListAdapter(Context context, ArrayList<Herds> herds, Activity activity) {
        super(context, R.layout.herd_list, herds);
        this.mainContext = context;
        this.activity = activity;
    }

    private static class ViewHolder {
        TextView lblName;
        TextView lblStatus;
        ImageButton btnInactivate;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Herds herd = getItem(position);

        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.herd_list, parent, false);
            viewHolder.lblName = (TextView) convertView.findViewById(R.id.lblHerdName);
            viewHolder.lblStatus = (TextView) convertView.findViewById(R.id.lblHerdStatus);
            viewHolder.lblName.setText(herd.getHerdName());
            viewHolder.lblStatus.setText(herd.getStatus());

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        return convertView;
    }
}
