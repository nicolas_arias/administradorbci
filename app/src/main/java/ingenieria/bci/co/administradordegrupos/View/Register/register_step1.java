package ingenieria.bci.co.administradordegrupos.View.Register;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import javax.crypto.spec.DESKeySpec;

import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.MainActivity;
import ingenieria.bci.co.administradordegrupos.Model.Breeds;
import ingenieria.bci.co.administradordegrupos.R;
import ingenieria.bci.co.administradordegrupos.RegisterActivity;
import ingenieria.bci.co.administradordegrupos.Services.Bluetooth;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

/**
 * Created by Nicolas on 15/06/2017.
 */

public class register_step1 extends Fragment {
    View vista;
    Database db;
    Spinner ddlBreed;
    Spinner ddlType;
    EditText txtDestination;
    Spinner ddlGender;
    Spinner ddlGroup;
    Button btnSaveAnimal;
    EditText txtEId;
    EditText txtVnumber;
    EditText txtDob;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.register_step1, container, false);
        getActivity().setTitle(getString(R.string.tab_one));
        db = new Database(getActivity());

        RegisterActivity.EId = "";
        RegisterActivity.Vnumber = "";
        RegisterActivity.DateOfBirth = "";
        RegisterActivity.Type = "";
        RegisterActivity.Breed = "";
        RegisterActivity.Destination = "";
        RegisterActivity.Group = "";
        RegisterActivity.Gender = "";

        setControls();
        return vista;
    }

    private void SetType() {
        ddlType = (Spinner) vista.findViewById(R.id.ddlType);
        final ArrayList<String> typeList = Tools.arrayToList(getResources().getStringArray(R.array.cattle_type));
        typeList.add(0, getResources().getString(R.string.choose_spinner));
        SetAdapters(typeList, ddlType);
        ddlType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SetBreeds(typeList.get(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void SetBreeds(String type) {
        ddlBreed = (Spinner) vista.findViewById(R.id.ddlBreed);
        txtDestination = (EditText) vista.findViewById(R.id.txtDestination);
        final ArrayList<Breeds> breedList = db.GetBreedsByType(type);
        ArrayList<String> breedListString = new ArrayList<>();
        breedListString.add(getString(R.string.choose_spinner));
        for (int i = 0; i < breedList.size(); i++) {
            breedListString.add(breedList.get(i).getBreedName());
        }
        SetAdapters(breedListString, ddlBreed);
        ddlBreed.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0)
                    txtDestination.setText("");
                else
                    txtDestination.setText(breedList.get(position - 1).getBreedDestination());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void SetGender() {
        ddlGender = (Spinner) vista.findViewById(R.id.ddlGender);
        ArrayList<String> GenderArray = Tools.arrayToList(getResources().getStringArray(R.array.genders));
        GenderArray.add(0, getString(R.string.choose_spinner));
        SetAdapters(GenderArray, ddlGender);
    }

    private void SetGroup() {
        ddlGroup = (Spinner) vista.findViewById(R.id.ddlGroup);
        ArrayList<String> GroupArray = Tools.arrayToList(getResources().getStringArray(R.array.cattle_group));
        GroupArray.add(0, getString(R.string.choose_spinner));
        SetAdapters(GroupArray, ddlGroup);
    }

    private void SetAdapters(ArrayList<String> array, Spinner spinner) {
        ArrayAdapter<String> spinnerArray = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, array);
        spinnerArray.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArray);
    }


    private void setControls() {
        // Controls //
        SetType();
        SetGroup();
        SetGender();
        txtEId = (EditText) vista.findViewById(R.id.txtAnimalId);
        txtEId.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Tools.ReadIdentificationFromBlueTooth(getActivity(), txtEId, null);
                return false;
            }
        });
        txtVnumber = (EditText) vista.findViewById(R.id.txtAnimalVn);
        txtDob = (EditText) vista.findViewById(R.id.txtDateOfBirth);
        btnSaveAnimal = (Button) vista.findViewById(R.id.btnSaveAnimal);
        btnSaveAnimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateAndContinue()) {
                    android.app.FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.register_fm_content, new register_step2()).setCustomAnimations(R.animator.enter, R.animator.exit, R.animator.enter, R.animator.exit).commit();
                }
            }
        });


        txtDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Tools.OpenDateDialog(txtDob, getActivity());
            }

        });
        Tools.ReadIdentificationFromBlueTooth(getActivity(), txtEId, null);
    }

    public boolean validateAndContinue() {

        String EId = txtEId.getText().toString();
        String Vnumber = txtVnumber.getText().toString();
        String DateOfBirth = txtDob.getText().toString();
        String Type = ddlType.getSelectedItem().toString();
        String Breed = ddlBreed.getSelectedItem().toString();
        String Destination = txtDestination.getText().toString();
        String Group = ddlGroup.getSelectedItem().toString();
        String Gender = ddlGender.getSelectedItem().toString();

        if (DateOfBirth.isEmpty() ||
                Vnumber.isEmpty() ||
                Type.isEmpty() ||
                Type.equals(getString(R.string.choose_spinner)) ||
                Breed.isEmpty() ||
                Breed.equals(getString(R.string.choose_spinner)) ||
                Destination.isEmpty() ||
                Destination.equals(getString(R.string.choose_spinner)) ||
                Group.isEmpty() ||
                Group.equals(getString(R.string.choose_spinner)) ||
                Gender.isEmpty() ||
                Gender.equals(getString(R.string.choose_spinner))) {
            Tools.CreateSimpleDialog(getActivity(), getString(R.string.completeFields));
            return false;
        }

        RegisterActivity.EId = EId;
        RegisterActivity.Vnumber = Vnumber;
        RegisterActivity.DateOfBirth = DateOfBirth;
        RegisterActivity.Type = Type;
        RegisterActivity.Breed = Breed;
        RegisterActivity.Destination = Destination;
        RegisterActivity.Group = Group;
        RegisterActivity.Gender = Gender;


        return true;
    }
}
