package ingenieria.bci.co.administradordegrupos;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Adapters.BreedListAdapter;
import ingenieria.bci.co.administradordegrupos.Adapters.MedicineListAdapter;
import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.Breeds;
import ingenieria.bci.co.administradordegrupos.Model.Medications;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
import ingenieria.bci.co.administradordegrupos.Services.Tools;
import ingenieria.bci.co.administradordegrupos.View.Medications.RestockDialog;

public class MedicationActivity extends AppCompatActivity {

    Database db;
    ArrayList<Medications> medicationsArray;
    public MedicineListAdapter adapter;
    RelativeLayout NoDataPanel;
    FloatingActionButton fabAddmedication;
    ListView medicationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_medication);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.medicationTitle);
        medicationList = (ListView) findViewById(R.id.medicinelist);
        SetViews();

    }

    protected void onStart() {
        FillList();
        super.onStart();
    }

    private void SetViews() {
        fabAddmedication = (FloatingActionButton) findViewById(R.id.fabAddMedicine);
        fabAddmedication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MedicationActivity.this, MedicationAddActivity.class);
                startActivity(intent);
            }
        });
    }

    private void FillList() {
        db = new Database(getBaseContext());
        NoDataPanel = (RelativeLayout) findViewById(R.id.NoDataPanel);
        medicationsArray = null;
        medicationsArray = db.GetMedications();
        adapter = new MedicineListAdapter(getBaseContext(), medicationsArray, MedicationActivity.this);
        medicationList.setAdapter(adapter);
        if (medicationsArray.size() > 0) {
            NoDataPanel.setVisibility(View.INVISIBLE);


            SetLongPress();

        } else {
            NoDataPanel.setVisibility(View.VISIBLE);
        }
        adapter.notifyDataSetChanged();
    }

    private void SetLongPress() {
        medicationList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView parent, View view, final int position, long id) {
                final CharSequence[] items = {
                        getString(R.string.details),
                        getString(R.string.Restock),
                        getString(R.string.change_status)
                };
                final AlertDialog.Builder builder = new AlertDialog.Builder(MedicationActivity.this);
                builder.setTitle("BCI");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final Database db = new Database(MedicationActivity.this);
                        switch (which) {
                            case 0:
                                Medications medicine = medicationsArray.get(position);
                                String Message = "";
                                Message += getString(R.string.wname) + "\t: " + medicine.getMedicationName() + "\n";
                                Message += getString(R.string.wexpiration) + "\t: " + medicine.getMedicationExpiryDate() + "\n";
                                Message += getString(R.string.wprice) + "\t: " + medicine.getMedicationPrice() + "\n";
                                Message += getString(R.string.suppliername) + "\t: " + medicine.getMedicationSupplierName() + "\n";
                                Message += getString(R.string.suppliertel) + "\t: " + medicine.getMedicationSupplierTelephone() + "\n";
                                Message += "Id\t: " + medicine.getMedicationId() + "\n";
                                Message += getString(R.string.wquantity) + "\t: " + medicine.getMedicationQuantity() + "\n";
                                Tools.CreateSimpleDialog(MedicationActivity.this, Message);

                                break;
                            case 1:
                                AlertDialog.Builder builder = new AlertDialog.Builder(MedicationActivity.this);
                                LayoutInflater inflater = MedicationActivity.this.getLayoutInflater();
                                final View vista = inflater.inflate(R.layout.restock_layout, null);
                                builder.setView(vista)
                                        .setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int id) {
                                                EditText quantity = (EditText) vista.findViewById(R.id.txtNewQuantity);
                                                String qty = quantity.getText().toString();
                                                if (qty.isEmpty()) {
                                                    return;
                                                }
                                                Database db = new Database(MedicationActivity.this);
                                                if (db.UpdateMedicineStock(medicationsArray.get(position).getMedicationId(), qty)) {
                                                    Toast.makeText(MedicationActivity.this, R.string.complete, Toast.LENGTH_LONG).show();
                                                } else {
                                                    Toast.makeText(MedicationActivity.this, R.string.tryagain, Toast.LENGTH_LONG).show();
                                                }
                                                FillList();
                                                adapter.notifyDataSetChanged();
                                            }
                                        })
                                        .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.dismiss();
                                            }
                                        });
                                TextView name = (TextView) vista.findViewById(R.id.medicineName);
                                TextView quant = (TextView) vista.findViewById(R.id.medicineQuantity);

                                name.setText(medicationsArray.get(position).getMedicationName());
                                quant.setText(String.valueOf(medicationsArray.get(position).getMedicationQuantity()));
                                builder.create().show();

                                break;
                            case 2:
                                AlertDialog.Builder builderD = new AlertDialog.Builder(MedicationActivity.this);
                                builderD.setTitle("BCI");
                                builderD.setMessage(R.string.inactivate);
                                builderD.setPositiveButton(R.string.alertyes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        db.InactivateMedicine(medicationsArray.get(position).getMedicationId());
                                        Toast.makeText(MedicationActivity.this, R.string.complete, Toast.LENGTH_LONG);
                                        try {
                                            FillList();
                                        } catch (Exception e) {

                                        }
                                    }
                                });
                                builderD.create().show();
                                break;
                        }
                        dialog.dismiss();
                        try {
                            FillList();
                        } catch (Exception e) {
                        }
                    }
                });
                builder.show();
                return false;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
