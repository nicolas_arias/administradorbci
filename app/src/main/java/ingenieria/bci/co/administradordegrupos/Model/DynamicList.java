package ingenieria.bci.co.administradordegrupos.Model;

/**
 * Created by nicolas on 7/2/17.
 */

public class DynamicList {

    public int Id;
    public String Text;
    public String Value;
    boolean Selected;

    public DynamicList() {
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        Text = text;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public boolean getSelected() {
        return Selected;
    }

    public void setSelected(boolean selected) {
        Selected = selected;
    }
}
