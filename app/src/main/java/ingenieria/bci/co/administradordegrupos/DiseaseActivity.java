package ingenieria.bci.co.administradordegrupos;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
import ingenieria.bci.co.administradordegrupos.Services.DatabaseTools;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

public class DiseaseActivity extends AppCompatActivity {

    Spinner ddlSelectBy;
    Spinner ddlGroupValue;
    ImageButton btnShowList;
    Spinner ddlTypeDisease;
    EditText txtComments;
    EditText txtDateDesease;
    Button btnRegisterDesease;
    public static ArrayList<String> IdData;
    Database db;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_disease);
        setTitle(getString(R.string.report_disease));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = DiseaseActivity.this;
        db = new Database(DiseaseActivity.this);
        IdData = new ArrayList<>();
        final DatabaseTools dbtools = new DatabaseTools(activity);

        ddlSelectBy = (Spinner) findViewById(R.id.ddlSelectBy);
        ddlGroupValue = (Spinner) findViewById(R.id.ddlGroupValue);

        btnShowList = (ImageButton) findViewById(R.id.btnShowList);
        btnShowList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = ddlSelectBy.getSelectedItem().toString();
                String value = ddlGroupValue.getSelectedItem().toString();

                Intent intent = new Intent(activity, SellListActivity.class);
                intent.putExtra("type", type);
                intent.putExtra("value", value);
                startActivityForResult(intent, 0);
            }
        });
        dbtools.setAdapterId(ddlSelectBy, R.array.sell_by, false);
        ddlSelectBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = ddlSelectBy.getSelectedItem().toString();
                if (value.equals("All"))
                    return;
                dbtools.setAdapter(ddlGroupValue, value + "s", value + "Name", "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ddlTypeDisease = (Spinner) findViewById(R.id.ddlTypeDisease);
        dbtools.setAdapterId(ddlTypeDisease, R.array.type_desease, false);
        txtComments = (EditText) findViewById(R.id.txtComments);
        txtDateDesease = (EditText) findViewById(R.id.txtDateDesease);
        txtDateDesease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.OpenDateDialog(txtDateDesease, activity);
            }
        });
        btnRegisterDesease = (Button) findViewById(R.id.btnRegisterDesease);
        btnRegisterDesease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = ddlTypeDisease.getSelectedItem().toString();
                String date = txtDateDesease.getText().toString();
                String comments = txtComments.getText().toString();

                if (type.isEmpty() || date.isEmpty() || comments.isEmpty() || IdData.size() <= 0) {
                    Tools.CreateSimpleDialog(activity, getString(R.string.completeFields));
                    return;
                }

                if (db.RegisterDisease(IdData, type, date, comments)) {
                    Tools.CreateSimpleDialog(activity, getString(R.string.complete));
                    txtDateDesease.setText("");
                    txtComments.setText("");
                    IdData = new ArrayList<>();
                    getSupportActionBar().setSubtitle("");
                } else {
                    Tools.CreateSimpleDialog(activity, getString(R.string.tryagain));
                }

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        getSupportActionBar().setSubtitle("( " + IdData.size() + " ) " + getString(R.string.selected));

    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
