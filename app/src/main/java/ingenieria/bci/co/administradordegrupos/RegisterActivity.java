package ingenieria.bci.co.administradordegrupos;

import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
import ingenieria.bci.co.administradordegrupos.View.Register.register_step1;
import ingenieria.bci.co.administradordegrupos.View.Register.register_step3;

public class RegisterActivity extends AppCompatActivity {

    public static String EId;
    public static String Vnumber;
    public static String DateOfBirth;
    public static String Type;
    public static String Breed;
    public static String Destination;
    public static String Group;
    public static String Gender;

    public static String BirthWeight;
    public static String CurrentWeight;
    public static String DamId;
    public static String SireId;

    public static String DamGrandDamId;
    public static String DamGrandSireId;
    public static String SireGrandDamId;
    public static String SireGrandSireId;
    public static String SurrogateDamId;

    public static String Photo;
    public static boolean IsEmbryo;


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_register);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        android.app.FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.register_fm_content, new register_step1()).setCustomAnimations(R.animator.enter, R.animator.exit, R.animator.enter, R.animator.exit).commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(RegisterActivity.this);
        builder.setTitle("BCI");
        builder.setMessage(getString(R.string.areyoushure));
        builder.setPositiveButton(R.string.alertno, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(R.string.alertyes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                close();
            }
        });
        builder.show();
    }

    private void close() {
        this.finish();
        super.onBackPressed();
    }
}
