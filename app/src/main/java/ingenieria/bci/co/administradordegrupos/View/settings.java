package ingenieria.bci.co.administradordegrupos.View;

import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.support.annotation.Nullable;

import ingenieria.bci.co.administradordegrupos.R;

/**
 * Created by nicolas on 6/12/17.
 */

public class settings extends PreferenceFragment{
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.config_preferences);
    }
}
