package ingenieria.bci.co.administradordegrupos;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.Farms;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;

public class FarmsAddActivity extends AppCompatActivity {

    EditText txtFarmName;
    EditText txtFarmSize;
    EditText txtFarmLocation;
    ImageButton btnOpenLocation;
    Button btnSaveFarm;
    public static String FarmLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        FarmLocation = "";
        setContentView(R.layout.activity_farms_add);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.farmsAddTitle);
        txtFarmName = (EditText) findViewById(R.id.txtFarmName);
        txtFarmLocation = (EditText) findViewById(R.id.txtFarmLocation);
        txtFarmSize = (EditText) findViewById(R.id.txtFarmSize);
        btnOpenLocation = (ImageButton) findViewById(R.id.btnOpenLocation);
        btnSaveFarm = (Button) findViewById(R.id.btnSaveFarm);
        setListener();
    }

    private void setListener() {
        btnOpenLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FarmsAddActivity.this, MapsActivity.class);
                startActivity(intent);
            }
        });
        btnSaveFarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Name = txtFarmName.getText().toString();
                String Size = txtFarmSize.getText().toString();
                String Location = txtFarmLocation.getText().toString();

                if (Name.isEmpty() || Size.isEmpty() || Location.isEmpty()) {
                    Toast.makeText(FarmsAddActivity.this, R.string.completeFields, Toast.LENGTH_LONG).show();
                    return;
                } else {
                    Database db = new Database(FarmsAddActivity.this);
                    String[] position = Location.split("\\|");
                    Farms farm = new Farms();
                    farm.setFarmName(Name);
                    farm.setFarmStatus("ACTIVO");
                    farm.setFarmSize(Double.parseDouble(Size));
                    farm.setFarmLat(position[0]);
                    farm.setFarmLng(position[1]);
                    if (db.AddFarm(farm)) {
                        finishActivity();
                    }
                }
            }
        });
    }

    public void finishActivity() {
        this.finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        if (!FarmLocation.isEmpty()) {
            txtFarmLocation.setText(FarmLocation);
        } else {
            txtFarmLocation.setText("0|0");
        }
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
