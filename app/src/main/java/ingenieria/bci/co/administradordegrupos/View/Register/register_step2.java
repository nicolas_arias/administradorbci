package ingenieria.bci.co.administradordegrupos.View.Register;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.MedicationActivity;
import ingenieria.bci.co.administradordegrupos.R;
import ingenieria.bci.co.administradordegrupos.RegisterActivity;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

/**
 * Created by Nicolas on 15/06/2017.
 */

public class register_step2 extends Fragment {

    CheckBox ckEmbryo;
    ImageButton btnDamDetails;
    ImageButton btnSireDetails;
    ImageButton btnEmbryoDetails;
    EditText txtBirthWeight;
    EditText txtCurrentWeight;
    EditText txtDamId;
    EditText txtSireId;
    Button btnSaveRegister;
    ImageButton btnReadCurrentWeight;
    TextView units;


    View vista;

    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        vista = inflater.inflate(R.layout.register_step2, container, false);
        setControls();
        return vista;
    }

    private void setControls() {
        units = (TextView) vista.findViewById(R.id.units);
        units.setText(getString(R.string.register_weight_title) + " (" + Tools.GetunitsWeight(getActivity()) + ") ");
        btnReadCurrentWeight = (ImageButton) vista.findViewById(R.id.btnReadCurrentWeight);
        btnDamDetails = (ImageButton) vista.findViewById(R.id.btnDamDetails);
        btnSireDetails = (ImageButton) vista.findViewById(R.id.btnSireDetails);
        btnEmbryoDetails = (ImageButton) vista.findViewById(R.id.btnEmbryoDetails);
        txtBirthWeight = (EditText) vista.findViewById(R.id.txtBirthWeight);
        txtCurrentWeight = (EditText) vista.findViewById(R.id.txtCurrentWeight);
        txtDamId = (EditText) vista.findViewById(R.id.txtDamId);
        txtSireId = (EditText) vista.findViewById(R.id.txtSireId);
        btnSaveRegister = (Button) vista.findViewById(R.id.btnSaveRegister);
        ckEmbryo = (CheckBox) vista.findViewById(R.id.ckEmbryo);
        btnEmbryoDetails.setEnabled(false);
        btnSaveRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String BirthWeight = txtBirthWeight.getText().toString();
                String CurrentWeight = txtCurrentWeight.getText().toString();
                boolean Embryo = ckEmbryo.isChecked();
                if (BirthWeight.isEmpty()) {
                    Tools.CreateSimpleDialog(getActivity(), getString(R.string.setBirthWeight));
                    return;
                } else if (CurrentWeight.isEmpty()) {
                    Tools.CreateSimpleDialog(getActivity(), getString(R.string.setCurrentWeight));
                    return;
                }
                RegisterActivity.BirthWeight = BirthWeight;
                RegisterActivity.CurrentWeight = CurrentWeight;
                android.app.FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction().replace(R.id.register_fm_content, new register_step3()).setCustomAnimations(R.animator.enter, R.animator.exit, R.animator.enter, R.animator.exit).commit();
            }
        });
        ckEmbryo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ckEmbryo.isChecked()) {
                    btnEmbryoDetails.setEnabled(true);
                } else {
                    btnEmbryoDetails.setEnabled(false);
                }
            }
        });
        btnDamDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenSireModal("Dam");
            }
        });
        btnSireDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenSireModal("Sire");
            }
        });
        btnEmbryoDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OpenSireModal("Embryo");
            }
        });

        btnReadCurrentWeight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnReadCurrentWeight.setEnabled(false);
                Tools.ReadWeightFromBlueTooth(getActivity(), txtCurrentWeight, btnReadCurrentWeight);
            }
        });
    }

    private void OpenSireModal(final String Type) {
        final TextView GrandDam;
        final TextView GrandSire;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View vista = inflater.inflate(R.layout.register_damsire, null);
        GrandDam = (TextView) vista.findViewById(R.id.txtGrandDam);
        GrandSire = (TextView) vista.findViewById(R.id.txtGrandSire);
        builder.setView(vista)
                .setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        switch (Type) {
                            case "Dam":
                                RegisterActivity.DamGrandDamId = GrandDam.getText().toString();
                                RegisterActivity.DamGrandSireId = GrandSire.getText().toString();
                                break;
                            case "Sire":
                                RegisterActivity.SireGrandDamId = GrandDam.getText().toString();
                                RegisterActivity.SireGrandSireId = GrandSire.getText().toString();
                                break;
                            case "Embryo":
                                RegisterActivity.SurrogateDamId = GrandDam.getText().toString();
                                break;
                        }
                    }
                })
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        if (Type == "Embryo") {
            GrandDam.setHint(R.string.SurrogateId);
            GrandSire.setVisibility(View.INVISIBLE);
        }
        builder.create().show();
    }


}
