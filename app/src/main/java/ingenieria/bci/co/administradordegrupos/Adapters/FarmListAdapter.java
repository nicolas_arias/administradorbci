package ingenieria.bci.co.administradordegrupos.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.FarmsAddActivity;
import ingenieria.bci.co.administradordegrupos.MapsActivity;
import ingenieria.bci.co.administradordegrupos.Model.Farms;
import ingenieria.bci.co.administradordegrupos.R;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

/**
 * Created by nicolas on 6/18/17.
 */

public class FarmListAdapter extends ArrayAdapter<Farms> {
    final Context mainContext;
    final Activity activity;

    public FarmListAdapter(Context context, ArrayList<Farms> farms, Activity activity) {
        super(context, 0, farms);
        this.mainContext = context;
        this.activity = activity;
    }

    private static class ViewHolder {
        TextView lblFarmName;
        TextView lblFarmSize;
        ImageButton btnShowLocation;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Farms farm = getItem(position);

        FarmListAdapter.ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new FarmListAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.farm_list, parent, false);

            viewHolder.lblFarmName = (TextView) convertView.findViewById(R.id.lblFarmName);
            viewHolder.lblFarmSize = (TextView) convertView.findViewById(R.id.lblFarmSize);
            //viewHolder.btnShowLocation = (ImageButton) convertView.findViewById(R.id.btnShowLocation);

            viewHolder.lblFarmName.setText(farm.getFarmName());
            viewHolder.lblFarmSize.setText(Tools.Number(String.valueOf(farm.getFarmSize())) + " " + Tools.GetunitsM(activity));

        } else {
            viewHolder = (FarmListAdapter.ViewHolder) convertView.getTag();
        }
        return convertView;
    }
}
