package ingenieria.bci.co.administradordegrupos;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.Objects;

import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.Medications;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
import ingenieria.bci.co.administradordegrupos.Services.DatabaseTools;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

public class VaccunateActivity extends AppCompatActivity {
    public static ArrayList<String> IdData;
    Database db;
    Activity activity;
    Spinner ddlSelectBy;
    Spinner ddlGroupValue;
    ImageView btnShowList;
    Spinner ddlVaccine;
    EditText txtAvailable;
    EditText txtDosage;
    EditText txtDate;
    EditText txtComments;
    ArrayAdapter<Medications> medicationsAdapter;
    ArrayList<Medications> medicationsArray;
    Button btnRegisterVaccinate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_vaccunate);
        setTitle(getString(R.string.Vaccination));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = VaccunateActivity.this;
        db = new Database(activity);
        IdData = new ArrayList<>();
        final DatabaseTools dbtools = new DatabaseTools(activity);

        ddlVaccine = (Spinner) findViewById(R.id.ddlVaccine);


        ddlSelectBy = (Spinner) findViewById(R.id.ddlSelectBy);
        ddlGroupValue = (Spinner) findViewById(R.id.ddlGroupValue);

        btnShowList = (ImageButton) findViewById(R.id.btnShowList);
        btnShowList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = ddlSelectBy.getSelectedItem().toString();
                String value = ddlGroupValue.getSelectedItem().toString();

                Intent intent = new Intent(activity, SellListActivity.class);
                intent.putExtra("type", type);
                intent.putExtra("value", value);
                startActivityForResult(intent, 0);
            }
        });
        dbtools.setAdapterId(ddlSelectBy, R.array.sell_by, false);
        ddlSelectBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = ddlSelectBy.getSelectedItem().toString();
                if (value.equals("All"))
                    return;
                dbtools.setAdapter(ddlGroupValue, value + "s", value + "Name", "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        txtAvailable = (EditText) findViewById(R.id.txtAvailable);
        txtDosage = (EditText) findViewById(R.id.txtDosage);
        txtComments = (EditText) findViewById(R.id.txtComments);
        txtDate = (EditText) findViewById(R.id.txtDate);
        txtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.OpenDateDialog(txtDate, activity);
            }
        });

        medicationsArray = db.GetMedications();
        medicationsAdapter = new ArrayAdapter(this, R.layout.spinner, medicationsArray);
        ddlVaccine.setAdapter(medicationsAdapter);

        ddlVaccine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Medications medication = (Medications) ddlVaccine.getSelectedItem();
                txtAvailable.setText(String.valueOf(medication.getMedicationQuantity()));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnRegisterVaccinate = (Button) findViewById(R.id.btnRegisterVaccinate);
        btnRegisterVaccinate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Object vacuna = ddlVaccine.getSelectedItem();
                if (ddlVaccine.getSelectedItem() == null) {
                    Tools.CreateSimpleDialog(activity, getString(R.string.completeFields));
                    return;
                }
                String vaccine = ddlVaccine.getSelectedItem().toString();
                String dosage = txtDosage.getText().toString();
                String available = txtAvailable.getText().toString();
                String date = txtDate.getText().toString();
                String comments = txtComments.getText().toString();

                if (vaccine.isEmpty() || dosage.isEmpty() || date.isEmpty() || comments.isEmpty() || IdData.size() <= 0) {
                    Tools.CreateSimpleDialog(activity, getString(R.string.completeFields));
                    return;
                }

                if (Double.valueOf(dosage) > Double.valueOf(available)) {
                    Tools.CreateSimpleDialog(activity, getString(R.string.dosage_volume_not_available));
                }

                if (db.RegisterVaccinate(IdData, vaccine, dosage, date, comments)) {
                    db.UpdateMedicationDosage(vaccine, dosage);
                    Tools.CreateSimpleDialog(activity, getString(R.string.complete));
                    txtDosage.setText("");
                    txtAvailable.setText("");
                    txtDate.setText("");
                    txtComments.setText("");
                    IdData = new ArrayList<>();
                    getSupportActionBar().setSubtitle("");
                } else {
                    Tools.CreateSimpleDialog(activity, getString(R.string.tryagain));
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        getSupportActionBar().setSubtitle("( " + IdData.size() + " ) " + getString(R.string.selected));

    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
