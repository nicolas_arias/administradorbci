package ingenieria.bci.co.administradordegrupos.View.Medications;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.MedicationActivity;
import ingenieria.bci.co.administradordegrupos.R;

/**
 * Created by nicolas on 6/26/17.
 */

public class RestockDialog extends DialogFragment {
    public static int IdMedicine;
    public static String MedicineName;
    public static double MedicineQty;
    Dialog dialog;

    public RestockDialog()
    {

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View vista = inflater.inflate(R.layout.restock_layout, null);
        builder.setView(vista)
                .setPositiveButton(R.string.btn_save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        EditText quantity = (EditText) vista.findViewById(R.id.txtNewQuantity);
                        String qty = quantity.getText().toString();
                        if (qty.isEmpty()) {
                            return;
                        }
                        Database db = new Database(getActivity());
                        if (db.UpdateMedicineStock(IdMedicine, qty)) {
                            Toast.makeText(getActivity(), R.string.complete, Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getActivity(), R.string.tryagain, Toast.LENGTH_LONG).show();
                        }
                    }
                })
                .setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        RestockDialog.this.getDialog().cancel();
                    }
                });
        TextView name = (TextView) vista.findViewById(R.id.medicineName);
        TextView quant = (TextView) vista.findViewById(R.id.medicineQuantity);

        name.setText(MedicineName);
        quant.setText(String.valueOf(MedicineQty));
        dialog = builder.create();
        return dialog;
    }

    @Override
    public Dialog getDialog() {
        return dialog;
    }
}
