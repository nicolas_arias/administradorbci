package ingenieria.bci.co.administradordegrupos.Services;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

/**
 * Created by nicolas on 6/12/17.
 */

public class ChangeLanguage {
    public static void SetNewLanguage(Context context, String code){

        code = Tools.GetLanguage(context);
        String languageToLoad  = code;
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,context.getResources().getDisplayMetrics());
    }
}
