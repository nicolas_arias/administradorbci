package ingenieria.bci.co.administradordegrupos;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;

public class AdminActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_admin);
        setTitle(R.string.admin_title);
        ConfigViews();
    }

    private void ConfigViews() {
        ImageButton btnManageHerd = (ImageButton) findViewById(R.id.btnManageHerd);
        ImageButton btnManageFarm = (ImageButton) findViewById(R.id.btnManageFarm);
        ImageButton btnManageBreed = (ImageButton) findViewById(R.id.btnManageBreed);
        ImageButton btnMaganeMadicine = (ImageButton) findViewById(R.id.btnManageMedicine);
        ImageButton btnManageEvents = (ImageButton) findViewById(R.id.btnManageEvents);
        btnManageHerd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), HerdsActivity.class);
                startActivity(intent);
            }
        });
        btnManageFarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), FarmsActivity.class);
                startActivity(intent);
            }
        });
        btnManageBreed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), BreedsActivity.class);
                startActivity(intent);
            }
        });
        btnMaganeMadicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), MedicationActivity.class);
                startActivity(intent);
            }
        });
        btnManageEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), EventsActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
