package ingenieria.bci.co.administradordegrupos;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

public class LoginActivity extends AppCompatActivity {

    Button btnLog;
    CheckBox ckremember;
    EditText txtUser;
    EditText txtPass;
    EditText txtEmail;
    Activity activity;
    boolean isChecked;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        activity = LoginActivity.this;
        IsLogged();
        ckremember = (CheckBox) findViewById(R.id.ckremember);


        txtUser = (EditText) findViewById(R.id.txtUser);
        txtPass = (EditText) findViewById(R.id.txtPass);
        txtEmail = (EditText) findViewById(R.id.txtEmail);

        //txtUser.setText("Nicolas");
        //txtPass.setText("Nicolas");
        //txtEmail.setText("Nicolas");


        btnLog = (Button) findViewById(R.id.btnLog);
        btnLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtUser.getText().toString().isEmpty() ||
                        txtEmail.getText().toString().isEmpty() ||
                        txtPass.getText().toString().isEmpty()) {
                    Tools.CreateSimpleDialog(activity, getString(R.string.completeFields));
                } else {
                    isChecked = ckremember.isChecked();
                    validate(txtUser.getText().toString(), txtPass.getText().toString(), txtEmail.getText().toString());
                }
            }
        });

    }

    private void IsLogged() {
        String isLogged = Tools.GetDefaultSetting(LoginActivity.this, "remember");
        try {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.BLUETOOTH,
                                Manifest.permission.BLUETOOTH_ADMIN,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.READ_PHONE_STATE
                        }, 0);
            }
        } catch (Exception ex) {
            Toast.makeText(this, getString(R.string.tryagain), Toast.LENGTH_LONG).show();
        }


        if (isLogged.equals("True")) {
            this.finish();
            Intent intent = new Intent(activity, MainActivity.class);
            startActivity(intent);
        }


    }

    public void validate(String user, String pass, String email) {
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String IMEI = telephonyManager.getDeviceId();
        final WSConsulta tarea = new WSConsulta(email, user, IMEI, pass);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pd = ProgressDialog.show(activity, getString(R.string.loadingTitle), "");
                tarea.execute();
            }
        });

    }

    @Override
    public void onBackPressed() {
        this.finish();
        return;
    }

    private class WSConsulta extends AsyncTask<String, Integer, Boolean> {
        String resSoap;
        String Email;
        String Nombre;
        String IMEI;
        String Code;

        public WSConsulta(String _email, String _nombre, String _imei, String _code) {
            Email = _email;
            Nombre = _nombre;
            IMEI = _imei;
            Code = _code;
        }

        protected Boolean doInBackground(String... params) {

            boolean resul = true;
            final String NAMESPACE = "http://www.bci.co/ws/";
            final String URL = "http://www.bci.co/ws/validador.php";
            final String METHOD_NAME = "aprobacion";
            final String SOAP_ACTION = "http://www.bci.co/ws/validador.php/aprobacion";

            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
            request.addProperty("email", Email);
            request.addProperty("Nombre", Nombre);
            request.addProperty("IMEI", IMEI);
            request.addProperty("CODE", Code);
            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = false;
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte = new HttpTransportSE(URL);
            try {
                transporte.call(SOAP_ACTION, envelope);
                resSoap = (String) envelope.getResponse();
                if (resSoap.equals("Acceptado")) {
                    resul = true;
                }
            } catch (Exception e) {

                resul = false;
            }
            return resul;
        }

        protected void onPostExecute(Boolean result) {
            pd.dismiss();
            if (result) {
                if (isChecked) {
                    Tools.SetDefaultSettings(LoginActivity.this, "user", Nombre);
                    Tools.SetDefaultSettings(LoginActivity.this, "email", Email);
                    Tools.SetDefaultSettings(LoginActivity.this, "code", Code);
                    Tools.SetDefaultSettings(LoginActivity.this, "remember", "True");
                } else {
                    Tools.SetDefaultSettings(LoginActivity.this, "remember", "False");
                }
                LoginActivity.this.finish();
                Intent intent = new Intent(activity, MainActivity.class);
                startActivity(intent);
            } else {
                Tools.CreateSimpleDialog(activity, getString(R.string.login_fail));
            }
        }
    }
}
