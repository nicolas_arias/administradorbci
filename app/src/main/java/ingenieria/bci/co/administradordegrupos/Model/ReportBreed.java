package ingenieria.bci.co.administradordegrupos.Model;

/**
 * Created by nicolas on 7/9/17.
 */

public class ReportBreed {
    public String Name;
    public String Type;
    public String Breed;
    public String Total;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getBreed() {
        return Breed;
    }

    public void setBreed(String breed) {
        Breed = breed;
    }

    public String getTotal() {
        return Total;
    }

    public void setTotal(String total) {
        Total = total;
    }
}
