package ingenieria.bci.co.administradordegrupos;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.Herds;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
import ingenieria.bci.co.administradordegrupos.View.Register.register_step1;

public class HerdsAddActivity extends AppCompatActivity {

    EditText txtHerdName;
    Button btnSaveHerd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.herdsTitle_add);
        setContentView(R.layout.activity_herds_add);
        btnSaveHerd = (Button) findViewById(R.id.btnSaveHerd);
        txtHerdName = (EditText) findViewById(R.id.txtHerdName);
        btnSaveHerd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String HerdName = txtHerdName.getText().toString();
                if (HerdName.isEmpty()) {
                    Toast.makeText(HerdsAddActivity.this, getString(R.string.completeFields), Toast.LENGTH_LONG).show();
                } else {
                    Database db = new Database(HerdsAddActivity.this);
                    Herds herd = new Herds();
                    herd.setHerdName(HerdName);
                    herd.setHerdStatus("ACTIVO");
                    if (db.AddHerd(herd)) {
                        finishActivity();
                    }
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    public void finishActivity() {
        this.finish();
    }
}
