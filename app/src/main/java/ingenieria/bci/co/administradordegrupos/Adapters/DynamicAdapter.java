package ingenieria.bci.co.administradordegrupos.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Model.Breeds;
import ingenieria.bci.co.administradordegrupos.Model.DynamicList;
import ingenieria.bci.co.administradordegrupos.R;

/**
 * Created by nicolas on 7/2/17.
 */

public class DynamicAdapter extends ArrayAdapter<DynamicList> {
    final Context mainContext;
    final Activity activity;

    public DynamicAdapter(Context context, ArrayList<DynamicList> dynamic, Activity activity) {
        super(context, 0, dynamic);
        this.mainContext = context;
        this.activity = activity;
    }

    private static class ViewHolder {
        TextView txtName;
        CheckBox ckSelected;
        TextView txtValue;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final DynamicList dynamic = getItem(position);

        DynamicAdapter.ViewHolder viewHolder;
        viewHolder = new DynamicAdapter.ViewHolder();
        if (convertView == null) {

            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.dynamic_list, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.txtName);
            viewHolder.txtValue = (TextView) convertView.findViewById(R.id.txtValue);
            viewHolder.ckSelected = (CheckBox) convertView.findViewById(R.id.ckSelected);
            viewHolder.txtValue.setText(dynamic.getValue().trim());
            viewHolder.txtName.setText(dynamic.getText().trim());
            convertView.setTag(viewHolder);

            viewHolder.ckSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    Object ob = cb.getTag();
                    DynamicList country = (DynamicList) cb.getTag();
                    country.setSelected(cb.isChecked());
                }
            });
        } else {
            viewHolder = (DynamicAdapter.ViewHolder) convertView.getTag();
        }
        viewHolder.ckSelected.setChecked(dynamic.getSelected());
        viewHolder.ckSelected.setTag(dynamic);
        return convertView;
    }
}
