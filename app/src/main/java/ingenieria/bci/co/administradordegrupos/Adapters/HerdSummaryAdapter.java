package ingenieria.bci.co.administradordegrupos.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Model.AnimalWeight;
import ingenieria.bci.co.administradordegrupos.Model.ReportBreed;
import ingenieria.bci.co.administradordegrupos.R;

/**
 * Created by nicolas on 7/9/17.
 */

public class HerdSummaryAdapter extends ArrayAdapter<ReportBreed> {
    final Context mainContext;

    public HerdSummaryAdapter(Context context, ArrayList<ReportBreed> report) {
        super(context, 0, report);
        this.mainContext = context;
    }

    private static class ViewHolder {
        TextView farmName;
        TextView typeName;
        TextView breedName;
        TextView countBreed;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ReportBreed report = getItem(position);

        final ReportBreed reportOld;

        if (position == 0) {
            reportOld = new ReportBreed();
            reportOld.setName("");
        } else
            reportOld = getItem(position - 1);

        HerdSummaryAdapter.ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new HerdSummaryAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.farm_summary, parent, false);

            viewHolder.farmName = (TextView) convertView.findViewById(R.id.farmName);
            viewHolder.typeName = (TextView) convertView.findViewById(R.id.typeName);
            viewHolder.breedName = (TextView) convertView.findViewById(R.id.breedName);
            viewHolder.countBreed = (TextView) convertView.findViewById(R.id.countBreed);

            if (!report.Name.equals(reportOld.Name)) {
                viewHolder.farmName.setText(report.Name);
            } else {
                viewHolder.farmName.setHeight(0);
                viewHolder.farmName.setVisibility(View.INVISIBLE);
            }
            viewHolder.typeName.setText(report.Type);
            viewHolder.breedName.setText(report.Breed);
            viewHolder.countBreed.setText(report.Total);

        } else {
            viewHolder = (HerdSummaryAdapter.ViewHolder) convertView.getTag();
        }
        return convertView;
    }
}
