package ingenieria.bci.co.administradordegrupos.Model;

/**
 * Created by nicolas on 6/13/17.
 */

public class Herds {
    public int HerdId;
    public String HerdName;
    public String HerdStatus;
    public Herds(){

    }
    public Herds(int herdId, String herdName,String status) {
        HerdId = herdId;
        HerdName = herdName;
        HerdStatus = status;
    }
    public int getHerdId() {
        return HerdId;
    }
    public String getHerdName() {
        return HerdName;
    }
    public String getStatus(){
        return HerdStatus;
    }

    public void setHerdId(int herdId) {
        HerdId = herdId;
    }

    public void setHerdName(String herdName) {
        HerdName = herdName;
    }

    public void setHerdStatus(String herdStatus) {
        HerdStatus = herdStatus;
    }
}
