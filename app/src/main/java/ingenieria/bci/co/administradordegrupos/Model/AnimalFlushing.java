package ingenieria.bci.co.administradordegrupos.Model;

/**
 * Created by nicolas on 7/2/17.
 */

public class AnimalFlushing {
    public int FlushingId;
    public String AnimalEID;
    public String AnimalVisualNumber;
    public String DateFlushed;
    public String NoEmbryos;
    public String Comments;

    public AnimalFlushing() {
    }

    public int getFlushingId() {
        return FlushingId;
    }

    public void setFlushingId(int flushingId) {
        FlushingId = flushingId;
    }

    public String getAnimalEID() {
        return AnimalEID;
    }

    public void setAnimalEID(String animalEID) {
        AnimalEID = animalEID;
    }

    public String getAnimalVisualNumber() {
        return AnimalVisualNumber;
    }

    public void setAnimalVisualNumber(String animalVisualNumber) {
        AnimalVisualNumber = animalVisualNumber;
    }

    public String getDateFlushed() {
        return DateFlushed;
    }

    public void setDateFlushed(String dateFlushed) {
        DateFlushed = dateFlushed;
    }

    public String getNoEmbryos() {
        return NoEmbryos;
    }

    public void setNoEmbryos(String noEmbryos) {
        NoEmbryos = noEmbryos;
    }

    public String getComments() {
        return Comments;
    }

    public void setComments(String comments) {
        Comments = comments;
    }
}
