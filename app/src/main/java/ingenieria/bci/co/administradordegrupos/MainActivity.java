package ingenieria.bci.co.administradordegrupos;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
//import ingenieria.bci.co.administradordegrupos.View.bluetooth_tool;
import ingenieria.bci.co.administradordegrupos.Services.Tools;
import ingenieria.bci.co.administradordegrupos.View.settings;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    View headerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }

        setContentView(R.layout.activity_main);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerView = navigationView.getHeaderView(0);
        InitialConfiguration();
        setTitle(R.string.page_home_title);
    }

    private void InitialConfiguration() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView txtCurrentUser = (TextView) headerView.findViewById(R.id.txtCurrentUser);
        TextView txtEmail = (TextView) headerView.findViewById(R.id.textView);
        txtCurrentUser.setText(Tools.GetDefaultSetting(MainActivity.this, "user"));
        txtEmail.setText(Tools.GetDefaultSetting(MainActivity.this, "email"));
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ImageButton btnRegister = (ImageButton) findViewById(R.id.btnRegister);
        ImageButton BtnWeigh = (ImageButton) findViewById(R.id.BtnWeigh);
        ImageButton btncount = (ImageButton) findViewById(R.id.btncount);
        ImageButton btnManageAnimal = (ImageButton) findViewById(R.id.btnManageAnimal);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent registeractivity = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(registeractivity);
            }
        });
        btnManageAnimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent manageActivity = new Intent(MainActivity.this, ManageActivity.class);
                startActivity(manageActivity);
            }
        });
        btncount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent countActivity = new Intent(MainActivity.this, CountActivity.class);
                startActivity(countActivity);
            }
        });
        BtnWeigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent weighActivity = new Intent(MainActivity.this, WeighActivity.class);
                startActivity(weighActivity);
            }
        });
        OpenBtoothDialog();
    }


    private void OpenBtoothDialog() {

        try {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA,
                                Manifest.permission.BLUETOOTH,
                                Manifest.permission.BLUETOOTH_ADMIN,
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE
                        }, 0);
            }
        } catch (Exception ex) {
            Toast.makeText(this, getString(R.string.tryagain), Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            // super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_settings) {
            Intent config = new Intent(this, ConfigActivity.class);
            startActivity(config);
        } else if (id == R.id.nav_manage) {
            Intent config = new Intent(this, AdminActivity.class);
            startActivity(config);
        } else if (id == R.id.na_about) {
            Tools.CreateSimpleDialog(this, getString(R.string.about_smooper));
        } else if (id == R.id.nav_logout) {
            this.finish();
            Tools.SetDefaultSettings(MainActivity.this, "user", "");
            Tools.SetDefaultSettings(MainActivity.this, "remember", "");
            Intent config = new Intent(this, LoginActivity.class);

            startActivity(config);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


}
