package ingenieria.bci.co.administradordegrupos;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Adapters.AnimalAdapter;
import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.Animal;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

public class AnimalDetailActivity extends AppCompatActivity {

    Database db;
    Activity activity;
    TextView txtInfo;
    ImageView imgAnimal;
    Animal animal;
    TextView units;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_animal_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = AnimalDetailActivity.this;
        txtInfo = (TextView) findViewById(R.id.txtInfo);
        imgAnimal = (ImageView) findViewById(R.id.imgAnimal);
        units = (TextView) findViewById(R.id.units);
        units.setText(getString(R.string.animal_details) + " " + "( " + Tools.GetunitsWeight(activity) + " )");
        db = new Database(activity);
        Intent intet = getIntent();
        String EID = intet.getStringExtra("EID");

        txtInfo.setText(GetDetail(EID));

    }

    private String GetDetail(String EID) {
        animal = db.GetAnimalByEid(EID);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ContentResolver cr = getContentResolver();
                Bitmap bitmap = null;
                try {
                    if (animal.getAnimalPhoto() != null) {
                        bitmap = android.provider.MediaStore.Images.Media
                                .getBitmap(cr, Uri.parse(animal.getAnimalPhoto()));
                        imgAnimal.setImageBitmap(bitmap);
                    } else {
                        imgAnimal.setImageResource(R.drawable.bug);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        return "" + getString(R.string.detail_EID) + "  : " + animal.getAnimalEID() + "\n" +
                "" + getString(R.string.detail_visualnumber) + "  : " + animal.getAnimalVisualNumber() + "\n" +
                "" + getString(R.string.detail_type) + "  : " + animal.getAnimalType() + "\n" +
                "" + getString(R.string.detail_destination) + "  : " + animal.getAnimalDestination() + "\n" +
                "" + getString(R.string.detail_breed) + "  : " + animal.getAnimalBreed() + "\n" +
                "" + getString(R.string.detail_dateofbirth) + "  : " + animal.getAnimalDateOfBirth() + "\n" +
                "" + getString(R.string.detail_gender) + "  : " + animal.getAnimalGender() + "\n" +
                "" + getString(R.string.detail_herd) + "  : " + animal.getAnimalHerd() + "\n" +
                "" + getString(R.string.detail_farm) + "  : " + animal.getAnimalFarm() + "\n" +
                "" + getString(R.string.detail_current_weight) + "  : " + animal.getAnimalCurrentWeight() + "\n" +
                "" + getString(R.string.detail_birth_weight) + "  : " + animal.getAnimalBirthWeight() + "\n" +
                "" + getString(R.string.detail_weight_12) + "  : " + animal.getAnimalW12() + "\n" +
                "" + getString(R.string.detail_weight_18) + "  : " + animal.getAnimalW18() + "\n" +
                "" + getString(R.string.detail_weight_24) + "  : " + animal.getAnimalW24() + "\n";
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        intent = new Intent(this, DetailActivity.class);
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        } else if (id == R.id.pedigree) {
            String text = "";
            text += getString(R.string.detail_damid) + " : " + animal.getAnimalDamId() + "\n";
            text += getString(R.string.detail_sireid) + " : " + animal.getAnimalSireId() + "\n\n";

            text += getString(R.string.detail_grandamid) + " : " + animal.getAnimalDamGrandDamId() + "\n";
            text += getString(R.string.detail_grandsirid) + " : " + animal.getAnimalDamGrandSireId() + "\n\n";

            Tools.CreateSimpleDialog(activity, text);
        } else if (id == R.id.insemination) {
            intent.putExtra("title", getString(R.string.insemination_details));
            DetailActivity.arrayList = db.GetInseminationByEID(animal.getAnimalId());
            startActivity(intent);
        } else if (id == R.id.dipping) {
            intent.putExtra("title", getString(R.string.dipping_details));
            DetailActivity.arrayList = db.GetDippingById(animal.getAnimalEID());
            startActivity(intent);
        } else if (id == R.id.flushing) {
            intent.putExtra("title", getString(R.string.flushing_details));
            DetailActivity.arrayList = db.GetFlushingByID(animal.getAnimalEID());
            startActivity(intent);
        } else if (id == R.id.medication) {
            intent.putExtra("title", getString(R.string.medication_details));
            DetailActivity.arrayList = db.GetMedicationsByID(animal.getAnimalEID());
            startActivity(intent);
        } else if (id == R.id.weight) {
            intent.putExtra("title", getString(R.string.weight_history));
            DetailActivity.arrayList = db.GetWeighHistory(animal.getAnimalEID());
            startActivity(intent);
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

}
