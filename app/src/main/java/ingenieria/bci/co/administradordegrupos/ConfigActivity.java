package ingenieria.bci.co.administradordegrupos;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ingenieria.bci.co.administradordegrupos.Services.Bluetooth;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
import ingenieria.bci.co.administradordegrupos.Services.Tools;
import ingenieria.bci.co.administradordegrupos.View.settings;

public class ConfigActivity extends AppCompatActivity {
    ArrayList<String> arrayListBluetooth;
    ArrayAdapter<String> adapter;
    ArrayList<BluetoothDevice> devices;
    String btdata = "";

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_config);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        android.app.FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_container, new settings()).commit();
        setTitle(R.string.configuration_title);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.config_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        } else if (id == R.id.config_bt) {
            btdata = "weight";
            BluetoothList();
        } else if (id == R.id.config_bt_id) {
            btdata = "id";
            BluetoothList();
        } else if (id == R.id.config_bk_i) {
            BackUp("i");
        } else if (id == R.id.config_bk_e) {
            BackUp("e");
        }
        return super.onOptionsItemSelected(item);
    }

    public void BackUp(String type) {

        if (type.equals("e")) {
            String resultado = Tools.BackUpDataBase(ConfigActivity.this);
            Toast.makeText(ConfigActivity.this, resultado.isEmpty() ? "Se presentó un error realizando el back up" : "Back up realizado correctamente", Toast.LENGTH_LONG).show();
        } else {
            boolean result = Tools.BackUpRestore(ConfigActivity.this);
            Toast.makeText(ConfigActivity.this, !result ? "Se presentó un error restaurando el back up" : "Back up restaurado correctamente", Toast.LENGTH_LONG).show();
        }
    }

    public void BluetoothList() {
        final Bluetooth bluetooth = new Bluetooth(ConfigActivity.this);
        final ProgressDialog dialogp = new ProgressDialog(ConfigActivity.this);
        dialogp.setTitle(ConfigActivity.this.getResources().getString(R.string.bci));
        dialogp.setMessage(ConfigActivity.this.getResources().getString(R.string.loadingTitle));
        devices = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(ConfigActivity.this);
        LayoutInflater inflater = ConfigActivity.this.getLayoutInflater();
        final View vista = inflater.inflate(R.layout.bluetooth_devices, null);
        final EditText txtCurrentDevice = (EditText) vista.findViewById(R.id.txtCurrentDevice);
        final TextView title_bt = (TextView) vista.findViewById(R.id.title_bt);
        ListView listDevices = (ListView) vista.findViewById(R.id.listDevices);
        if (btdata.equals("id")) {
            title_bt.setText(getString(R.string.current_device) + " - " + getString(R.string.identification_machine));
            txtCurrentDevice.setText(Tools.GetDefaultSetting(ConfigActivity.this, "btid"));
        } else {
            title_bt.setText(getString(R.string.current_device) + " - " + getString(R.string.weighing_machine));
            txtCurrentDevice.setText(Tools.GetDefaultSetting(ConfigActivity.this, "btweight"));
        }
        arrayListBluetooth = new ArrayList<>();
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, new ArrayList<String>());
        listDevices.setAdapter(adapter);
        listDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = arrayListBluetooth.get(position);
                try {
                    BluetoothDevice device = devices.get(position);
                    List<BluetoothDevice> paired = bluetooth.getPairedDevices();
                    boolean isPaired = false;
                    for (BluetoothDevice pair : paired) {
                        if (device.getAddress().equals(pair.getAddress())) {
                            isPaired = true;
                        }
                    }
                    if (!isPaired) {
                        dialogp.show();
                        bluetooth.pair(device);
                    } else
                        Tools.CreateSimpleDialog(ConfigActivity.this, getString(R.string.complete));
                } catch (Exception e) {

                }
                if (btdata.equals("id")) {
                    Tools.SetDefaultSettings(ConfigActivity.this, "btid", name);
                    title_bt.setText(getString(R.string.current_device) + " - " + getString(R.string.identification_machine));

                    txtCurrentDevice.setText(Tools.GetDefaultSetting(ConfigActivity.this, "btid"));
                } else {
                    Tools.SetDefaultSettings(ConfigActivity.this, "btweight", name);
                    title_bt.setText(getString(R.string.current_device) + " - " + getString(R.string.weighing_machine));
                    txtCurrentDevice.setText(Tools.GetDefaultSetting(ConfigActivity.this, "btweight"));
                }


            }
        });

        builder.setView(vista)
                .setNegativeButton(R.string.btn_close, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();
                    }
                });

        builder.create().show();


        bluetooth.setDiscoveryCallback(new Bluetooth.DiscoveryCallback() {

            @Override
            public void onFinish() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogp.dismiss();
                    }
                });
            }

            @Override
            public void onDevice(final BluetoothDevice device) {


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            arrayListBluetooth.add(device.getName() + "");
                            adapter.add(device.getName() + " - " + device.getAddress());
                            devices.add(device);
                        } catch (Exception e) {
                            arrayListBluetooth.add("null");
                        }
                        adapter.notifyDataSetChanged();

                    }
                });
            }

            @Override
            public void onPair(BluetoothDevice device) {
                Tools.CreateSimpleDialog(ConfigActivity.this, getString(R.string.paired) + " | Device : " + device.getName());
                dialogp.dismiss();
            }

            @Override
            public void onUnpair(BluetoothDevice device) {

            }

            @Override
            public void onError(String message) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialogp.dismiss();
                    }
                });
            }
        });
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialogp.show();
            }
        });
        bluetooth.enableBluetooth();
        bluetooth.scanDevices();
    }
}
