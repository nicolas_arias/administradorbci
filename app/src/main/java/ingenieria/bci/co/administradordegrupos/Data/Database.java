package ingenieria.bci.co.administradordegrupos.Data;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import ingenieria.bci.co.administradordegrupos.Model.Animal;
import ingenieria.bci.co.administradordegrupos.Model.AnimalCondition;
import ingenieria.bci.co.administradordegrupos.Model.AnimalFlushing;
import ingenieria.bci.co.administradordegrupos.Model.AnimalInsemination;
import ingenieria.bci.co.administradordegrupos.Model.AnimalSales;
import ingenieria.bci.co.administradordegrupos.Model.AnimalWeight;
import ingenieria.bci.co.administradordegrupos.Model.Breeds;
import ingenieria.bci.co.administradordegrupos.Model.DynamicList;
import ingenieria.bci.co.administradordegrupos.Model.Farms;
import ingenieria.bci.co.administradordegrupos.Model.Herds;
import ingenieria.bci.co.administradordegrupos.Model.Medications;
import ingenieria.bci.co.administradordegrupos.Model.ReportBreed;
import ingenieria.bci.co.administradordegrupos.R;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

/**
 * Created by nicolas on 6/13/17.
 */

public class Database extends SQLiteOpenHelper {

    // Version Numero 1
    // Creación Tablas : Users,Herds,Farms,Breeds,Medications.
    // Creación Usuario por defecto : adminbci.

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "administradorbci";
    String creationString = "";
    Context activity;


    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        activity = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DatabaseTables.AnimalTable);
        db.execSQL(DatabaseTables.MedicationsTable);
        db.execSQL(DatabaseTables.HerdsTable);
        db.execSQL(DatabaseTables.FarmsTable);
        db.execSQL(DatabaseTables.BreedsTable);
        db.execSQL(DatabaseTables.AnimalTypeTable);
        db.execSQL(DatabaseTables.AnimalChageStatusTable);
        db.execSQL(DatabaseTables.AnimalInsemination);
        db.execSQL(DatabaseTables.AnimalDipping);
        db.execSQL(DatabaseTables.AnimalFlushing);
        db.execSQL(DatabaseTables.AnimalDisease);
        db.execSQL(DatabaseTables.AnimalSales);
        db.execSQL(DatabaseTables.AnimalVaccine);
        db.execSQL(DatabaseTables.AnimalCondition);
        db.execSQL(DatabaseTables.AnimalWeighHistory);
        db.execSQL(DatabaseTables.Events);
        db.execSQL(DatabaseTables.AnimalCountHistory);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    /* Region Herds */
    public ArrayList<Herds> GetHerds() {
        ArrayList<Herds> herdList = new ArrayList<>();
        String query = "SELECT  * FROM Herds Where HerdStatus = 'ACTIVO' ORDER BY HerdName DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Herds herd = null;
        if (cursor.moveToFirst()) {
            do {
                herd = new Herds();
                herd.setHerdId(cursor.getInt(0));
                herd.setHerdName(cursor.getString(1));
                herd.setHerdStatus(cursor.getString(2));
                herdList.add(herd);
            } while (cursor.moveToNext());
        }
        return herdList;
    }

    public void InactivateHerds(int HerdId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete("Herds", " HerdId = ?", new String[]{String.valueOf(HerdId)});
        } catch (Exception e) {
            Log.e("error", e.getMessage());
        }
    }

    public boolean AddHerd(Herds herd) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("HerdName", herd.getHerdName());
        contentValues.put("HerdStatus", herd.getStatus());
        long id = 0;
        try {
            id = db.insert("Herds", null, contentValues);
        } catch (Exception e) {
            id = 0;
        }
        return id > 0;
    }

    /* Region Farms*/
    public ArrayList<Farms> GetFarms() {
        ArrayList<Farms> farmList = new ArrayList<>();
        String query = "SELECT  * FROM Farms Where FarmStatus = 'ACTIVO' ORDER BY FarmName DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Farms farm = null;
        if (cursor.moveToFirst()) {
            do {
                farm = new Farms();
                farm.setFarmId(cursor.getInt(0));
                farm.setFarmName(cursor.getString(1));
                farm.setFarmStatus(cursor.getString(2));
                farm.setFarmSize(cursor.getDouble(3));
                farm.setFarmLng(cursor.getString(4));
                farm.setFarmLat(cursor.getString(5));
                farmList.add(farm);
            } while (cursor.moveToNext());
        }
        return farmList;
    }

    public boolean AddFarm(Farms farm) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("FarmName", farm.getFarmName());
        contentValues.put("FarmStatus", farm.getFarmStatus());
        contentValues.put("FarmSize", farm.getFarmSize());
        contentValues.put("FarmLng", farm.getFarmLng());
        contentValues.put("FarmLat", farm.getFarmLat());
        long id = 0;
        try {
            id = db.insert("Farms", null, contentValues);
        } catch (Exception e) {
            id = 0;
        }
        return id > 0;
    }

    public void InactivateFarms(int FarmId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete("Farms", " FarmId = ?", new String[]{String.valueOf(FarmId)});
        } catch (Exception e) {
            Log.e("error", e.getMessage());
        }
    }

    /* Region Breeds */
    public ArrayList<Breeds> GetBreeds() {
        ArrayList<Breeds> breedList = new ArrayList<>();
        String query = "SELECT  * FROM Breeds Where BreedStatus = 'ACTIVO' ORDER BY BreedName DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Breeds breed = null;
        if (cursor.moveToFirst()) {
            do {
                breed = new Breeds();
                breed.setBreedId(cursor.getInt(0));
                breed.setBreedName(cursor.getString(1));
                breed.setBreedType(cursor.getString(2));
                breed.setBreedDestination(cursor.getString(3));
                breedList.add(breed);
            } while (cursor.moveToNext());
        }
        return breedList;
    }

    public boolean AddBreed(Breeds breed) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("BreedName", breed.getBreedName());
        contentValues.put("BreedType", breed.getBreedType());
        contentValues.put("BreedDestination", breed.getBreedDestination());
        contentValues.put("BreedStatus", "ACTIVO");
        long id = 0;
        try {
            id = db.insert("Breeds", null, contentValues);
        } catch (Exception e) {
            id = 0;
        }
        return id > 0;
    }

    public void InactivateBreeds(int BreedId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.delete("Breeds", " BreedId = ?", new String[]{String.valueOf(BreedId)});
        } catch (Exception e) {
            Log.e("error", e.getMessage());
        }
    }

    public ArrayList<Breeds> GetBreedsByType(String type) {
        ArrayList<Breeds> breedList = new ArrayList<>();
        String query = "SELECT  * FROM Breeds Where BreedStatus = 'ACTIVO' and BreedType = '" + type + "' ORDER BY BreedName DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Breeds breed = null;
        if (cursor.moveToFirst()) {
            do {
                breed = new Breeds();
                breed.setBreedId(cursor.getInt(0));
                breed.setBreedName(cursor.getString(1));
                breed.setBreedType(cursor.getString(2));
                breed.setBreedDestination(cursor.getString(3));
                breedList.add(breed);
            } while (cursor.moveToNext());
        }
        return breedList;
    }

    /* Region Medication */
    public void InactivateMedicine(int MedicineId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("MedicationStatus", "INACTIVO");
        try {
            db.update("Medications", contentValues, " MedicationId = ?", new String[]{String.valueOf(MedicineId)});
        } catch (Exception e) {
            Log.e("error", e.getMessage());
        }
    }

    public ArrayList<Medications> GetMedications() {
        ArrayList<Medications> medicationList = new ArrayList<>();
        String query = "SELECT  * FROM Medications Where MedicationStatus = 'ACTIVO' ORDER BY MedicationId DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Medications medications = null;
        if (cursor.moveToFirst()) {
            do {
                medications = new Medications();
                medications.setMedicationId(cursor.getInt(0));
                medications.setMedicationName(cursor.getString(1));
                medications.setMedicationPrice(cursor.getDouble(2));
                medications.setMedicationQuantity(cursor.getDouble(3));

                medications.setMedicationExpiryDate(cursor.getString(4));
                medications.setMedicationSupplierName(cursor.getString(5));
                medications.setMedicationSupplierTelephone(cursor.getString(6));
                medications.setMedicationStatus(cursor.getString(7));

                medicationList.add(medications);
            } while (cursor.moveToNext());
        }
        return medicationList;
    }

    public boolean AddMedicine(Medications medicine) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("MedicationName", medicine.getMedicationName());
        contentValues.put("MedicationPrice", medicine.getMedicationPrice());
        contentValues.put("MedicationQuantity", medicine.getMedicationQuantity());
        contentValues.put("MedicationExpiryDate", medicine.getMedicationExpiryDate());
        contentValues.put("MedicationSupplierName", medicine.getMedicationSupplierName());
        contentValues.put("MedicationSupplierTelephone", medicine.getMedicationSupplierTelephone());
        contentValues.put("MedicationStatus", "ACTIVO");
        long id = 0;
        try {
            id = db.insert("Medications", null, contentValues);
        } catch (Exception e) {
            id = 0;
        }
        return id > 0;
    }

    public boolean UpdateMedicineStock(int idMedicine, String qty) {
        String strSQL = "UPDATE Medications SET MedicationQuantity = MedicationQuantity + " + qty + " WHERE MedicationId = " + idMedicine;
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL(strSQL);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            return false;
        }
        return true;
    }


    /* Region Animal*/
    public boolean AddAnimal(Animal animal) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("AnimalEID", animal.getAnimalEID());
        contentValues.put("AnimalVisualNumber", animal.getAnimalVisualNumber());
        contentValues.put("AnimalDateOfBirth", animal.getAnimalDateOfBirth());
        contentValues.put("AnimalType", animal.getAnimalType());
        contentValues.put("AnimalGroup", animal.getAnimalGroup());
        contentValues.put("AnimalDestination", animal.getAnimalDestination());
        contentValues.put("AnimalBreed", animal.getAnimalBreed());
        contentValues.put("AnimalGender", animal.getAnimalGender());
        contentValues.put("AnimalBirthWeight", animal.getAnimalBirthWeight());
        contentValues.put("AnimalCurrentWeight", animal.getAnimalCurrentWeight());
        contentValues.put("AnimalDamId", animal.getAnimalDamId());
        contentValues.put("AnimalDamGrandDamId", animal.getAnimalDamGrandDamId());
        contentValues.put("AnimalDamGrandSireId", animal.getAnimalDamGrandSireId());
        contentValues.put("AnimalSireId", animal.getAnimalSireId());
        contentValues.put("AnimalSireGrandDamId", animal.getAnimalSireGrandDamId());
        contentValues.put("AnimalSireGrandSireId", animal.getAnimalSireGrandSireId());
        contentValues.put("AnimalEmbryo", animal.getAnimalEmbryo());
        contentValues.put("AnimalSurrogateDamId", animal.getAnimalSurrogateDamId());
        contentValues.put("AnimalHerd", animal.getAnimalHerd());
        contentValues.put("AnimalFarm", animal.getAnimalFarm());
        contentValues.put("AnimalPhoto", animal.getAnimalPhoto());
        contentValues.put("AnimalStatus", "ACTIVO");
        long id = 0;
        try {
            id = db.insert("Animal", null, contentValues);
        } catch (Exception e) {
            id = 0;
        }
        return id > 0;
    }

    public ArrayList<Animal> GetAnimals() {
        ArrayList<Animal> animalList = new ArrayList<>();
        String query = "SELECT  AnimalId," +
                "AnimalVisualNumber," +
                "AnimalDateOfBirth," +
                "AnimalBreed," +
                "AnimalPhoto," +
                "AnimalFarm," +
                "AnimalEID," +
                "AnimalHerd," +
                "AnimalType," +
                "AnimalGender," +
                "AnimalCurrentWeight," +
                "AnimalW12," +
                "AnimalW18," +
                "AnimalW24" +
                " FROM Animal Where AnimalStatus = 'ACTIVO' ORDER BY AnimalId DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Animal animal = null;
        if (cursor.moveToFirst()) {
            do {
                animal = new Animal();
                animal.setAnimalId(cursor.getInt(0));
                animal.setAnimalVisualNumber(cursor.getString(1));
                animal.setAnimalDateOfBirth(cursor.getString(2));
                animal.setAnimalBreed(cursor.getString(3));
                animal.setAnimalPhoto(cursor.getString(4));
                animal.setAnimalFarm(cursor.getString(5));
                animal.setAnimalEID(cursor.getString(6));
                animal.setAnimalHerd(cursor.getString(7));
                animal.setAnimalType(cursor.getString(8));
                animal.setAnimalGender(cursor.getString(9));

                animal.setAnimalCurrentWeight(cursor.getString(10));
                animal.setAnimalW12(cursor.getString(11));
                animal.setAnimalW18(cursor.getString(12));
                animal.setAnimalW24(cursor.getString(13));

                animalList.add(animal);
            } while (cursor.moveToNext());
        }
        return animalList;

    }

    public ArrayList<Animal> GetAnimalsFilter(String type, String breed, String farm, String herd) {
        ArrayList<Animal> animalList = new ArrayList<>();
        String all = activity.getResources().getString(R.string.all);
        if (type.equals(all)) {
            type = "%";
        }
        if (breed.equals(all)) {
            breed = "%";
        }
        if (farm.equals(all)) {
            farm = "%";
        }
        if (herd.equals(all)) {
            herd = "%";
        }

        String query = "SELECT  AnimalId," +
                "AnimalVisualNumber," +
                "AnimalDateOfBirth," +
                "AnimalBreed," +
                "AnimalPhoto," +
                "AnimalFarm," +
                "AnimalEID," +
                "AnimalHerd," +
                "AnimalType," +
                "AnimalGender" +
                " FROM Animal WHERE AnimalStatus = 'ACTIVO' and  AnimalType like '" + type + "' and" +
                " AnimalBreed like '" + breed + "' and" +
                " AnimalFarm like '" + farm + "' and" +
                " AnimalHerd like '" + herd + "' ORDER BY AnimalId DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Animal animal = null;
        if (cursor.moveToFirst()) {
            do {
                animal = new Animal();
                animal.setAnimalId(cursor.getInt(0));
                animal.setAnimalVisualNumber(cursor.getString(1));
                animal.setAnimalDateOfBirth(cursor.getString(2));
                animal.setAnimalBreed(cursor.getString(3));
                animal.setAnimalPhoto(cursor.getString(4));
                animal.setAnimalFarm(cursor.getString(5));
                animal.setAnimalEID(cursor.getString(6));
                animal.setAnimalHerd(cursor.getString(7));
                animal.setAnimalType(cursor.getString(8));
                animal.setAnimalGender(cursor.getString(9));
                animalList.add(animal);
            } while (cursor.moveToNext());
        }
        return animalList;

    }

    public ArrayList<Animal> GetAnimalsByID(String id) {
        ArrayList<Animal> animalList = new ArrayList<>();

        String query = "SELECT  AnimalId," +
                "AnimalVisualNumber," +
                "AnimalDateOfBirth," +
                "AnimalBreed," +
                "AnimalPhoto," +
                "AnimalFarm," +
                "AnimalEID," +
                "AnimalHerd," +
                "AnimalType," +
                "AnimalGender" +
                " FROM Animal WHERE AnimalStatus = 'ACTIVO' and AnimalEID like '" + id + "%' or " +
                " AnimalVisualNumber like '" + id + "%' ORDER BY AnimalEID ASC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Animal animal = null;
        if (cursor.moveToFirst()) {
            do {
                animal = new Animal();
                animal.setAnimalId(cursor.getInt(0));
                animal.setAnimalVisualNumber(cursor.getString(1));
                animal.setAnimalDateOfBirth(cursor.getString(2));
                animal.setAnimalBreed(cursor.getString(3));
                animal.setAnimalPhoto(cursor.getString(4));
                animal.setAnimalFarm(cursor.getString(5));
                animal.setAnimalEID(cursor.getString(6));
                animal.setAnimalHerd(cursor.getString(7));
                animal.setAnimalType(cursor.getString(8));
                animal.setAnimalGender(cursor.getString(9));

                animalList.add(animal);
            } while (cursor.moveToNext());
        }
        return animalList;
    }

    public void InactivateAnimals(int animalId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("AnimalStatus", "INACTIVO");
        try {
            db.update("Animal", contentValues, " AnimalId = ?", new String[]{String.valueOf(animalId)});
        } catch (Exception e) {
            Log.e("error", e.getMessage());
        }
    }

    public boolean UpdateAnimal(int animalId, String newStatus, String newDescription, String newFarm, String newHerd) {
        SQLiteDatabase db = this.getWritableDatabase();
        Date cDate = new Date();
        String fDate = new SimpleDateFormat("yyyy-MM-dd").format(cDate);
        try {
            if (newStatus.equals("Active") || newStatus.equals("Activo"))
                newStatus = "ACTIVO";
            db.execSQL("update Animal set AnimalStatus = '" + newStatus + "',AnimalHerd = '" + newHerd + "',AnimalFarm = '" + newFarm + "' where AnimalId = '" + animalId + "'");
            db.execSQL("insert into ChangeStatus (ChangeDescription,ChangeDate,AnimalId) values ('" + newDescription + "','" + fDate + "'," + animalId + ")");
            return true;
        } catch (Exception e) {
            Log.e("error", e.getMessage());
            return false;
        }
    }

    public boolean AddAnimalInsemination(AnimalInsemination process) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("AnimalId", process.getAnimalId());
        contentValues.put("CalvingDate", process.getCalvingDate());
        contentValues.put("NoStraws", process.getNoStraws());
        contentValues.put("SireId", process.getSireId());
        contentValues.put("Comments", process.getComments());
        contentValues.put("Status", process.getStatus());
        long id = 0;
        try {
            id = db.insert("AnimalInsemination", null, contentValues);
        } catch (Exception e) {
            id = 0;
        }
        return id > 0;
    }

    public ArrayList<DynamicList> GetAnimalsSell(String query) {
        ArrayList<DynamicList> dynamicList = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        DynamicList dynamic = null;
        if (cursor.moveToFirst()) {
            do {
                dynamic = new DynamicList();
                dynamic.setId(cursor.getInt(0));
                dynamic.setText(cursor.getString(1));
                dynamic.setValue(cursor.getString(2));
                dynamic.setSelected(false);
                dynamicList.add(dynamic);
            } while (cursor.moveToNext());
        }
        return dynamicList;
    }
    /* Region General */

    public ArrayList<String> getStringArray(String model, String field, String condition) {
        ArrayList<String> stringArray = new ArrayList<>();
        String query = "";
        if (!condition.isEmpty()) {
            query = "SELECT " + field + " FROM " + model + " WHERE  " + condition + " ORDER BY " + field + " ASC";
        } else {
            query = "SELECT " + field + " FROM " + model + " ORDER BY " + field + " ASC";
        }
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                stringArray.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return stringArray;

    }

    public boolean SaveSale(AnimalSales animalSales, ArrayList<String> idData) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        String date = Tools.GetDate(false);
        int value = (SecuenceAnimalSales() + 1);
        long id = 0;
        try {
            for (String EID : idData) {
                contentValues.put("AnimalSaleSecuence", value);
                contentValues.put("AnimalEID", EID);
                contentValues.put("SaleDate", date);
                contentValues.put("PaymentType", animalSales.getPaymentType());
                contentValues.put("Buyer", animalSales.getBuyer());
                contentValues.put("Reference", animalSales.getReference());
                contentValues.put("Price", animalSales.getPrice());
                id = db.insert("AnimalSales", null, contentValues);
            }
        } catch (Exception e) {
            id = 0;
        }
        return id > 0;

    }


    public int SecuenceAnimalSales() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select ifnull(max(AnimalSaleSecuence),0) from AnimalSales";
        Cursor cursor = db.rawQuery(query, null);
        int value = 0;
        if (cursor.moveToFirst()) {
            do {
                value = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        return value;
    }

    public int SecuenceAnimalDipping() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select ifnull(max(Secuence),0) from AnimalDipping";
        Cursor cursor = db.rawQuery(query, null);
        int value = 0;
        if (cursor.moveToFirst()) {
            do {
                value = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        return value;
    }

    public int SecuenceAnimalDisease() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select ifnull(max(Secuence),0) from AnimalDisease";
        Cursor cursor = db.rawQuery(query, null);
        int value = 0;
        if (cursor.moveToFirst()) {
            do {
                value = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        return value;
    }

    public int SecuenceAnimalVaccinate() {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select ifnull(max(Secuence),0) from AnimalVaccine";
        Cursor cursor = db.rawQuery(query, null);
        int value = 0;
        if (cursor.moveToFirst()) {
            do {
                value = cursor.getInt(0);
            } while (cursor.moveToNext());
        }
        return value;
    }

    public String GetLastSeenCondition(String EID) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select ifnull(LastSeen,'') from AnimalCondition where AnimalEID = '" + EID + "' Order by RegisterId DESC LIMIT 1";
        Cursor cursor = db.rawQuery(query, null);
        String value = "";
        if (cursor.moveToFirst()) {
            do {
                value = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        return value;
    }

    public boolean UpdateMedicationDosage(String medicine, String dosage) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL("update Medications set MedicationQuantity = MedicationQuantity - " + dosage + " where MedicationName = '" + medicine + "'");
            return true;
        } catch (Exception e) {
            Log.e("error", e.getMessage());
            return false;
        }
    }

    public boolean RegisterFlushing(AnimalFlushing flushing) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("AnimalEID", flushing.getAnimalEID());
        contentValues.put("AnimalVisualNumber", flushing.getAnimalVisualNumber());
        contentValues.put("DateFlushed", flushing.getDateFlushed());
        contentValues.put("NoEmbryos", flushing.getNoEmbryos());
        contentValues.put("Comments", flushing.getComments());
        long id = 0;
        try {
            id = db.insert("AnimalFlushing", null, contentValues);
        } catch (Exception e) {
            id = 0;
        }
        return id > 0;
    }

    public boolean RegisterDipping(String date, String method, String comments, ArrayList<String> idData) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        int value = (SecuenceAnimalDipping() + 1);
        long id = 0;
        try {
            for (String EID : idData) {
                contentValues.put("AnimalEID", EID);
                contentValues.put("Date", date);
                contentValues.put("Method", method);
                contentValues.put("Comments", comments);
                contentValues.put("Secuence", value);
                id = db.insert("AnimalDipping", null, contentValues);
            }
        } catch (Exception e) {
            id = 0;
        }
        return id > 0;
    }

    public boolean RegisterDisease(ArrayList<String> idData, String type, String date, String comments) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        int value = (SecuenceAnimalDisease() + 1);
        long id = 0;
        try {
            for (String EID : idData) {
                contentValues.put("AnimalEID", EID);
                contentValues.put("Date", date);
                contentValues.put("DiseaseType", type);
                contentValues.put("Comments", comments);
                contentValues.put("Secuence", value);
                id = db.insert("AnimalDisease", null, contentValues);
            }
        } catch (Exception e) {
            id = 0;
        }
        return id > 0;
    }

    public boolean RegisterVaccinate(ArrayList<String> idData, String vaccine, String dosage, String date, String comments) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        int value = (SecuenceAnimalVaccinate() + 1);
        long id = 0;
        try {
            for (String EID : idData) {
                contentValues.put("DosageVolume", dosage);
                contentValues.put("Date", date);
                contentValues.put("Comments", comments);
                contentValues.put("Secuence", value);
                contentValues.put("Vaccine", vaccine);
                contentValues.put("AnimalEID", EID);
                id = db.insert("AnimalVaccine", null, contentValues);
            }
        } catch (Exception e) {
            id = 0;
        }
        return id > 0;
    }

    public boolean RegisterAnimalCondition(AnimalCondition animalCondition) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("AnimalEID", animalCondition.getAnimalEID());
        contentValues.put("AnimalVisualNumber", animalCondition.getAnimalVisualNumber());
        contentValues.put("LastSeen", Tools.GetDate(false));
        contentValues.put("Symptoms", animalCondition.getSymptoms());
        contentValues.put("Condition", animalCondition.getCondition());
        contentValues.put("Diagnosis", animalCondition.getDiagnosis());
        contentValues.put("Medication", animalCondition.getMedication());
        contentValues.put("Dosage", animalCondition.getDosage());
        contentValues.put("Date", Tools.GetDate(false));

        long id = 0;
        try {
            id = db.insert("AnimalCondition", null, contentValues);
        } catch (Exception e) {
            id = 0;
        }
        return id > 0;
    }


    public Animal GetAnimalByEid(String eid) {
        String query = "SELECT * FROM Animal Where AnimalStatus = 'ACTIVO' and AnimalEID = '" + eid + "'  ORDER BY AnimalId DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Animal animal = null;
        if (cursor.moveToFirst()) {
            do {
                animal = new Animal();
                animal.setAnimalId(cursor.getInt(0));
                animal.setAnimalEID(cursor.getString(1));
                animal.setAnimalVisualNumber(cursor.getString(2));
                animal.setAnimalDateOfBirth(cursor.getString(3));
                animal.setAnimalType(cursor.getString(4));
                animal.setAnimalGroup(cursor.getString(5));
                animal.setAnimalDestination(cursor.getString(6));
                animal.setAnimalBreed(cursor.getString(7));
                animal.setAnimalGender(cursor.getString(8));
                animal.setAnimalBirthWeight(Tools.NumberW(cursor.getString(9), activity));
                animal.setAnimalCurrentWeight(cursor.getString(10));
                animal.setAnimalDamId(cursor.getString(11));
                animal.setAnimalDamGrandDamId(cursor.getString(12));
                animal.setAnimalDamGrandSireId(cursor.getString(13));
                animal.setAnimalSireId(cursor.getString(14));
                animal.setAnimalSireGrandDamId(cursor.getString(15));
                animal.setAnimalSireGrandSireId(cursor.getString(16));
                animal.setAnimalEmbryo(cursor.getString(17));
                animal.setAnimalSurrogateDamId(cursor.getString(18));
                animal.setAnimalHerd(cursor.getString(19));
                animal.setAnimalFarm(cursor.getString(20));
                animal.setAnimalPhoto(cursor.getString(21));
                animal.setAnimalStatus(cursor.getString(22));
                animal.setAnimalW12(cursor.getString(24));
                animal.setAnimalW18(cursor.getString(25));
                animal.setAnimalW24(cursor.getString(26));

            } while (cursor.moveToNext());
        }
        return animal;
    }

    public Animal GetAnimalByEidVn(String eid) {
        String query = "SELECT  * FROM Animal Where AnimalStatus = 'ACTIVO' and AnimalVisualNumber = '" + eid + "' ORDER BY AnimalId DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Animal animal = null;
        if (cursor.moveToFirst()) {
            do {
                animal = new Animal();
                animal.setAnimalId(cursor.getInt(0));
                animal.setAnimalEID(cursor.getString(1));
                animal.setAnimalVisualNumber(cursor.getString(2));
                animal.setAnimalDateOfBirth(cursor.getString(3));
                animal.setAnimalType(cursor.getString(4));
                animal.setAnimalGroup(cursor.getString(5));
                animal.setAnimalDestination(cursor.getString(6));
                animal.setAnimalBreed(cursor.getString(7));
                animal.setAnimalGender(cursor.getString(8));
                animal.setAnimalBirthWeight(Tools.NumberW(cursor.getString(9), activity));
                animal.setAnimalCurrentWeight(cursor.getString(10));
                animal.setAnimalDamId(cursor.getString(11));
                animal.setAnimalDamGrandDamId(cursor.getString(12));
                animal.setAnimalDamGrandSireId(cursor.getString(13));
                animal.setAnimalSireId(cursor.getString(14));
                animal.setAnimalSireGrandDamId(cursor.getString(15));
                animal.setAnimalSireGrandSireId(cursor.getString(16));
                animal.setAnimalEmbryo(cursor.getString(17));
                animal.setAnimalSurrogateDamId(cursor.getString(18));
                animal.setAnimalHerd(cursor.getString(19));
                animal.setAnimalFarm(cursor.getString(20));
                animal.setAnimalPhoto(cursor.getString(21));
                animal.setAnimalStatus(cursor.getString(22));
                animal.setAnimalW12(cursor.getString(24));
                animal.setAnimalW18(cursor.getString(25));
                animal.setAnimalW24(cursor.getString(26));

            } while (cursor.moveToNext());
        }
        return animal;
    }

    public ArrayList<String> GetInseminationByEID(int id) {
        ArrayList<String> arrayList = new ArrayList<>();
        String query = "select CalvingDate,NoStraws,Comments,SireId from AnimalInsemination where AnimalId = " + id + "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        Animal animal = null;
        if (cursor.moveToFirst()) {
            do {
                String item = "";
                item += activity.getString(R.string.calvingdate) + " : " + cursor.getString(0) + "\n";
                item += activity.getString(R.string.amount_straws) + " : " + cursor.getString(1) + "\n";
                item += activity.getString(R.string.notes) + " : " + cursor.getString(2) + "\n";
                item += activity.getString(R.string.detail_sireid) + " : " + cursor.getString(3) + "\n";
                arrayList.add(item);
            } while (cursor.moveToNext());
        }
        return arrayList;

    }

    public ArrayList<String> GetDippingById(String eid) {
        ArrayList<String> arrayList = new ArrayList<>();
        String query = "select Date,Method,Comments from AnimalDipping where AnimalEID = '" + eid + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                String item = "";
                item += activity.getString(R.string.date) + " : " + cursor.getString(0) + "\n";
                item += activity.getString(R.string.dipping_method) + " : " + cursor.getString(1) + "\n";
                item += activity.getString(R.string.notes) + " : " + cursor.getString(2) + "\n";
                arrayList.add(item);
            } while (cursor.moveToNext());
        }
        return arrayList;

    }

    public ArrayList<String> GetFlushingByID(String animalEID) {
        ArrayList<String> arrayList = new ArrayList<>();
        String query = "select DateFlushed,NoEmbryos,Comments from AnimalFlushing where AnimalEID = '" + animalEID + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                String item = "";
                item += activity.getString(R.string.dateflushed) + " : " + cursor.getString(0) + "\n";
                item += activity.getString(R.string.noembryos) + " : " + cursor.getString(1) + "\n";
                item += activity.getString(R.string.notes) + " : " + cursor.getString(2) + "\n";
                arrayList.add(item);
            } while (cursor.moveToNext());
        }
        return arrayList;

    }

    public ArrayList<String> GetMedicationsByID(String animalEID) {
        ArrayList<String> arrayList = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select Date,Comments,DiseaseType from AnimalDisease where AnimalEID = '" + animalEID + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                String item = "";
                item += activity.getString(R.string.disease) + "\n";
                item += activity.getString(R.string.date) + " : " + cursor.getString(0) + "\n";
                item += activity.getString(R.string.notes) + " : " + cursor.getString(1) + "\n";
                item += activity.getString(R.string.disease_type) + " : " + cursor.getString(2) + "\n";
                arrayList.add(item);
            } while (cursor.moveToNext());
        }

        query = "select Date,Vaccine,DosageVolume from AnimalVaccine where AnimalEID = '" + animalEID + "'";
        cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                String item = "";
                item += activity.getString(R.string.vaccine) + "\n";
                item += activity.getString(R.string.date) + " : " + cursor.getString(0) + "\n";
                item += activity.getString(R.string.vaccine) + " : " + cursor.getString(1) + "\n";
                item += activity.getString(R.string.dosage_volume) + " : " + cursor.getString(2) + "\n";
                arrayList.add(item);
            } while (cursor.moveToNext());
        }

        query = "select LastSeen,Condition,Diagnosis,Medication,Dosage from AnimalCondition where AnimalEID = '" + animalEID + "'";
        cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                String item = "";
                item += activity.getString(R.string.animalcondition) + "\n";
                item += activity.getString(R.string.last_seen_date) + " : " + cursor.getString(0) + "\n";
                item += activity.getString(R.string.condition) + " : " + cursor.getString(1) + "\n";
                item += activity.getString(R.string.diagnosis) + " : " + cursor.getString(2) + "\n";
                item += activity.getString(R.string.medication) + " : " + cursor.getString(3) + "\n";
                item += activity.getString(R.string.dosage_volume) + " : " + cursor.getString(4) + "\n";
                arrayList.add(item);
            } while (cursor.moveToNext());
        }
        return arrayList;

    }

    public ArrayList<AnimalSales> GetSalesHistory() {
        ArrayList<AnimalSales> arrayList = new ArrayList<>();
        String query = "select AnimalEID,SaleDate,PaymentType,Buyer,Reference,Price,AnimalSaleId from AnimalSales ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        AnimalSales animalsales = null;
        if (cursor.moveToFirst()) {
            do {
                animalsales = new AnimalSales();
                animalsales.setAnimalEID(cursor.getString(0));
                animalsales.setSaleDate(cursor.getString(1));
                animalsales.setPaymentType(cursor.getString(2));
                animalsales.setBuyer(cursor.getString(3));
                animalsales.setReference(cursor.getString(4));
                animalsales.setPrice(cursor.getString(5));
                animalsales.setAnimalSaleId(cursor.getInt(6));
                arrayList.add(animalsales);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }

    public ArrayList<String> GetSalesHistoryString() {
        ArrayList<String> arrayList = new ArrayList<>();
        String query = "select AnimalEID,SaleDate,PaymentType,Buyer,Reference,Price,AnimalSaleId from AnimalSales Order by AnimalSaleId DESC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                String item = "";
                item += activity.getString(R.string.detail_EID) + " : " + cursor.getString(0) + "\n";
                item += activity.getString(R.string.detail_sale_date) + " : " + cursor.getString(1) + "\n";
                item += activity.getString(R.string.detail_paymenttype) + " : " + cursor.getString(2) + "\n";
                item += activity.getString(R.string.detail_buyer) + " : " + cursor.getString(3) + "\n";
                item += activity.getString(R.string.detail_reference) + " : " + cursor.getString(4) + "\n";
                item += activity.getString(R.string.detail_price) + " : " + Tools.GetCurrency(activity) + " " + Tools.Number(cursor.getString(5));
                arrayList.add(item);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }

    public boolean UpdateAnimalWeight(String current, String month12, String month18, String month24, String animalEID) {
        String update = "update Animal set " +
                "AnimalCurrentWeight = '" + current + "'," +
                "AnimalW12 = '" + month12 + "'," +
                "AnimalW18 = '" + month18 + "'," +
                "AnimalW24 = '" + month24 + "' where AnimalEID = '" + animalEID + "'";
        SQLiteDatabase db = this.getWritableDatabase();

        try {
            String OldWeigh = GetDbString("select AnimalCurrentWeight from Animal where AnimalEID = '" + animalEID + "'");
            db.execSQL(update);
            db.execSQL("insert into AnimalWeigh (AnimalEID,Weight,Date,OldWeight) values ('" + animalEID + "','" + current + "','" + Tools.GetDate(true) + "','" + OldWeigh + "')");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean UpdateAnimalCurrentWeight(String current, String animalEID) {
        String update = "update Animal set " +
                "AnimalCurrentWeight = '" + current + "' " +
                " where AnimalEID = '" + animalEID + "' or AnimalVisualNumber = '" + animalEID + "'";
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            String OldWeigh = GetDbString("select AnimalCurrentWeight from Animal where AnimalEID = '" + animalEID + "' or AnimalVisualNumber = '" + animalEID + "'");
            db.execSQL(update);
            db.execSQL("insert into AnimalWeigh (AnimalEID,Weight,Date,OldWeight) values ('" + animalEID + "','" + current + "','" + Tools.GetDate(true) + "','" + OldWeigh + "')");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public ArrayList<String> GetWeighHistory(String animalEID) {
        ArrayList<String> arrayList = new ArrayList<>();
        String query = "select Date,Weight,Event,OldWeight from AnimalWeigh where AnimalEID = '" + animalEID + "' order by RegisterId DESC ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {

                double gain = 0;
                double lost = 0;
                double current = Double.valueOf(cursor.getString(1));
                double previous = Double.valueOf(cursor.getString(3));
                if (current < previous) {
                    gain = 0;
                    lost = previous - current;
                } else {
                    gain = current - previous;
                    lost = 0;
                }

                String item = "";
                item += activity.getString(R.string.detail_date) + " : " + cursor.getString(0) + "\n";
                item += activity.getString(R.string.detail_weight) + " : " + cursor.getString(1) + " " + Tools.GetunitsWeight(activity) + "\n";
                item += activity.getString(R.string.previous_weight) + " : " + cursor.getString(3) + " " + Tools.GetunitsWeight(activity) + "\n";
                item += activity.getString(R.string.gain) + " : " + gain + " " + Tools.GetunitsWeight(activity) + "\n";
                item += activity.getString(R.string.lost) + " : " + lost + " " + Tools.GetunitsWeight(activity) + "\n";
                item += activity.getString(R.string.event_name) + " : " + (cursor.getString(2) != null ? cursor.getString(2) : "");
                arrayList.add(item);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }

    public boolean InactivateEvent(String s) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL("delete from Events Where EventName = '" + s + "'");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean AddEvent(String s) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            db.execSQL("insert into Events (EventName,EventState) values ('" + s + "','ACTIVO')");
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public ArrayList<String> GetEvents() {
        ArrayList<String> arrayList = new ArrayList<>();
        String query = "select EventName from Events where EventState = 'ACTIVO' ";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                arrayList.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        return arrayList;
    }


    public String RegisterAnimalEvent(AnimalWeight animal, boolean weight) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("AnimalEID", animal.getAnimalEID());
        contentValues.put("Date", Tools.GetDate(true));
        contentValues.put("Event", animal.getEvent());
        if (weight) {
            contentValues.put("Weight", animal.getWeight());
            contentValues.put("OldWeight", animal.getOldWeight());
        }
        long id = 0;
        try {
            if (weight)
                id = db.insert("AnimalWeigh", null, contentValues);
            else
                id = db.insert("AnimalCount", null, contentValues);
        } catch (Exception e) {
            id = 0;
        }
        return String.valueOf(id);
    }

    public ArrayList<String> GetCountEvents() {
        ArrayList<String> arrayList = new ArrayList<>();
        String query = "Select Event,Date from AnimalCount Group by Event,Date ORDER BY Date Desc";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                String data = cursor.getString(0);
                if (data != null)
                    arrayList.add(data);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }

    public ArrayList<AnimalWeight> GetCountEventsByEvent(String Event) {
        ArrayList<AnimalWeight> arrayList = new ArrayList<>();
        String query = "Select AnimalEID,Event,Date from AnimalCount Where Event = '" + Event + "' ORDER BY CountId";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        AnimalWeight animal;
        if (cursor.moveToFirst()) {
            do {
                animal = new AnimalWeight();
                animal.setAnimalEID(cursor.getString(0));
                animal.setEvent(cursor.getString(1));
                animal.setDate(cursor.getString(2));
                arrayList.add(animal);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }

    public ArrayList<String> GetWeightEvents() {
        ArrayList<String> arrayList = new ArrayList<>();
        String query = "Select Event from AnimalWeigh Group by Event";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                String data = cursor.getString(0);
                if (data != null)
                    arrayList.add(data);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }

    public ArrayList<AnimalWeight> GetWeightEventsByEvent(String Event) {
        ArrayList<AnimalWeight> arrayList = new ArrayList<>();
        String query = "Select AnimalEID,Event,Date,Weight from AnimalWeigh Where Event = '" + Event + "' ORDER BY Date Desc";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        AnimalWeight animal;
        if (cursor.moveToFirst()) {
            do {
                animal = new AnimalWeight();
                animal.setAnimalEID(cursor.getString(0));
                animal.setEvent(cursor.getString(1));
                animal.setDate(cursor.getString(2));
                animal.setWeight(cursor.getString(3));
                arrayList.add(animal);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }

    public void DeleteProcessCount(String saveDate) {

    }

    public ArrayList<ReportBreed> GetReportFarm() {
        ArrayList<ReportBreed> arrayList = new ArrayList<>();
        String query = "select count(1),AnimalType,AnimalBreed,AnimalFarm from Animal Group by AnimalFarm,AnimalBreed,AnimalType";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        ReportBreed animal;
        if (cursor.moveToFirst()) {
            do {
                animal = new ReportBreed();
                animal.setName(cursor.getString(3));
                animal.setType(cursor.getString(1));
                animal.setBreed(cursor.getString(2));
                animal.setTotal(String.valueOf(cursor.getInt(0)));
                arrayList.add(animal);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }

    public ArrayList<ReportBreed> GetReportHerd() {
        ArrayList<ReportBreed> arrayList = new ArrayList<>();
        String query = "select count(1),AnimalType,AnimalBreed,AnimalHerd from Animal Group by AnimalHerd,AnimalBreed,AnimalType";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        ReportBreed animal;
        if (cursor.moveToFirst()) {
            do {
                animal = new ReportBreed();
                animal.setName(cursor.getString(3));
                animal.setType(cursor.getString(1));
                animal.setBreed(cursor.getString(2));
                animal.setTotal(String.valueOf(cursor.getInt(0)));
                arrayList.add(animal);
            } while (cursor.moveToNext());
        }
        return arrayList;
    }

    public String GetDbString(String query) {
        String result = "";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                result = cursor.getString(0);
            } while (cursor.moveToNext());
        }
        return result;
    }
}
