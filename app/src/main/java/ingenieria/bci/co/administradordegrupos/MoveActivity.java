package ingenieria.bci.co.administradordegrupos;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Adapters.AnimalAdapter;
import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.AnimalSales;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
import ingenieria.bci.co.administradordegrupos.Services.DatabaseTools;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

public class MoveActivity extends AppCompatActivity {

    Spinner ddlSellGroup;
    Spinner ddlGroupValue;
    ImageButton btnShowList;
    EditText txtSellTo;
    EditText txtPrice;
    Spinner ddlPaymentMethod;
    EditText txtPaymentReference;
    Button btnRegisterPayment;
    Activity activity;
    Database db;
    public static ArrayList<String> IdData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_move);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.manage_move_sell));
        activity = MoveActivity.this;
        final DatabaseTools dbtools = new DatabaseTools(activity);
        IdData = new ArrayList<>();

        db = new Database(activity);

        ddlGroupValue = (Spinner) findViewById(R.id.ddlGroupValue);
        ddlSellGroup = (Spinner) findViewById(R.id.ddlSellGroup);
        dbtools.setAdapterId(ddlSellGroup, R.array.sell_by, false);

        ddlSellGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = ddlSellGroup.getSelectedItem().toString();
                if (value.equals("All"))
                    return;
                dbtools.setAdapter(ddlGroupValue, value + "s", value + "Name", "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ddlPaymentMethod = (Spinner) findViewById(R.id.ddlPaymentMethod);
        dbtools.setAdapterId(ddlPaymentMethod, R.array.payment, false);


        btnShowList = (ImageButton) findViewById(R.id.btnShowList);
        txtSellTo = (EditText) findViewById(R.id.txtSellTo);
        txtPrice = (EditText) findViewById(R.id.txtPrice);
        txtPaymentReference = (EditText) findViewById(R.id.txtPaymentReference);
        btnRegisterPayment = (Button) findViewById(R.id.btnRegisterPayment);
        btnShowList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = ddlSellGroup.getSelectedItem().toString();
                String value = ddlGroupValue.getSelectedItem().toString();

                Intent intent = new Intent(MoveActivity.this, SellListActivity.class);
                intent.putExtra("type", type);
                intent.putExtra("value", value);
                startActivityForResult(intent, 0);
            }
        });
        btnRegisterPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String buyer = txtSellTo.getText().toString();
                String price = txtPrice.getText().toString();
                String ptype = ddlPaymentMethod.getSelectedItem().toString();
                String preference = txtPaymentReference.getText().toString();

                if (buyer.isEmpty() || price.isEmpty() || ptype.isEmpty() || preference.isEmpty() || IdData.size() <= 0) {
                    Tools.CreateSimpleDialog(activity, getString(R.string.completeFields));
                    return;
                }
                if (IdData.size() <= 0) {
                    Tools.CreateSimpleDialog(activity, getString(R.string.select_animals));
                    return;
                }


                AnimalSales animalSales = new AnimalSales();
                animalSales.setBuyer(buyer);
                animalSales.setPrice(price);
                animalSales.setPaymentType(ptype);
                animalSales.setReference(preference);
                if (db.SaveSale(animalSales, IdData)) {
                    Tools.CreateSimpleDialog(activity, getString(R.string.complete));
                    txtSellTo.setText("");
                    txtPrice.setText("");
                    txtPaymentReference.setText("");
                    IdData = new ArrayList<String>();
                    getSupportActionBar().setSubtitle("");
                } else {
                    Tools.CreateSimpleDialog(activity, getString(R.string.tryagain));
                }
            }
        });

    }

    @Override
    protected void onResume() {
        getSupportActionBar().setSubtitle("( " + IdData.size() + " ) " + getString(R.string.selected));
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_sale, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        } else if (id == R.id.history) {
            String type = ddlSellGroup.getSelectedItem().toString();
            String value = ddlGroupValue.getSelectedItem().toString();

            Intent intent = new Intent(this, SellHistoryActivity.class);
            intent.putExtra("text", type);
            intent.putExtra("value", value);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

}
