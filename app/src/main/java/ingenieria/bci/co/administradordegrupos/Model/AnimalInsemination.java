package ingenieria.bci.co.administradordegrupos.Model;

/**
 * Created by nicolas on 7/2/17.
 */

public class AnimalInsemination {

    public int InseminationId;
    public int AnimalId;
    public String CalvingDate;
    public String NoStraws;
    public String SireId;
    public String Comments;
    public String Status;

    public String toString() {

        return getCalvingDate() + " _ " + getComments();
    }

    public AnimalInsemination() {
    }

    public int getInseminationId() {
        return InseminationId;
    }

    public void setInseminationId(int inseminationId) {
        InseminationId = inseminationId;
    }

    public int getAnimalId() {
        return AnimalId;
    }

    public void setAnimalId(int animalId) {
        AnimalId = animalId;
    }

    public String getCalvingDate() {
        return CalvingDate;
    }

    public void setCalvingDate(String calvingDate) {
        CalvingDate = calvingDate;
    }

    public String getNoStraws() {
        return NoStraws;
    }

    public void setNoStraws(String noStraws) {
        NoStraws = noStraws;
    }

    public String getSireId() {
        return SireId;
    }

    public void setSireId(String sireId) {
        SireId = sireId;
    }

    public String getComments() {
        return Comments;
    }

    public void setComments(String comments) {
        Comments = comments;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
