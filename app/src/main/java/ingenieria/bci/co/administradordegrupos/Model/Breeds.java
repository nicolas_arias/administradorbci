package ingenieria.bci.co.administradordegrupos.Model;

/**
 * Created by nicolas on 6/13/17.
 */

public class Breeds {

    public int BreedId;
    public String BreedName;
    public String BreedType;
    public String BreedDestination;
    public String BreedStatus;

    public Breeds() {
    }

    public int getBreedId() {
        return BreedId;
    }

    public void setBreedId(int breedId) {
        BreedId = breedId;
    }

    public String getBreedName() {
        return BreedName;
    }

    public void setBreedName(String breedName) {
        BreedName = breedName;
    }

    public String getBreedType() {
        return BreedType;
    }

    public void setBreedType(String breedType) {
        BreedType = breedType;
    }

    public String getBreedDestination() {
        return BreedDestination;
    }

    public void setBreedDestination(String breedDestination) {
        BreedDestination = breedDestination;
    }

    public String getBreedStatus() {
        return BreedStatus;
    }

    public void setBreedStatus(String breedStatus) {
        BreedStatus = breedStatus;
    }
}
