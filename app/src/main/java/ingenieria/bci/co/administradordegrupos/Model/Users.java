package ingenieria.bci.co.administradordegrupos.Model;

/**
 * Created by nicolas on 6/13/17.
 */

public class Users {
    public int UserId;
    public String UserName;
    public String UserPassword;
    public String UserEmail;
    public String UserLicence;
    public String UserStatus;
    public String UserExpireDate;

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getUserPassword() {
        return UserPassword;
    }

    public void setUserPassword(String userPassword) {
        UserPassword = userPassword;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public void setUserEmail(String userEmail) {
        UserEmail = userEmail;
    }

    public void setUserLicence(String userLicence) {
        UserLicence = userLicence;
    }

    public void setUserStatus(String userStatus) {
        UserStatus = userStatus;
    }

    public void setUserExpireDate(String userExpireDate) {
        UserExpireDate = userExpireDate;
    }

    public int getUserId() {
        return UserId;
    }

    public String getUserName() {
        return UserName;
    }

    public String getUserEmail() {
        return UserEmail;
    }

    public String getUserLicence() {
        return UserLicence;
    }

    public String getUserStatus() {
        return UserStatus;
    }

    public String getUserExpireDate() {
        return UserExpireDate;
    }
}

