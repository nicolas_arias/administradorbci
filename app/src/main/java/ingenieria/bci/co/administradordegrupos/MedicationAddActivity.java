package ingenieria.bci.co.administradordegrupos;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.Medications;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

public class MedicationAddActivity extends AppCompatActivity {

    Button btnSaveMedicine;
    EditText txtMedicationName;
    EditText txtMedicinePrice;
    EditText txtMedicineQty;
    EditText txtMedicineExpiration;
    EditText txtSupplierName;
    EditText txtSuppierTel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_medication_add);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.AddMedicineTitle);

        txtMedicationName = (EditText) findViewById(R.id.txtMedicationName);
        txtMedicinePrice = (EditText) findViewById(R.id.txtMedicinePrice);
        txtMedicineQty = (EditText) findViewById(R.id.txtMedicineQty);
        txtMedicineExpiration = (EditText) findViewById(R.id.txtMedicineExpiration);
        txtSupplierName = (EditText) findViewById(R.id.txtSupplierName);
        txtSuppierTel = (EditText) findViewById(R.id.txtSuppierTel);

        btnSaveMedicine = (Button) findViewById(R.id.btnSaveMedicine);
        btnSaveMedicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = txtMedicationName.getText().toString();
                String price = txtMedicinePrice.getText().toString();
                String qty = txtMedicineQty.getText().toString();
                String expiration = txtMedicineExpiration.getText().toString();
                String suppname = txtSupplierName.getText().toString();
                String supptel = txtSuppierTel.getText().toString();

                if (name.isEmpty() || price.isEmpty() || qty.isEmpty() || expiration.isEmpty() || suppname.isEmpty() || supptel.isEmpty()) {
                    Toast.makeText(getBaseContext(), R.string.completeFields, Toast.LENGTH_LONG).show();
                    return;
                }
                try {
                    Medications medications = new Medications();
                    medications.setMedicationName(name);
                    medications.setMedicationPrice(Double.valueOf(price));
                    medications.setMedicationQuantity(Double.valueOf(qty));
                    medications.setMedicationExpiryDate(expiration);
                    medications.setMedicationSupplierName(suppname);
                    medications.setMedicationSupplierTelephone(supptel);
                    Database db = new Database(MedicationAddActivity.this);
                    if (db.AddMedicine(medications)) {
                        finishActivity();
                    }
                } catch (Exception ex) {
                    Toast.makeText(getBaseContext(), R.string.correct_values, Toast.LENGTH_LONG).show();
                    return;
                }


            }
        });
        txtMedicineExpiration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.OpenDateDialog(txtMedicineExpiration, MedicationAddActivity.this);
            }
        });
    }

    public void finishActivity() {
        this.finish();
    }

    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
