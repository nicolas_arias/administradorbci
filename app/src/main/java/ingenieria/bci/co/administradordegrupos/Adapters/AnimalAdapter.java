package ingenieria.bci.co.administradordegrupos.Adapters;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Model.Animal;
import ingenieria.bci.co.administradordegrupos.Model.Medications;
import ingenieria.bci.co.administradordegrupos.R;

/**
 * Created by nicolas on 6/29/17.
 */

public class AnimalAdapter extends ArrayAdapter<Animal> {
    final Context mainContext;
    final Activity activity;

    public AnimalAdapter(Context context, ArrayList<Animal> animals, Activity activity) {
        super(context, R.layout.animal_list, animals);
        this.mainContext = context;
        this.activity = activity;
    }

    private static class ViewHolder {
        TextView lblVisual;
        TextView lblEID;
        TextView lblBreed;
        TextView lblFarm;
        ImageView imgAnimal;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Animal animal = getItem(position);

        final AnimalAdapter.ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new AnimalAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.animal_list, parent, false);

            viewHolder.lblVisual = (TextView) convertView.findViewById(R.id.lblVisual);
            viewHolder.lblEID = (TextView) convertView.findViewById(R.id.lblEID);
            viewHolder.lblBreed = (TextView) convertView.findViewById(R.id.lblBreed);
            viewHolder.imgAnimal = (ImageView) convertView.findViewById(R.id.imgAnimal);

            viewHolder.lblVisual.setText(animal.getAnimalVisualNumber());
            viewHolder.lblEID.setText(animal.getAnimalEID());
            viewHolder.lblBreed.setText(animal.getAnimalGender() + " - " + animal.getAnimalBreed());

            final String data = animal.getAnimalPhoto();
//            final byte[] decodedString = Base64.decode(animal.getAnimalPhoto(), Base64.DEFAULT);
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (animal.getAnimalType().equals(activity.getResources().getString(R.string.Cattle))) {
                        viewHolder.imgAnimal.setImageResource(R.drawable.cow_list);
                    } else if (animal.getAnimalType().equals(activity.getResources().getString(R.string.Goat))) {
                        viewHolder.imgAnimal.setImageResource(R.drawable.breed);
                    } else if (animal.getAnimalType().equals(activity.getResources().getString(R.string.Sheep))) {
                        viewHolder.imgAnimal.setImageResource(R.drawable.sheep);
                    } else {
                        viewHolder.imgAnimal.setImageResource(R.drawable.bug);
                    }
                }
            });
        } else {
            viewHolder = (AnimalAdapter.ViewHolder) convertView.getTag();
        }
        return convertView;
    }
}
