package ingenieria.bci.co.administradordegrupos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Adapters.HerdSummaryAdapter;
import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.ReportBreed;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;

public class ReportActivity extends AppCompatActivity {

    ListView summarylist;
    Database db;
    ArrayList<ReportBreed> array;
    ArrayAdapter<ReportBreed> adapter;
    RelativeLayout NoDataPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_report);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        summarylist = (ListView) findViewById(R.id.summarylist);
        NoDataPanel = (RelativeLayout) findViewById(R.id.NoDataPanel);

        array = new ArrayList<>();
        db = new Database(this);
        Intent intent = getIntent();
        String tipo = intent.getStringExtra("tipo");
        if (tipo.equals("farm")) {
            Farm();
        } else {
            Herd();
        }
    }

    private void Farm() {
        setTitle(getString(R.string.farm_title_summary));
        if (!array.isEmpty())
            array.clear();
        array = db.GetReportFarm();
        adapter = new HerdSummaryAdapter(this, array);
        summarylist.setAdapter(adapter);
        if(array.size() <= 0){
            NoDataPanel.setVisibility(View.VISIBLE);
        }
    }

    private void Herd() {
        setTitle(getString(R.string.herd_title_summary));
        if (!array.isEmpty())
            array.clear();
        array = db.GetReportHerd();
        adapter = new HerdSummaryAdapter(this, array);
        summarylist.setAdapter(adapter);
        if(array.size() <= 0){
            NoDataPanel.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
