package ingenieria.bci.co.administradordegrupos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.AnimalWeight;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;

public class CountWeighListActivity extends AppCompatActivity {
    ListView summarylist;
    Database db;
    ArrayList<String> array;
    ArrayAdapter<String> adapter;
    String tipo;
    RelativeLayout NoDataPanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_count_weigh_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getString(R.string.select_event));
        Intent intent = getIntent();
        NoDataPanel = (RelativeLayout) findViewById(R.id.NoDataPanel);
        db = new Database(this);
        tipo = intent.getStringExtra("tipo");
        if (tipo.equals("weight")) {
            array = db.GetWeightEvents();
        } else {
            array = db.GetCountEvents();
        }
        summarylist = (ListView) findViewById(R.id.summarylist);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, array);
        summarylist.setAdapter(adapter);
        if(array.size() <= 0){
            NoDataPanel.setVisibility(View.VISIBLE);
        }
        summarylist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String event = array.get(position);
                Intent intent = new Intent(CountWeighListActivity.this, CountSummaryActivity.class);
                intent.putExtra("tipo", tipo);
                intent.putExtra("event", event);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
