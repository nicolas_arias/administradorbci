package ingenieria.bci.co.administradordegrupos.Model;

/**
 * Created by nicolas on 7/3/17.
 */

public class AnimalCondition {
    public int RegisterId;
    public String AnimalEID;
    public String AnimalVisualNumber;
    public String LastSeen;
    public String Symptoms;
    public String Condition;
    public String Diagnosis;
    public String Medication;
    public String Dosage;
    public String Date;

    public AnimalCondition() {

    }

    public int getRegisterId() {
        return RegisterId;
    }

    public void setRegisterId(int registerId) {
        RegisterId = registerId;
    }

    public String getAnimalEID() {
        return AnimalEID;
    }

    public void setAnimalEID(String animalEID) {
        AnimalEID = animalEID;
    }

    public String getAnimalVisualNumber() {
        return AnimalVisualNumber;
    }

    public void setAnimalVisualNumber(String animalVisualNumber) {
        AnimalVisualNumber = animalVisualNumber;
    }

    public String getLastSeen() {
        return LastSeen;
    }

    public void setLastSeen(String lastSeen) {
        LastSeen = lastSeen;
    }

    public String getSymptoms() {
        return Symptoms;
    }

    public void setSymptoms(String symptoms) {
        Symptoms = symptoms;
    }

    public String getCondition() {
        return Condition;
    }

    public void setCondition(String condition) {
        Condition = condition;
    }

    public String getDiagnosis() {
        return Diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        Diagnosis = diagnosis;
    }

    public String getMedication() {
        return Medication;
    }

    public void setMedication(String medication) {
        Medication = medication;
    }

    public String getDosage() {
        return Dosage;
    }

    public void setDosage(String dosage) {
        Dosage = dosage;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}
