package ingenieria.bci.co.administradordegrupos.Model;

/**
 * Created by nicolas on 6/13/17.
 */

public class Farms {
    public int FarmId;
    public String FarmName;
    public double FarmSize;
    public String FarmStatus;
    public String FarmLng;
    public String FarmLat;

    public Farms() {
    }

    public Farms(int farmId, String farmName, double farmSize, String farmStatus, String farmLng, String farmLat) {
        FarmId = farmId;
        FarmName = farmName;
        FarmSize = farmSize;
        FarmStatus = farmStatus;
        FarmLng = farmLng;
        FarmLat = farmLat;
    }

    public int getFarmId() {
        return FarmId;
    }

    public void setFarmId(int farmId) {
        FarmId = farmId;
    }

    public String getFarmName() {
        return FarmName;
    }

    public void setFarmName(String farmName) {
        FarmName = farmName;
    }

    public double getFarmSize() {
        return FarmSize;
    }

    public void setFarmSize(double farmSize) {
        FarmSize = farmSize;
    }

    public String getFarmStatus() {
        return FarmStatus;
    }

    public void setFarmStatus(String farmStatus) {
        FarmStatus = farmStatus;
    }

    public String getFarmLng() {
        return FarmLng;
    }

    public void setFarmLng(String farmLng) {
        FarmLng = farmLng;
    }

    public String getFarmLat() {
        return FarmLat;
    }

    public void setFarmLat(String farmLat) {
        FarmLat = farmLat;
    }
}
