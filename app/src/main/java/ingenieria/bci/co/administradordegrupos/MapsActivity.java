package ingenieria.bci.co.administradordegrupos;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Map;

import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    LocationManager locationManager;
    ProgressDialog pd;
    String Lat;
    String Lng;
    Button btn;
    Button btnClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        //setDisplayHomeAsUpEnabled(true);
        //getActionBar().show();
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        Lat = intent.getStringExtra("Lat");
        Lng = intent.getStringExtra("Lng");


        FarmsAddActivity.FarmLocation = "0|0";
        btn = (Button) findViewById(R.id.btn);
        btnClose = (Button) findViewById(R.id.btnClose);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askForPermission(Manifest.permission.ACCESS_FINE_LOCATION, 0x1);
                pd.setMessage("loading");
                pd.show();
                getLocation();
            }
        });
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FinishActivity();
            }
        });
        pd = new ProgressDialog(MapsActivity.this);


    }

    public void FinishActivity() {
        this.finish();
    }

    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(MapsActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MapsActivity.this, permission)) {
                ActivityCompat.requestPermissions(MapsActivity.this, new String[]{permission}, requestCode);
            } else {
                ActivityCompat.requestPermissions(MapsActivity.this, new String[]{permission}, requestCode);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (Lat != null && !Lat.isEmpty()) {
            try {
                LatLng currentPlace = new LatLng(Double.parseDouble(Lat), Double.parseDouble(Lng));
                btn.setVisibility(View.INVISIBLE);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentPlace, 8));
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Double.parseDouble(Lat), Double.parseDouble(Lng))));
            } catch (Exception e) {
                Toast.makeText(this, R.string.location_problem, Toast.LENGTH_LONG).show();
            }
        } else {
            LatLng sydney = new LatLng(-4.7, 72.2);
            mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                @Override
                public void onMapClick(LatLng latLng) {
                    mMap.clear();
                    mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(latLng.latitude, latLng.longitude)));
                    FarmsAddActivity.FarmLocation = latLng.latitude + "|" + latLng.longitude;
                }
            });
        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.askEnableGps)
                .setCancelable(false)
                .setPositiveButton(R.string.alertyes, new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(R.string.alertno, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                        finishActivity();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    public void finishActivity() {
        this.finish();
    }

    public void getLocation() {
        try {
            final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                buildAlertMessageNoGps();
            }
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5, this);
        } catch (SecurityException e) {
            pd.dismiss();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mMap.clear();
        LatLng currentPlace = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentPlace, 8));
        mMap.addMarker(new MarkerOptions()
                .position(new LatLng(location.getLatitude(), location.getLongitude())));
        FarmsAddActivity.FarmLocation = location.getLatitude() + "|" + location.getLongitude();
        pd.dismiss();
    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }
}
