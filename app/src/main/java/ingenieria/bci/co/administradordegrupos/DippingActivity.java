package ingenieria.bci.co.administradordegrupos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.DynamicList;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;
import ingenieria.bci.co.administradordegrupos.Services.DatabaseTools;
import ingenieria.bci.co.administradordegrupos.Services.Tools;

public class DippingActivity extends AppCompatActivity {

    Spinner ddlSelectBy;
    Spinner ddlGroupValue;
    EditText txtDippingMethod;
    EditText txtComments;
    EditText txtDateDipping;
    Button btnRegisterDipping;
    ImageButton btnShowList;
    public static ArrayList<String> IdData;
    Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        setContentView(R.layout.activity_dipping);
        setTitle(getString(R.string.manage_dipping));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        db = new Database(DippingActivity.this);
        IdData = new ArrayList<>();
        txtDateDipping = (EditText) findViewById(R.id.txtDateDipping);


        ddlSelectBy = (Spinner) findViewById(R.id.ddlSelectBy);
        ddlGroupValue = (Spinner) findViewById(R.id.ddlGroupValue);
        txtDippingMethod = (EditText) findViewById(R.id.txtDippingMethod);
        txtComments = (EditText) findViewById(R.id.txtComments);
        txtDateDipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.OpenDateDialog(txtDateDipping, DippingActivity.this);
            }
        });


        btnRegisterDipping = (Button) findViewById(R.id.btnRegisterDipping);
        btnShowList = (ImageButton) findViewById(R.id.btnShowList);
        btnShowList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String type = ddlSelectBy.getSelectedItem().toString();
                String value = ddlGroupValue.getSelectedItem().toString();

                Intent intent = new Intent(DippingActivity.this, SellListActivity.class);
                intent.putExtra("type", type);
                intent.putExtra("value", value);
                startActivityForResult(intent, 0);
            }
        });

        final DatabaseTools dbtools = new DatabaseTools(DippingActivity.this);
        dbtools.setAdapterId(ddlSelectBy, R.array.sell_by, false);
        ddlSelectBy.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = ddlSelectBy.getSelectedItem().toString();
                if (value.equals("All"))
                    return;
                dbtools.setAdapter(ddlGroupValue, value + "s", value + "Name", "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnRegisterDipping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String method = txtDippingMethod.getText().toString();
                String comments = txtComments.getText().toString();
                String date = txtDateDipping.getText().toString();

                if (date.isEmpty() || method.isEmpty() || comments.isEmpty() || IdData.size() <= 0) {
                    Tools.CreateSimpleDialog(DippingActivity.this, getString(R.string.completeFields));
                    return;
                }
                if (db.RegisterDipping(date, method, comments, IdData)) {
                    Tools.CreateSimpleDialog(DippingActivity.this, getString(R.string.complete));
                    txtDippingMethod.setText("");
                    txtComments.setText("");
                    txtDateDipping.setText("");
                    IdData = new ArrayList<>();
                    getSupportActionBar().setSubtitle("");
                } else {
                    Tools.CreateSimpleDialog(DippingActivity.this, getString(R.string.tryagain));
                }
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        getSupportActionBar().setSubtitle("( " + IdData.size() + " ) " + getString(R.string.selected));

    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }
}
