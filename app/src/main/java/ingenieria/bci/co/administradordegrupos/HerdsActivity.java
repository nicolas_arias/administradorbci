package ingenieria.bci.co.administradordegrupos;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;

import android.preference.DialogPreference;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;

import java.util.ArrayList;

import ingenieria.bci.co.administradordegrupos.Adapters.HerdListAdapter;
import ingenieria.bci.co.administradordegrupos.Data.Database;
import ingenieria.bci.co.administradordegrupos.Model.Herds;
import ingenieria.bci.co.administradordegrupos.Services.ChangeLanguage;

public class HerdsActivity extends AppCompatActivity {
    Database db;
    ArrayList<Herds> herdsArray;
    HerdListAdapter adapter;
    RelativeLayout NoDataPanel;
    FloatingActionButton fabAddHerd;
    ListView herdList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            ChangeLanguage.SetNewLanguage(this, "en");
        } catch (Exception e) {
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(R.string.herdsTitle);
        setContentView(R.layout.activity_herds);
        SetViews();
    }

    private void SetViews() {
        fabAddHerd = (FloatingActionButton) findViewById(R.id.fabAddHerd);
        fabAddHerd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HerdsActivity.this, HerdsAddActivity.class);
                startActivity(intent);
            }
        });
    }

    private void FillList() {
        db = new Database(getBaseContext());
        NoDataPanel = (RelativeLayout) findViewById(R.id.NoDataPanel);
        herdsArray = null;
        herdsArray = db.GetHerds();
        adapter = new HerdListAdapter(getBaseContext(), herdsArray, HerdsActivity.this);
        herdList = (ListView) findViewById(R.id.herdlist);
        herdList.setAdapter(adapter);
        if (herdsArray.size() > 0) {
            NoDataPanel.setVisibility(View.INVISIBLE);

            herdList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView parent, View view, final int position, long id) {
                    final CharSequence[] items = {getString(R.string.change_status)};
                    final AlertDialog.Builder builder = new AlertDialog.Builder(HerdsActivity.this);
                    builder.setTitle("BCI");
                    builder.setItems(items, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            AlertDialog.Builder builderD = new AlertDialog.Builder(HerdsActivity.this);
                            builderD.setTitle("BCI");
                            builderD.setMessage(R.string.inactivate);
                            builderD.setPositiveButton(R.string.alertyes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Database db = new Database(HerdsActivity.this);
                                    db.InactivateHerds(herdsArray.get(position).getHerdId());
                                    dialog.dismiss();
                                    Toast.makeText(HerdsActivity.this, R.string.complete, Toast.LENGTH_LONG);
                                    try {
                                        FillList();
                                    } catch (Exception e) {

                                    }
                                }
                            });
                            builderD.setNegativeButton(R.string.alertno, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                            builderD.show();
                        }
                    });
                    builder.show();
                    return false;
                }
            });
        } else
        {
            NoDataPanel.setVisibility(View.VISIBLE);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onStart() {
        FillList();
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        this.finish();
        super.onBackPressed();
    }

}
